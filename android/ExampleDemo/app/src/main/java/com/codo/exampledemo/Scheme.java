package com.codo.exampledemo;

/**
 * Created by Administrator on 2015/11/2.
 */
public class Scheme {
    private int _id;
    private String name;

    public Scheme(int _id, String name) {
        this._id = _id;
        this.name = name;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Scheme{" +
                "_id=" + _id +
                ", name='" + name + '\'' +
                '}';
    }
}
