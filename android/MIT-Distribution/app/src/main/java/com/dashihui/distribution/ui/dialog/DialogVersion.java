package com.dashihui.distribution.ui.dialog;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dashihui.distribution.R;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;

import java.io.File;


public class DialogVersion extends Activity implements View.OnClickListener {
    private TextView txtUpdateLog;
    private TextView mTxtTime;
    private Button btnUpdate;
    private Button btnCancle;
    private TextView txtTitle;
    private ProgressBar mPbDown;
    private static final String downPath = "/sdcard/com.dashihui.distribution/";
    private LinearLayout llBtn;
    private View dialog_line2;
    private String mDownurl;
    private String mCurver;
    private String fileName;
    private File file;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_version);
        //获取数据
        Intent intent = getIntent();
        mDownurl = intent.getStringExtra("downurl");
        mCurver = intent.getStringExtra("curver");
        fileName = intent.getStringExtra("fileName");
        Log.i("mmmmm",mDownurl);
        Log.i("mmmmm",mCurver);
        Log.i("mmmmm", fileName);
        initView();
    }

    /**
     * 初始化控件和点击事件
     */
    private void initView() {
        txtUpdateLog = (TextView) findViewById(R.id.txtUpdateLog);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        mPbDown = (ProgressBar) findViewById(R.id.pbDown);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnCancle = (Button) findViewById(R.id.btnCancle);
        llBtn = (LinearLayout) findViewById(R.id.llBtn);
        dialog_line2 = (View) findViewById(R.id.dialog_line2);
        btnCancle.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUpdate:
                //点击更新,开始下载
                txtTitle.setText("正在下载...");
                mPbDown.setVisibility(View.VISIBLE);
                dialog_line2.setVisibility(View.GONE);
                llBtn.setVisibility(View.GONE);
                txtUpdateLog.setVisibility(View.GONE);
                file = new File(downPath + fileName);
                if(file.exists()){
                    installApk(file);
                }else{
                    downAPK();
                }
                break;
            case R.id.btnCancle:
                //点击取消,关闭页面
                finish();
                break;
        }
    }

    /**
     * 开始下载apk
     */
    private void  downAPK() {
        HttpUtils http = new HttpUtils();
        HttpHandler handler = http.download(mDownurl, downPath + fileName, true, true, new RequestCallBack<File>() {
            /**
             * 开始下载
             */
            @Override
            public void onStart() {
            }

            /**
             * 正在下载
             * @param total 文件总大小
             * @param current 当前大小
             * @param isUploading
             */
            @Override
            public void onLoading(long total, long current, boolean isUploading) {
                mPbDown.setMax((int) total);
                mPbDown.setProgress((int) current);
            }

            /**
             * 下载成功,进入安装页面,并关闭本页面
             * @param responseInfo
             */
            @Override
            public void onSuccess(ResponseInfo<File> responseInfo) {
                file = responseInfo.result;
                installApk(file);
            }

            /**
             * 下载失败
             * @param error
             * @param msg 下载失败原因
             */
            @Override
            public void onFailure(HttpException error, String msg) {
                Log.i("msg",msg);
                finish();
            }
        });
    }

    /**
     * 安装文件
     */
    private void installApk(File file){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        startActivity(intent);
    }
}
