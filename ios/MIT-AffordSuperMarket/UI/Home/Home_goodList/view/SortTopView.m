//
//  SortTopView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#define KViewHight self.bounds.size.height
#import "SortTopView.h"

//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
@interface SortTopView()
{
    NSInteger  _seleteCount;
}
@end
@implementation SortTopView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //
         _seleteCount =0;
        self.backgroundColor =[UIColor whiteColor];
        [self initViewsWithFrame:frame];
    }
    return self;
}
#pragma mark -- button Action
-(void)OnFilterBtn:(UIButton *)sender{
    for (int i = 0; i < 3; i++) {
        if (i ==0 ||i==2) {
            UIButton *btn = (UIButton *)[self viewWithTag:100+i];
            UIButton *sanjiaoBtn = (UIButton *)[self viewWithTag:120+i];
            btn.selected = NO;
            sanjiaoBtn.selected = NO;
        }
    }
    if (sender.tag==kTogetherBtn ) {
         sender.selected = YES;
        if (_delegate && [_delegate respondsToSelector:@selector(kTogetherBtnClick)]) {
            [_delegate kTogetherBtnClick];
        }
        UIButton *btn = (UIButton *)[self viewWithTag:100];
        [btn setTitleColor:[UIColor colorWithHexString:@"#db3b34"] forState:UIControlStateNormal];
    }else if(sender.tag==kProprietaryBtn){
        UIButton *ziYing=(UIButton *)sender;
        ziYing.selected = !sender.selected;
        UIButton *sanjiaoBtn = (UIButton *)[self viewWithTag:121];
        if (ziYing.selected ==YES) {
            sanjiaoBtn.selected = YES;
        }else if(ziYing.selected ==NO){
            sanjiaoBtn.selected = NO;
        }
        if (_delegate && [_delegate respondsToSelector:@selector(kProprietaryBtnWithSelectedState:)]) {
            [_delegate kProprietaryBtnWithSelectedState:ziYing.selected];
        }

    }else if(sender.tag==kPriceBtn ){
        sender.selected = YES;
        UIButton *sjBtn = (UIButton *)[self viewWithTag:122];
         _seleteCount ++;
        NSInteger a =_seleteCount%2;
        NSString *state=@"";
        if (a == 0) {
            
            [sjBtn setImage:[UIImage imageNamed:@"cvs_btn_nav_top_red_sel"] forState:UIControlStateSelected];
            state =@"1";
        }else{
            [sjBtn setImage:[UIImage imageNamed:@"cvs_btn_nav_down_red_sel"] forState:UIControlStateSelected];
            state =@"2";
        }
        sjBtn.selected = YES;
        if (_delegate && [_delegate respondsToSelector:@selector(kPriceBtnClickWithSort:)]) {
            [_delegate kPriceBtnClickWithSort:state];
        }
        UIButton *btn = (UIButton *)[self viewWithTag:100];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
}

-(void)sanjiaoBtn:(UIButton *)sender{
    if (sender.tag == 121) {
        UIButton *sanjiaoBtn=(UIButton *)sender;
        sanjiaoBtn.selected = !sender.selected;
        UIButton *ziYing = (UIButton *)[self viewWithTag:101];
        if (sanjiaoBtn.selected ==YES) {
            ziYing.selected = YES;
        }else if(sanjiaoBtn.selected ==NO){
            ziYing.selected = NO;
        }
        if (_delegate && [_delegate respondsToSelector:@selector(kProprietaryBtnWithSelectedState:)]) {
            [_delegate kProprietaryBtnWithSelectedState:ziYing.selected];
        }

    }
}
//初始化视图
-(void)initViewsWithFrame:(CGRect)frame{
    //筛选
    NSArray *filterName = @[@"综合",[NSString stringWithFormat:@"%@     ",kDaShiHuiStoreName],@"价格   "];
    //筛选
    for (int i = 0; i < filterName.count; i++) {
        //文字
        UIButton *filterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        filterBtn.tag = 100+i;
        filterBtn.titleLabel.font = [UIFont systemFontOfSize:HightScalar(16)];
        filterBtn.frame = CGRectMake(i*VIEW_WIDTH/3, 0, VIEW_WIDTH/3-4, KViewHight);
        [filterBtn setTitle:filterName[i] forState:UIControlStateNormal];
        [filterBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [filterBtn setTitleColor:[UIColor colorWithHexString:@"#db3b34"] forState:UIControlStateSelected];
        [filterBtn addTarget:self action:@selector(OnFilterBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:filterBtn];
        //分割线
        UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake((i+1)*VIEW_WIDTH/3, KViewHight/4, 1, KViewHight/2)];
        imageView.image =[UIImage createImageWithColor:[UIColor colorWithHexString:@"#cccccc"] frame:CGRectMake(0, 0, 0.5, KViewHight/2)];
        [self addSubview:imageView];
        if (i==0) {
            [filterBtn setTitleColor:[UIColor colorWithHexString:@"#db3b34"] forState:UIControlStateNormal];
        }else if(i==1){
            
        }else if(i==2){
           
        }
        //三角
        UIButton *sanjiaoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [sanjiaoBtn addTarget:self action:@selector(sanjiaoBtn:) forControlEvents:UIControlEventTouchUpInside];

        sanjiaoBtn.tag = 120+i;
        if (i==1) {
            sanjiaoBtn.frame = CGRectMake((i+1)*VIEW_WIDTH/filterName.count-25, (KViewHight - 20)/2, 20, 20);
            [sanjiaoBtn setImage:[UIImage imageNamed:@"fxk"] forState:UIControlStateNormal];
            [sanjiaoBtn setImage:[UIImage imageNamed:@"fxk_choose"] forState:UIControlStateSelected];
            [self addSubview:sanjiaoBtn];
        }
        if (i==2) {
           
            sanjiaoBtn.frame = CGRectMake((i+1)*VIEW_WIDTH/filterName.count-25, (KViewHight - 10)/2, 8, 10);
            [sanjiaoBtn setImage:[UIImage imageNamed:@"cvs_btn_nav_down_black_nor"] forState:UIControlStateNormal];
            [self addSubview:sanjiaoBtn];
        }
        
    }
    //下划线
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-1, VIEW_WIDTH, 0.7)];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#dddddd"];
    [self addSubview:lineView];
}

@end
