//
//  Home_Root_HeaderView.h
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Home_Root_HeaderView
 Created_Date： 20160217
 Created_People： GT
 Function_description： 首页头部展示视图
 ***************************************/

#import <UIKit/UIKit.h>

@interface Home_Root_HeaderView : UIView
- (id)initWithFrame:(CGRect)frame delegate:(id)delegate;
- (void)updateTableViewWithData:(NSDictionary*)dataDic;
-(CGFloat)calculationFactHight;
@end
