//
//  Home_TableView_BigKindVIewCell.h
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Home_TableView_BigKindVIewCell
 Created_Date： 20160217
 Created_People： GT
 Function_description： 商品大类展示视图
 ***************************************/

#import <UIKit/UIKit.h>

@interface Home_TableView_BigKindVIewCell : UITableViewCell
- (void)updateViewWithData:(NSArray*)dataArr sectionTitle:(NSString*)sectionTitle;
- (CGFloat)cellFactHight;
@end
