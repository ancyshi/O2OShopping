//
//  Home_TableView_recommendStyle.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/2/23.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Home_TableView_recommendStyle.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "Global.h"
#define ksepLineHight 0.8
#define khSpace 10
@interface Home_TableView_recommendStyle()
@property(nonatomic,strong)UIImageView *leftLine;
@property(nonatomic,strong)UIImageView *imageLogo;
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UIImageView *rightLine;
@property(nonatomic, strong)UIImageView *hsepLineImageView;
@end
@implementation Home_TableView_recommendStyle
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSubview];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setUpSubview {
    [self.contentView addSubview:self.hsepLineImageView];
    [self.contentView addSubview:self.leftLine];
    [self.contentView addSubview:self.imageLogo];
    [self.contentView addSubview:self.title];
    [self.contentView addSubview:self.rightLine];
    
}
-(void)updateViewWithData:(NSArray *)titleArr
{
    self.title.text =[titleArr firstObject];
}


- (UIImageView*)hsepLineImageView {
    if (!_hsepLineImageView) {
        UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, ksepLineHight - 0.1)];
        imageView.image =[UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"] frame:CGRectMake(0, 0, 1, ksepLineHight)];
        _hsepLineImageView =imageView;
        
    }
    return _hsepLineImageView;
}
-(UIImageView *)leftLine
{
    if (!_leftLine) {
        UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, HightScalar(23), (VIEW_WIDTH-HightScalar(104))/2, ksepLineHight)];
        imageView.image =[UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"] frame:CGRectMake(0, 0, 1, ksepLineHight)];
        _leftLine =imageView;
        
    }
    return _leftLine;
}

-(UIImageView *)imageLogo
{
    if (!_imageLogo) {
        UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftLine.frame) + khSpace
                                                                             ,CGRectGetMidY(self.leftLine.frame) - HightScalar(18)/2
                                                                             , HightScalar(18), HightScalar(18))];
        imageView.image =[UIImage imageNamed:@"home_img_recommend"];
        _imageLogo =imageView;
    }
    return _imageLogo;
}
-(UILabel *)title
{
    if (!_title) {
        UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imageLogo.frame) + 3
                                                                 , CGRectGetMidY(self.leftLine.frame) - HightScalar(20)/2
                                                                 , HightScalar(65)
                                                                 , HightScalar(20))];
        label.textColor =[UIColor colorWithHexString:@"#e9423a"];
        label.font =[UIFont systemFontOfSize:FontSize(16)];
        _title =label;
    }
    return _title;
}
-(UIImageView *)rightLine
{
    if (!_rightLine) {
        UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.title.frame) + khSpace
                                                                             ,CGRectGetMinY(self.leftLine.frame)
                                                                             ,CGRectGetWidth(self.leftLine.bounds)
                                                                            , CGRectGetHeight(self.leftLine.bounds))];
        imageView.image =[UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"] frame:CGRectMake(0, 0, 1, ksepLineHight)];
        _rightLine =imageView;
    }
    return _rightLine;
}
@end
