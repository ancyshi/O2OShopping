//
//  Home_TableView_ServiceViewCell.m
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Home_TableView_ServiceViewCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "FL_WaterFallLayout.h"
#define kviewHeight HightScalar(196)
@interface Home_TableView_ServiceViewCell()<UICollectionViewDelegate,UICollectionViewDataSource,FL_WaterFallLayoutDelegate>
{
}
@property(nonatomic, strong)NSMutableArray *dataSource;
@property(nonatomic, strong)UICollectionView *collectionView;
@end
@implementation Home_TableView_ServiceViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _dataSource = [[NSMutableArray alloc]init];
        [self setUpSubview];
    }
    return self;
}
- (void)updateViewWithData:(NSArray*)dataArr {
    [self.dataSource removeAllObjects];
    [self.dataSource addObjectsFromArray:dataArr];
    self.frame = CGRectMake(0, 0, VIEW_WIDTH, kviewHeight);
    [self.collectionView reloadData];
    
}
#pragma mark -- 初始化视图
- (void)setUpSubview {
    
    [self.contentView addSubview:self.collectionView];
}

#pragma mark -- UICollectionViewDataSource
-(CGFloat)waterFlowLayout:(FL_WaterFallLayout *)waterFlowLayout heightForWidth:(CGFloat)width indexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return kviewHeight;
    } else {
        return kviewHeight/2;
    }
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Home_TableView_ServiceCollectionCell" forIndexPath:indexPath];
    cell.backgroundColor= [UIColor whiteColor];
    UIImageView * imageview = [[UIImageView alloc]initWithFrame:cell.frame];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    UIImage *image = [UIImage imageNamed:[self.dataSource objectAtIndex:indexPath.row]];
    imageview.image = image;
    cell.backgroundView =imageview;
    return cell;
}
#pragma mark -- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_delegate && [_delegate respondsToSelector:@selector(selectedItemAtIndexPath:)]) {
        [_delegate selectedItemAtIndexPath:indexPath];
    }
    
}
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *Cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    Cell.alpha = 0.6;
    
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *Cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    Cell.alpha = 1;
}
- (UICollectionView*)collectionView {
    if (!_collectionView) {
        FL_WaterFallLayout *waterFlow = [[FL_WaterFallLayout alloc]init];
        waterFlow.delegate = self;
        UICollectionView *collectionview = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, kviewHeight) collectionViewLayout:waterFlow];
        collectionview.backgroundColor = [UIColor colorWithHexString:@"#dddddd"];
        [collectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Home_TableView_ServiceCollectionCell"];
        collectionview.delegate = self;
        collectionview.dataSource = self;
        collectionview.scrollEnabled = NO;
        _collectionView = collectionview;
    }
    return _collectionView;
}


@end
