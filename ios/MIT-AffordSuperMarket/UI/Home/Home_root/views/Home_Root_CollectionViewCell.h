//
//  Home_Root_CollectionViewCell.h
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol Home_Root_CollectionViewCellDelegate <NSObject>

- (void)selectedItemAtIndexPath:(NSIndexPath *)indexPath buttonPoint:(CGPoint )buttonPonint;
@end
@interface Home_Root_CollectionViewCell : UICollectionViewCell
- (void)retSubViewWithData:(NSDictionary*)dataDic;
@property(nonatomic, strong)NSIndexPath *indexPath;
@property(assign,nonatomic)id<Home_Root_CollectionViewCellDelegate>delegate;

@end
