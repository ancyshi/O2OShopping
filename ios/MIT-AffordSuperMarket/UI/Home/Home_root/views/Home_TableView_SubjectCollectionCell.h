//
//  Home_TableView_SubjectCollectionCell.h
//  DaShiHuiHome
//
//  Created by apple on 16/2/18.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： Home_Root_HeaderView
 Created_Date： 20160217
 Created_People： JSQ
 Function_description： 首页活动ConllertionViewCell
 ***************************************/
#import <UIKit/UIKit.h>

@interface Home_TableView_SubjectCollectionCell : UICollectionViewCell
@property(nonatomic, strong) UIImageView *leftSepLine;
- (void)updateViewWithData:(NSDictionary*)dataDic;

@end
