//
//  Home_Root_ViewModel.h
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Home_Root_ViewModel
 Created_Date： 20160217
 Created_People： GT
 Function_description： 请求数据，并对数据进行处理
 ***************************************/

#import <Foundation/Foundation.h>
@interface Home_Root_ViewModel : NSObject
- (void)loadHomeHeaderDataSuccess:(void(^)(NSDictionary* responseData))success failue:(void(^)(bool isFailure))failure;
@end
