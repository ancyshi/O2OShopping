//
//  Service_HomemakingCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/1/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Service_HomemakingCell : UITableViewCell
//传值 设置Cell
-(void)setSuberViewDataWithDictionary:(NSDictionary *)dictionary;
@end
