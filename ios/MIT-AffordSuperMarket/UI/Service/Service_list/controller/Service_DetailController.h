//
//  Service_DetailController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/21.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Service_DetailController
 Created_Date： 20151221
 Created_People： GT
 Function_description： 服务商家详情页
 ***************************************/

#import "BaseViewController.h"

@interface Service_DetailController : BaseViewController
@property(nonatomic, strong)NSString *shopID;
@property(nonatomic, strong)NSString *shopName;
@property(nonatomic, strong)NSString *shopImage;
@end
