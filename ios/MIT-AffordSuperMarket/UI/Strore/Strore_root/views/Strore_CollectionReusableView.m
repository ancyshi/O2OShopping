//
//  Strore_CollectionReusableView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/5.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Strore_CollectionReusableView.h"
//untils
#import "UIColor+Hex.h"
#import "Global.h"
#import "NSString+TextSize.h"
#import "NSString+Conversion.h"
#import "UIImage+ColorToImage.h"
//vendor
#import "HYBLoopScrollView.h"
#import "Masonry.h"
#import "UIImageView+AFNetworking.h"
@interface Strore_CollectionReusableView ()
//推荐视图
@property(nonatomic, strong)UIImageView *leftImageView;
@property(nonatomic, strong)UILabel *leftTitleLabel;
@property(nonatomic, strong)UILabel *leftCurrentPrice;
@property(nonatomic, strong)UILabel *leftOldPrice;
//限量视图
@property(nonatomic, strong)UIImageView *rightOneImageView;
@property(nonatomic, strong)UILabel *rightOneTitleLabel;
@property(nonatomic, strong)UILabel *righOneCurrentPrice;
@property(nonatomic, strong)UILabel *righOneOldPrice;
//一元购视图
@property(nonatomic, strong)UIImageView *rightTwoImageView;
@property(nonatomic, strong)UILabel *rightTwoTitleLabel;
@property(nonatomic, strong)UILabel *rightTwoCurrentPrice;
@property(nonatomic, strong)UILabel *rightTwoOldPrice;
@property(nonatomic, strong)HYBLoopScrollView *loopScrollView;
@end
@implementation Strore_CollectionReusableView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupSubView];
    }
    return self;
}
//初始化视图
- (void)setupSubView {
    //轮播图
    NSArray *defaultImages = @[@"productDetail_Default_loopImage"];
    CGRect loopScrollViewFrame = CGRectMake(0, 0, VIEW_WIDTH, 110);
    _loopScrollView = [HYBLoopScrollView loopScrollViewWithFrame:loopScrollViewFrame
                                                       imageUrls:defaultImages];
    _loopScrollView.imageUrls = defaultImages;
    
    _loopScrollView.delegate = (id<HYBLoopScrollViewDelegate>)self;
    //设置pagecontroll样式
    _loopScrollView.pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"#c52720"];
    _loopScrollView.pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"#dededd"];
    _loopScrollView.timeInterval = 5;//设置间隔时间
    _loopScrollView.placeholder = [UIImage imageNamed:@"productDetail_Default_loopImage"];
    _loopScrollView.alignment = kPageControlAlignRight;//设置pagecontroll对齐方式
    [self addSubview:_loopScrollView];
//    //推荐视图
//    UIButton *leftButton = [self setButtonWithTitle:@"" backgroundColor:@"#ffffff" withTag:krecommendButtonTag];
//    [self addSubview:leftButton];
//   
//    //限量视图
//    UIButton *rightOneButton = [self setButtonWithTitle:@"" backgroundColor:@"#ffffff" withTag:klimitCountButtonTag];
//    [self addSubview:rightOneButton];
//    //两元购视图
//    UIButton *rightTwoButton = [self setButtonWithTitle:@"" backgroundColor:@"#ffffff" withTag:kyiYuanGouButtonTag];
//    [self addSubview:rightTwoButton];
////    //标题视图
////    UILabel *titleLabel = [self setLabelWithTitle:@"特价抢购" fontSize:14 textColor:@"#72706f"];
////    [self addSubview:titleLabel];
//    //添加主视图约束
//    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_loopScrollView.mas_bottom).offset(8);
//        make.left.mas_equalTo(8);
//        make.width.equalTo(rightOneButton);
//        make.height.mas_equalTo(125);
//    }];
//    [rightOneButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(leftButton);
//        make.left.equalTo(leftButton.mas_right).offset(8);
//        make.right.mas_equalTo(-8);
//        make.width.equalTo(leftButton);
//        make.height.mas_equalTo(58);
//    }];
//    [rightTwoButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(rightOneButton);
//        make.top.equalTo(rightOneButton.mas_bottom).offset(8);
//        make.height.and.width.equalTo(rightOneButton);
//    }];
////    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.top.equalTo(leftButton.mas_bottom).offset(0);
////        make.left.mas_equalTo(16);
////        make.right.mas_equalTo(- 16);
////        make.height.mas_equalTo(20);
////    }];
//    
//    //推荐子视图
//    UIView *leftKindView = [self setCombineImage:@"Nav_RightDoubleArrow" title:@"推荐"
//                                       titleFont:13 titleColor:@"#ffffff"
//                                  backgroudImage:@"#da251d"];
//    leftKindView.backgroundColor = [UIColor redColor];
//    [leftButton addSubview:leftKindView];
//    [leftKindView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.and.top.mas_equalTo(8);
//        make.width.mas_equalTo(46);
//        make.height.mas_equalTo(24);
//    }];
//    
//    UILabel *leftContentLabel  = [self setLabelWithTitle:@"精品好货" fontSize:12 textColor:@"#72706f"];
//    leftContentLabel.textAlignment = NSTextAlignmentLeft;
//    [leftButton addSubview:leftContentLabel];
//    [leftContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(leftKindView.mas_right).offset(12);
//        make.centerY.equalTo(leftKindView);
//        make.right.mas_equalTo(-2);
//    }];
//    
//    _leftImageView = [[UIImageView alloc]init];
//    _leftImageView.image = [UIImage imageNamed:@"place_image"];
//    [leftButton addSubview:_leftImageView];
//    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(leftKindView);
//        make.top.equalTo(leftKindView.mas_bottom).offset(8);
//        make.width.mas_equalTo(67);
//        make.bottom.mas_equalTo(-8);
//    }];
//    
//    _leftTitleLabel = [self setLabelWithTitle:@"" fontSize:12 textColor:@"#4d4948"];
//    _leftTitleLabel.textAlignment = NSTextAlignmentLeft;
//    _leftTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    _leftTitleLabel.numberOfLines = 0;
//    [leftButton addSubview:_leftTitleLabel];
//    [_leftTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_leftImageView.mas_right).offset(8);
//        make.top.equalTo(_leftImageView.mas_top).offset(0);
//        make.right.mas_equalTo(-3);
//        make.height.mas_equalTo(34);
//    }];
//    
//    _leftCurrentPrice = [self setLabelWithTitle:@"" fontSize:11 textColor:@"#da251d"];
//    _leftCurrentPrice.textAlignment = NSTextAlignmentLeft;
//    [leftButton addSubview:_leftCurrentPrice];
//    [_leftCurrentPrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_leftTitleLabel);
//        make.right.mas_equalTo(-4);
//        make.bottom.mas_equalTo(-19);
//        make.height.mas_equalTo(13);
//    }];
//    
//    NSString *olePrice = @"￥12.00";
//    _leftOldPrice = [self setLabelWithTitle:@"" fontSize:10 textColor:@"#72706f"];
//    _leftOldPrice.textAlignment = NSTextAlignmentLeft;
//    [leftButton addSubview:_leftOldPrice];
//    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"￥12.00"];
//    [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, olePrice.length)];
//    
//    _leftOldPrice.attributedText = attrString;
//    [_leftOldPrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_leftCurrentPrice).offset(2);
//        make.right.mas_equalTo(-4);
//        make.bottom.mas_equalTo(-7);
//        make.height.mas_equalTo(10);
//    }];
//    //限量子视图
//    
//    UIView *rightOneKindView = [self setCombineImage:@"Nav_RightDoubleArrow" title:@"限量"
//                                           titleFont:13 titleColor:@"#ffffff"
//                                      backgroudImage:@"#da251d"];
//    rightOneKindView.backgroundColor = [UIColor redColor];
//    [rightOneButton addSubview:rightOneKindView];
//    [rightOneKindView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.and.top.mas_equalTo(8);
//        make.width.mas_equalTo(46);
//        make.height.mas_equalTo(24);
//    }];
//    
//    UILabel *rightOneContentLabel  = [self setLabelWithTitle:@"限量发售" fontSize:12 textColor:@"#72706f"];
//    rightOneContentLabel.textAlignment = NSTextAlignmentLeft;
//    [rightOneButton addSubview:rightOneContentLabel];
//    [rightOneContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(rightOneKindView.mas_left).offset(0);
//        make.bottom.mas_equalTo(-6);
//        make.width.equalTo(rightOneKindView.mas_width);
//        make.height.mas_equalTo(14);
//    }];
//    
//    _rightOneImageView = [[UIImageView alloc]init];
//    _rightOneImageView.image = [UIImage imageNamed:@"place_image"];
//    [rightOneButton addSubview:_rightOneImageView];
//    [_rightOneImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(rightOneKindView.mas_right).offset(8);
//        make.top.mas_equalTo(7);
//        make.width.mas_equalTo(38);
//        make.bottom.mas_equalTo(-7);
//    }];
//
//    _rightOneTitleLabel = [self setLabelWithTitle:@"" fontSize:12 textColor:@"#4d4948"];
//    _rightOneTitleLabel.textAlignment = NSTextAlignmentLeft;
//    [rightOneButton addSubview:_rightOneTitleLabel];
//    [_rightOneTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_rightOneImageView.mas_top).offset(0);
//        make.left.equalTo(_rightOneImageView.mas_right).offset(6);
//        make.right.mas_equalTo(-2);
//        make.height.mas_equalTo(13);
//    }];
//
//    _righOneCurrentPrice = [self setLabelWithTitle:@"" fontSize:12 textColor:@"#da251d"];
//    _righOneCurrentPrice.textAlignment = NSTextAlignmentLeft;
//    [rightOneButton addSubview:_righOneCurrentPrice];
//    [_righOneCurrentPrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_rightOneTitleLabel);
//        make.right.mas_equalTo(0);
//        make.top.equalTo(_rightOneTitleLabel.mas_bottom).offset(8);
//        make.height.mas_equalTo(12);
//    }];
//    
//    NSString *rightOnePrice = @"￥12.00";
//    _righOneOldPrice = [self setLabelWithTitle:@"" fontSize:10 textColor:@"#72706f"];
//    _righOneOldPrice.textAlignment = NSTextAlignmentLeft;
//    [rightOneButton addSubview:_righOneOldPrice];
//    NSMutableAttributedString *rightOneattrString = [[NSMutableAttributedString alloc] initWithString:@"￥12.00"];
//    [rightOneattrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, rightOnePrice.length)];
//    
//   // _righOneOldPrice.attributedText = attrString;
//    [_righOneOldPrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_rightOneTitleLabel);
//        make.right.mas_equalTo(0);
//        make.bottom.mas_equalTo(-6);
//        make.height.mas_equalTo(10);
//    }];
//    //一元购子视图
//
//    UIView *rightTwoKindView = [self setCombineImage:@"Nav_RightDoubleArrow" title:@"一元购"
//                                           titleFont:12 titleColor:@"#ffffff"
//                                      backgroudImage:@"#da251d"];
//    rightTwoKindView.backgroundColor = [UIColor redColor];
//    [rightTwoButton addSubview:rightTwoKindView];
//    [rightTwoKindView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.and.top.mas_equalTo(8);
//        make.width.mas_equalTo(54);
//        make.height.mas_equalTo(24);
//    }];
//    
//    UILabel *rightTwoContentLabel  = [self setLabelWithTitle:@"全部两元" fontSize:12 textColor:@"#72706f"];
//    rightTwoContentLabel.textAlignment = NSTextAlignmentLeft;
//    [rightTwoButton addSubview:rightTwoContentLabel];
//    [rightTwoContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(rightTwoKindView.mas_left).offset(0);
//        make.bottom.mas_equalTo(-6);
//        make.width.equalTo(rightTwoKindView.mas_width);
//        make.height.mas_equalTo(14);
//    }];
//
//    _rightTwoImageView = [[UIImageView alloc]init];
//    _rightTwoImageView.image = [UIImage imageNamed:@"place_image"];
//    [rightTwoButton addSubview:_rightTwoImageView];
//    [_rightTwoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(rightTwoContentLabel.mas_right).offset(8);
//        make.top.mas_equalTo(7);
//        make.width.mas_equalTo(38);
//        make.bottom.mas_equalTo(-7);
//    }];
//
//    _rightTwoTitleLabel = [self setLabelWithTitle:@"" fontSize:12 textColor:@"#4d4948"];
//    _rightTwoTitleLabel.textAlignment = NSTextAlignmentLeft;
//    [rightTwoButton addSubview:_rightTwoTitleLabel];
//    [_rightTwoTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_rightTwoImageView.mas_top).offset(0);
//        make.left.equalTo(_rightTwoImageView.mas_right).offset(6);
//        make.right.mas_equalTo(-2);
//        make.height.mas_equalTo(13);
//    }];
//
//    _rightTwoCurrentPrice = [self setLabelWithTitle:@"" fontSize:12 textColor:@"#da251d"];
//    _rightTwoCurrentPrice.textAlignment = NSTextAlignmentLeft;
//    [rightTwoButton addSubview:_rightTwoCurrentPrice];
//    [_rightTwoCurrentPrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_rightTwoTitleLabel);
//        make.right.mas_equalTo(0);
//        make.top.equalTo(_rightTwoTitleLabel.mas_bottom).offset(8);
//        make.height.mas_equalTo(12);
//    }];
//
//    NSString *rightTwoPrice = @"￥12.00";
//    _rightTwoOldPrice = [self setLabelWithTitle:@"" fontSize:10 textColor:@"#72706f"];
//    _rightTwoOldPrice.textAlignment = NSTextAlignmentLeft;
//    [rightTwoButton addSubview:_rightTwoOldPrice];
//    NSMutableAttributedString *rightTwoattrString = [[NSMutableAttributedString alloc] initWithString:@"￥12.00"];
//    [rightTwoattrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, rightTwoPrice.length)];
//    
//    //_rightTwoOldPrice.attributedText = attrString;
//    [_rightTwoOldPrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_rightTwoTitleLabel);
//        make.right.mas_equalTo(0);
//        make.bottom.mas_equalTo(-6);
//        make.height.mas_equalTo(10);
//    }];
//

}

#pragma mark --  HYBLoopScrollViewDelegate
- (void)didSelectItemIndex:(NSInteger)index{
    if (_delegate && [_delegate respondsToSelector:@selector(selectedLoopImageIndex:)]) {
        [_delegate selectedLoopImageIndex:index];
    }
}

#pragma mark -- 根据属性生产对象
//生成label对象
- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.backgroundColor = [UIColor clearColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}
//生成button对像
- (UIButton *)setButtonWithTitle:(NSString*)title backgroundColor:(NSString*)backgroundColor withTag:(NSInteger)tag{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:backgroundColor] frame:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"] frame:CGRectMake(0, 0, 1, 1)] forState:UIControlStateHighlighted];
   // button.backgroundColor = [UIColor colorWithHexString:backgroundColor];
    button.tag = tag;
    
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:self action:@selector(beatTeHuiEvent:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}
//图片和标题的组合视图
- (UIView*)setCombineImage:(NSString*)imageName title:(NSString*)title titleFont:(NSInteger)titleFont titleColor:(NSString*)titleColor backgroudImage:(NSString*)backgroundImageName{
    //标题
    UIView *view = [[UIView alloc]init];
    [view.layer setMasksToBounds:YES];
    [view.layer setCornerRadius:3];
    view.backgroundColor = [UIColor colorWithHexString:backgroundImageName];
    
    CGSize titleSize = [title sizeWithFont:[UIFont systemFontOfSize:titleFont] maxSize:CGSizeMake(120, 18)];
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.font = [UIFont systemFontOfSize:titleFont];
    titleLabel.text = title;
    titleLabel.textColor = [UIColor colorWithHexString:titleColor];
    titleLabel.frame = CGRectMake(4, 24/2 - titleSize.height/2, titleSize.width, titleFont);
    [view addSubview:titleLabel];
    //图片
    UIImage *image = [UIImage imageNamed:imageName];
    UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
    imageView.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame) + 2, 24/2  - 5, 10, 10);
    [view addSubview:imageView];
    view.userInteractionEnabled = YES;
    return view;
}
#pragma mark -- button action
- (void)beatTeHuiEvent:(id)sender {
    UIButton *butotn = (UIButton*)sender;
    if (_delegate && [_delegate respondsToSelector:@selector(selectedTeHuiTypeWithIndex:)]) {
        [_delegate selectedTeHuiTypeWithIndex:butotn.tag - 100];
    }
}
#pragma makr -- 数据初始化视图
//设置轮播图
- (void)resetLoopScrollViewImages:(NSArray*)imageUrls {
    self.loopScrollView.imageUrls = imageUrls;
}
//设置品种信息
- (void)resetKindInfo:(NSDictionary*)dataDic {
    if (dataDic.count != 0) {
        NSString *moneyFuHao = @"￥";
        if (![[dataDic objectForKey:@"TYPE2"] isKindOfClass:[NSNull class]] && [[dataDic objectForKey:@"TYPE2"] count] != 0 && [dataDic objectForKey:@"TYPE2"] != nil) {
            NSDictionary *tuiJianDataDic = [dataDic objectForKey:@"TYPE2"];
            [self.leftImageView setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[tuiJianDataDic objectForKey:@"THUMB"]]]
                               placeholderImage:[UIImage imageNamed:@"place_image"]];
            self.leftTitleLabel.text = [NSString stringISNull:[tuiJianDataDic objectForKey:@"NAME"]];
            self.leftCurrentPrice.text = [NSString stringWithFormat:@"%@%.2f",moneyFuHao,[[NSString stringTransformObject:[tuiJianDataDic objectForKey:@"SELLPRICE"]] floatValue]];
            NSString *leftOldPriceStr = [NSString stringWithFormat:@"%@%.2f",moneyFuHao,[[NSString stringTransformObject:[tuiJianDataDic objectForKey:@"MARKETPRICE"]] floatValue]];
            NSMutableAttributedString *leftOldPriceAttrString = [[NSMutableAttributedString alloc] initWithString:leftOldPriceStr];
            [leftOldPriceAttrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, leftOldPriceStr.length)];
            self.leftOldPrice.attributedText =leftOldPriceAttrString;
        } else {
            self.leftTitleLabel.text = @"";
            self.leftCurrentPrice.text = @"";
            self.leftOldPrice.text = @"";
            [self.leftImageView setImageWithURL:[NSURL URLWithString:@""]
                               placeholderImage:[UIImage imageNamed:@"place_image"]];
        }
        
        if (![[dataDic objectForKey:@"TYPE3"] isKindOfClass:[NSNull class]] && [[dataDic objectForKey:@"TYPE3"] count] != 0 && [dataDic objectForKey:@"TYPE3"] != nil) {
            NSDictionary *limitCountDataDic = [dataDic objectForKey:@"TYPE3"];
            [self.rightOneImageView setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[limitCountDataDic objectForKey:@"THUMB"]]]
                                   placeholderImage:[UIImage imageNamed:@"place_image"]];
            
            self.rightOneTitleLabel.text = [NSString stringISNull:[limitCountDataDic objectForKey:@"NAME"]];
            self.righOneCurrentPrice.text = [NSString stringWithFormat:@"%@%.2f",moneyFuHao,[[NSString stringTransformObject:[limitCountDataDic objectForKey:@"SELLPRICE"]] floatValue]];
            NSString *rightOneOldPriceStr = [NSString stringWithFormat:@"%@%.2f",moneyFuHao,[[NSString stringTransformObject:[limitCountDataDic objectForKey:@"MARKETPRICE"]] floatValue]];
            NSMutableAttributedString *rightOneOldPriceAttrString = [[NSMutableAttributedString alloc] initWithString:rightOneOldPriceStr];
            [rightOneOldPriceAttrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, rightOneOldPriceStr.length)];
            self.righOneOldPrice.attributedText = rightOneOldPriceAttrString;
        } else {
            self.rightOneTitleLabel.text = @"";
            self.righOneCurrentPrice.text = @"";
            self.righOneOldPrice.text = @"";
            [self.rightOneImageView setImageWithURL:[NSURL URLWithString:@""]
                               placeholderImage:[UIImage imageNamed:@"place_image"]];
        }
        
       
        if (![[dataDic objectForKey:@"TYPE4"] isKindOfClass:[NSNull class]] && [[dataDic objectForKey:@"TYPE4"] count] != 0 && [dataDic objectForKey:@"TYPE4"] != nil) {
            
            NSDictionary *yiYuanBuyDataDic = [dataDic objectForKey:@"TYPE4"];
            [self.rightTwoImageView setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[yiYuanBuyDataDic objectForKey:@"THUMB"]]]
                                   placeholderImage:[UIImage imageNamed:@"place_image"]];
            self.rightTwoTitleLabel.text = [NSString stringISNull:[yiYuanBuyDataDic objectForKey:@"NAME"]];
            self.rightTwoCurrentPrice.text = [NSString stringWithFormat:@"%@%.2f",moneyFuHao,[[NSString stringTransformObject:[yiYuanBuyDataDic objectForKey:@"SELLPRICE"]] floatValue]];
            NSString *rightTwoOldPriceStr = [NSString stringWithFormat:@"%@%.2f",moneyFuHao,[[NSString stringTransformObject:[yiYuanBuyDataDic objectForKey:@"MARKETPRICE"]] floatValue]];
            NSMutableAttributedString *rightTwoOldPriceAttrString = [[NSMutableAttributedString alloc] initWithString:rightTwoOldPriceStr];
            [rightTwoOldPriceAttrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, rightTwoOldPriceStr.length)];
            self.rightTwoOldPrice.attributedText = rightTwoOldPriceAttrString;
        } else {
            self.rightTwoTitleLabel.text = @"";
            self.rightTwoCurrentPrice.text = @"";
            self.rightTwoOldPrice.text = @"";
            [self.rightTwoImageView setImageWithURL:[NSURL URLWithString:@""]
                               placeholderImage:[UIImage imageNamed:@"place_image"]];
            
        }
       
    }
}

@end
