//
//  Strore_Root_Controller.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Strore_Root_Controller
 Created_Date： 20151106
 Created_People： GT
 Function_description： 便利店页面
 ***************************************/

#import "BaseViewController.h"

@interface Strore_Root_Controller : BaseViewController

@end
