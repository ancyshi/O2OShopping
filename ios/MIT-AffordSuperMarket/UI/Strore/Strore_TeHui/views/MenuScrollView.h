//
//  MenuScrollView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/7.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MenuScrollView
 Created_Date： 20151107
 Created_People： GT
 Function_description： 自定义menuScrollView
 ***************************************/

#import <UIKit/UIKit.h>
@protocol MenuScrollViewDelegate <NSObject>
- (void)selectedMenuItemAtIndex:(NSInteger)index;
@end
@interface MenuScrollView : UIScrollView

@property(nonatomic ,unsafe_unretained)id<MenuScrollViewDelegate>menuScrollDelegate;
/**   外部改变菜单索引  */
@property(nonatomic) NSInteger selectIndex;
/**  菜单按钮背景颜色属性  */
@property (nonatomic, strong) UIColor *tabBackgroundColor;
@property (nonatomic, strong) UIColor *tabSelectedBackgroundColor;

/**  菜单按钮背景图片属性  */
@property (nonatomic, strong) UIImage *tabBackgroundImage;
@property (nonatomic, strong) UIImage *tabSelectedBackgroundImage;

/**  菜单按钮的标题颜色属性   */
@property (nonatomic, strong) UIColor *tabTitleColor;
@property (nonatomic, strong) UIColor *tabSelectedTitleColor;

/**  是否显示垂直分割线  默认显示 */
@property (nonatomic, assign) BOOL showVerticalLine;

/**  垂直分割线颜色  */
@property (nonatomic, assign) UIColor *verticalLineColor;

/**  垂直分割线背景图片  */
@property (nonatomic, assign) UIImage *verticalLineImage;

/**  是否显示底部横线  默认显示  */
@property (nonatomic, assign) BOOL showBottomLine;

/**  底部横线颜色  */
@property (nonatomic, assign) UIColor *bottomLineColor;

/**  底部横线背景图片  */
@property (nonatomic, assign) UIImage *bottomLineImage;


//初始化方法
- (id)initWithFrame:(CGRect)frame titles:(NSArray*)titles dataScrollView:(UIScrollView *)scrollView;
@end
