//
//  ServerDetailPriceIntroducedCell.m
//  ServerDetailDemo
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "ServerDetailPriceIntroducedCell.h"
#import "Global.h"
#define VIEW_WIDTH [UIScreen mainScreen].bounds.size.width//屏幕宽度
#define VIEW_HEIGHT [UIScreen mainScreen].bounds.size.height//屏幕高度
#define kleftSpace 15
#define ktopSpace 5
#define kVSpace 5
#define klabelHeight 30
//untils
#import "UIColor+Hex.h"
@interface ServerDetailPriceIntroducedCell ()
@property(nonatomic, strong)UILabel *titleLabel;

@property(nonatomic, strong)UILabel *contentLabel;
@end
@implementation ServerDetailPriceIntroducedCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    //标题
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                          , ktopSpace
                                                          , VIEW_WIDTH - 2*kleftSpace
                                                           , klabelHeight)];
    _titleLabel.font = [UIFont systemFontOfSize:FontSize(18)];
    _titleLabel.text = @"价格说明：";
    _titleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_titleLabel];
    //内容
    _contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_titleLabel.frame)
                                                            , CGRectGetMaxY(_titleLabel.frame)
                                                            , CGRectGetWidth(_titleLabel.bounds)
                                                             ,30)];
    _contentLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    _contentLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    _contentLabel.numberOfLines = 0;
    _contentLabel.lineBreakMode = NSLineBreakByCharWrapping;
    [self.contentView addSubview:_contentLabel];
    
}
- (void)setContent:(NSString *)content {
   
    if (content.length != 0) {
        self.contentLabel.text = content;
        CGFloat contentW = VIEW_WIDTH - 2 * kleftSpace; // 屏幕宽度减去左右间距
        CGFloat contentH = [content boundingRectWithSize:CGSizeMake(contentW, MAXFLOAT)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:FontSize(15)]}
                                                      context:nil].size.height;
        CGRect frame = self.contentLabel.frame;
        frame.size.height = contentH;
        self.contentLabel.frame = frame;

    }
     _content = content;
    _titleLabel.text=_title;
}
- (CGFloat)cellFactHeight {
    
    return CGRectGetMaxY(_contentLabel.frame) - CGRectGetMinY(_titleLabel.frame) + 2*ktopSpace + 5;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

@end
