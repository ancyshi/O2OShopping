//
//  BaiDuLocationViewController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/21.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： SearchHistoryController
 Created_Date： 20151103
 Created_People： GT
 Function_description：定位页
 ***************************************/
#import "BaseViewController.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Cloud/BMKCloudSearchComponent.h>
typedef NS_ENUM(NSInteger, BaiDuLocationViewControllerSupperControllerType) {
    kIntroduction_ControllerSupperController = 0,//重引导页进入
    kOther_ControllerSupperController //从其他页面进入
};

@interface BaiDuLocationViewController : BaseViewController<BMKLocationServiceDelegate,BMKCloudSearchDelegate>
{
    
    BMKCloudSearch* _search;
    BMKLocationService* _locService;
    
}

@property(nonatomic)BaiDuLocationViewControllerSupperControllerType suppperControllerType;
@end
