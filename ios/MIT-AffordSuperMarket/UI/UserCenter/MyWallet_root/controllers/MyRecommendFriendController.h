//
//  MyRecommendFriendController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： MyWalletViewController
 Created_Date： 20160504
 Created_People： JSQ
 Function_description： 推荐的好友
 ***************************************/#import "BaseViewController.h"

@interface MyRecommendFriendController : BaseViewController
@property(nonatomic,strong)NSString *recommendFriendNum;
@property (nonatomic ,strong)NSString *userLevel;
@end
