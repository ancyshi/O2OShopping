//
//  SettingFooterView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SettingFooterView.h"
#define ktopSpace 40
#define kCommonHeight 44
#define kleftSpace 15
//untils
#import "UIColor+Hex.h"
#import "Global.h"
#import "NSString+TextSize.h"
#import "UIImage+ColorToImage.h"
@interface SettingFooterView ()
@property(nonatomic, strong)UILabel *telePhoneNunberLabel;
@property(nonatomic, strong)UILabel *openTimeLabel;
@property(nonatomic, strong)UIButton *quitAccountBtn;
@end
@implementation SettingFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super  initWithFrame:frame];
    if (self) {
        [self setupSubView];
        self.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    }
    return self;
}

#pragma mark -- 初始化子视图
- (void)setupSubView {
    //客户电话
    NSString *telePhoneText = @"客服电话：0371-86563519";
    CGSize telePhoneTextSize = [telePhoneText sizeWithFont:[UIFont systemFontOfSize:FontSize(17)] maxSize:CGSizeMake(CGRectGetWidth(self.bounds), kCommonHeight)];
    _telePhoneNunberLabel = [[UILabel alloc]initWithFrame:CGRectMake((CGRectGetWidth(self.bounds) - telePhoneTextSize.width)/2
                                                                    , ktopSpace
                                                                    , telePhoneTextSize.width
                                                                     , kCommonHeight)];
    _telePhoneNunberLabel.font = [UIFont systemFontOfSize:FontSize(17)];
    NSMutableAttributedString *attributedStr = [self dealWithLableFirstFontSize:FontSize(17) secondFontSize:FontSize(17) firstTextColor:@"#000000" secondTextColor:@"#c52720" firstRang:NSMakeRange(0, 5) content:telePhoneText];
    _telePhoneNunberLabel.attributedText = attributedStr;
    [self addSubview:_telePhoneNunberLabel];
    //服务时间
     NSString *openTimeStr = @"服务时间：9:00-17:30";
    CGSize openTimeStrSize = [openTimeStr sizeWithFont:[UIFont systemFontOfSize:FontSize(17)] maxSize:CGSizeMake(CGRectGetWidth(self.bounds), kCommonHeight)];
    _openTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake((CGRectGetWidth(self.bounds) - openTimeStrSize.width)/2
                                                             , CGRectGetMaxY(_telePhoneNunberLabel.frame)
                                                             , openTimeStrSize.width
                                                              , kCommonHeight)];
    _openTimeLabel.font = [UIFont systemFontOfSize:FontSize(17)];
    _openTimeLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    _openTimeLabel.text = openTimeStr;
    [self addSubview:_openTimeLabel];
    
    //退出登录按钮视图
    _quitAccountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _quitAccountBtn.frame = CGRectMake(kleftSpace
                                       , CGRectGetMaxY(_openTimeLabel.frame) + kCommonHeight
                                       , CGRectGetWidth(self.bounds) - 2*kleftSpace
                                       , kCommonHeight);
    _quitAccountBtn.titleLabel.font = [UIFont systemFontOfSize:FontSize(17)];
    UIImage *loginImage = [UIImage imageNamed:@"login_Btn"];
    loginImage = [loginImage stretchableImageWithLeftCapWidth:6 topCapHeight:6];
    [_quitAccountBtn setBackgroundImage:loginImage forState:UIControlStateNormal];
    [_quitAccountBtn setTitle:@"退出当前账号" forState:UIControlStateNormal];
    [_quitAccountBtn setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
    [_quitAccountBtn.layer setMasksToBounds:YES];
    [_quitAccountBtn addTarget:self action:@selector(quitLoginEvent) forControlEvents:UIControlEventTouchUpInside];
    [_quitAccountBtn.layer setCornerRadius:3];
    [self addSubview:_quitAccountBtn];

}

- (NSMutableAttributedString *)dealWithLableFirstFontSize:(CGFloat)firstFontSize secondFontSize:(CGFloat)secondFontSize firstTextColor:(NSString*)firstTextColor secondTextColor:(NSString*)secondTextColor firstRang:(NSRange)range content:(NSString*)content{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:content];
    //设置：在0-3个单位长度内的内容显示成红色
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:firstTextColor] range:range];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:secondTextColor] range:NSMakeRange(range.length, content.length -range.length)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:firstFontSize] range:range];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:secondFontSize] range:NSMakeRange(range.length, content.length - range.length)];
    return str;
}
#pragma mark -- setter method
- (void)setTelePhoneNumber:(NSString *)telePhoneNumber {
    NSString *clientTelPhoneStr = @"客户电话：";
    clientTelPhoneStr = [NSString stringWithFormat:@"%@%@",clientTelPhoneStr,telePhoneNumber];
    CGSize clientTelPhoneStrSize = [clientTelPhoneStr sizeWithFont:[UIFont systemFontOfSize:FontSize(17)] maxSize:CGSizeMake(CGRectGetWidth(self.bounds), kCommonHeight)];
    CGRect frame = self.telePhoneNunberLabel.frame;
    frame.origin.x = (CGRectGetWidth(self.bounds) - clientTelPhoneStrSize.width)/2;
    frame.size.width = clientTelPhoneStrSize.width;
    self.telePhoneNunberLabel.frame = frame;
}

- (void)setOpenTime:(NSString *)openTime {
    NSString *opentimeStr = @"服务时间：";
    opentimeStr = [NSString stringWithFormat:@"%@%@",opentimeStr,openTime];
    CGSize openTimeStrSize = [opentimeStr sizeWithFont:[UIFont systemFontOfSize:FontSize(17)] maxSize:CGSizeMake(CGRectGetWidth(self.bounds), kCommonHeight)];
    CGRect frame = self.openTimeLabel.frame;
    frame.origin.x = (CGRectGetWidth(self.bounds) - openTimeStrSize.width)/2;
    frame.size.width = openTimeStrSize.width;
    self.openTimeLabel.frame = frame;
}

#pragma mark -- buttonAction
- (void)quitLoginEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(quitUserLogin)]) {
        [_delegate quitUserLogin];
    }
}
- (void)changeSuViewShowWithLoginStatus:(BOOL)isLogin {
    self.quitAccountBtn.hidden = !isLogin;
}
@end
