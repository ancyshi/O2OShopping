//
//  StoreShowCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "StoreShowCell.h"
#define krightSpace 10
#import "UIColor+Hex.h"
#import "Global.h"

@interface StoreShowCell ()
@property(nonatomic, strong)UIButton *callBtn;
@end
@implementation StoreShowCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}
- (void)awakeFromNib {
    self.contentLabel.textColor = [UIColor colorWithHexString:@"#555555"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)setModel:(StoreShowModel *)model {
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.lineBreakMode = NSLineBreakByCharWrapping;
    
    
    if (model.buttonImageName.length != 0) {
        self.contentLabel.attributedText = [self dealWithLableFirstFontSize:FontSize(16) secondFontSize:FontSize(16) firstTextColor:@"#555555" secondTextColor:@"#0094dd" firstRang:NSMakeRange(0, 3) content:model.content];
        [self.callBtn setImage:[UIImage imageNamed:model.buttonImageName] forState:UIControlStateNormal];
    } else {
        self.contentLabel.text = model.content;
    }
}

- (UIButton*)callBtn {
    if (!_callBtn) {
        _callBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _callBtn.frame = CGRectMake(VIEW_WIDTH - 44 - krightSpace, 0, 44, 54);
        [_callBtn addTarget:self action:@selector(callTelPhoneNum) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_callBtn];
    }
    return _callBtn;
}

- (NSMutableAttributedString *)dealWithLableFirstFontSize:(CGFloat)firstFontSize secondFontSize:(CGFloat)secondFontSize firstTextColor:(NSString*)firstTextColor secondTextColor:(NSString*)secondTextColor firstRang:(NSRange)range content:(NSString*)content{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:content];
    //设置：在0-3个单位长度内的内容显示成红色
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:firstTextColor] range:range];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:secondTextColor] range:NSMakeRange(range.length, content.length -range.length)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:firstFontSize] range:range];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:secondFontSize] range:NSMakeRange(range.length, content.length - range.length)];
    return str;
}
#pragma mark -- button Action
- (void)callTelPhoneNum {
    if (_delegate && [_delegate respondsToSelector:@selector(callTelePhoneWithIndexPath:)]) {
        [_delegate callTelePhoneWithIndexPath:self.indexpath];
    }
   
}
@end
