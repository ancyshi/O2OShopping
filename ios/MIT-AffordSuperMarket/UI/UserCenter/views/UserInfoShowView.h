//
//  UserInfoShowView.h
//  BG-Clound
//
//  Created by Mega on 14/11/1.
//  Copyright (c) 2014年 zd. All rights reserved.
/***************************************
 ClassName： UserInfoShowView
 Created_Date： 20151108
 Created_People： GT
 Function_description： 个人中心头部信息
 ***************************************/

#import <UIKit/UIKit.h>
@protocol UserInfoShowViewDelegate  <NSObject>
- (void)userUnLogin;
- (void)manageUserInfo;
- (void)membersJump;
- (void)manageUserRecivedAddress;
- (void)lookGuanZhun;
- (void)lookLiuLanHistory;
@end


@interface UserInfoShowView : UIView
@property (nonatomic,unsafe_unretained) id<UserInfoShowViewDelegate>delegate;
@property(nonatomic, strong)UIImage *backImage;//背景图片
@property(nonatomic, strong)UIImageView *userImageView;
@property(nonatomic, strong)NSString *title;
@property(nonatomic, strong)NSString *subTitle;
@property(nonatomic, strong)NSString *guanZhuCountText;
@property(nonatomic, strong)NSString *historyCountText;
@property(nonatomic, strong)NSString *membersBtnTitle;
@property(nonatomic, strong)UIImageView *CrownLogo;//皇冠
//改变用户头部视图
- (void)showUserInfoWithLoginStatus:(BOOL)islogoin;
@end
