//
//  MyMarkAndHistory_Cell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/17.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyMarkAndHistory_Cell.h"
//untils
#import "UIColor+Hex.h"
#import "Global.h"
#import "UIImage+ColorToImage.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+Conversion.h"
#import "KZLinkLabel.h"
#define kleftSpace 15
#define ktopSpace 15
#define kimageWidth HightScalar(119)
@interface MyMarkAndHistory_Cell ()
@property(nonatomic, strong)UIImageView *imageview;
@property(nonatomic, strong)KZLinkLabel *titleLabel;
@property(nonatomic, strong)UILabel *currentPriceLabel;
@property(nonatomic, strong)UILabel *oldPriceLabel;

@end
@implementation MyMarkAndHistory_Cell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.backgroundColor=[UIColor whiteColor];
        [self setUpSubview];
    }

    return self;
    
}

#pragma mark -- 初始化视图
- (void)setUpSubview {
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 130)];
//    view.backgroundColor = [UIColor whiteColor];
//    [self.contentView addSubview:view];
    _imageview = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace, ktopSpace, kimageWidth, kimageWidth)];
    [_imageview.layer setBorderWidth:0.7];
    _imageview.layer.masksToBounds = YES;
    _imageview.layer.cornerRadius = 4.0;
    [_imageview.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    [self.contentView addSubview:_imageview];
    
    _titleLabel = [[KZLinkLabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_imageview.frame) + kleftSpace
                                                               , CGRectGetMinY(_imageview.frame)
                                                               , VIEW_WIDTH - 3*kleftSpace - kimageWidth
                                                               , HightScalar(50))];
    _titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    _titleLabel.automaticLinkDetectionEnabled = YES;
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _titleLabel.numberOfLines = 2;
    _titleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    [self.contentView addSubview:_titleLabel];

    
    _currentPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_titleLabel.frame) 
                                                                 , CGRectGetMaxY(_titleLabel.frame) +5
                                                                 , CGRectGetWidth(_titleLabel.bounds)
                                                                  , HightScalar(30))];
    _currentPriceLabel.backgroundColor = [UIColor clearColor];
    _currentPriceLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    _currentPriceLabel.textAlignment = NSTextAlignmentLeft;
    _currentPriceLabel.textColor = [UIColor colorWithHexString:@"#c52720"];
   
    [self.contentView addSubview:_currentPriceLabel];
    
    _oldPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_currentPriceLabel.frame)
                                                             , CGRectGetMaxY(_currentPriceLabel.frame) + 5
                                                             , CGRectGetWidth(_currentPriceLabel.bounds)
                                                             , HightScalar(30))];
    _oldPriceLabel.backgroundColor = [UIColor clearColor];
    _oldPriceLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    _oldPriceLabel.textAlignment = NSTextAlignmentLeft;
    _oldPriceLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    [self.contentView addSubview:_oldPriceLabel];
    
}

#pragma mark -- 填充数据

- (void)updataWithDataDic:(NSDictionary*)dataDic {
    if (dataDic.count != 0) {
        
        _imageview.image =[UIImage imageWithData:[dataDic objectForKey:@"picture"]];
       
        NSString *isSelf =[NSString stringTransformObject:[dataDic objectForKey:@"isProprietary"]];
        if ([isSelf isEqualToString:@"1"]) {
            NSString *emojiString = [NSString stringWithFormat:@"[zhiying] %@",[NSString stringTransformObject:[dataDic objectForKey:@"name"]]];
            UIFont *font = [UIFont systemFontOfSize:FontSize(14)];
            NSDictionary *attributes = @{NSFontAttributeName: font};
            NSAttributedString *attributedString = [NSAttributedString emotionAttributedStringFrom:emojiString attributes:attributes];
            
            self.titleLabel.attributedText =attributedString;
        }else{
            _titleLabel.text = [dataDic objectForKey:@"name"];
        }
        if ([[dataDic objectForKey:@"goodID"] length]!=0) {
            _currentPriceLabel.text =[NSString stringWithFormat:@"￥%@",[dataDic objectForKey:@"price"]];
            
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%@",[dataDic objectForKey:@"oldprice"]]];
            [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, [[dataDic objectForKey:@"oldprice"] length]+1)];
            _oldPriceLabel.attributedText = attrString;
        }else if([[dataDic objectForKey:@"serviceshopID"] length]!=0)
        {
             _currentPriceLabel.textColor = [UIColor colorWithHexString:@"#999999"];
            NSMutableAttributedString *str=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"累计服务:%@次",[NSString stringTransformObject:[dataDic objectForKey:@"sercvicenum"]]]];
            
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#ff5555"] range:NSMakeRange(5, str.length-6)];
            
            _currentPriceLabel.attributedText=str;
            _oldPriceLabel.text=[dataDic objectForKey:@"serciceshopdetail"];
        }
       
        
    }
}
- (void)updataCollectionDataDic:(NSDictionary*)dataDic {
    if (dataDic.count != 0) {
        [self.imageview setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[NSString stringTransformObject:[dataDic objectForKey:@"THUMB"]]]]
                       placeholderImage:[UIImage imageNamed:@"place_image"]];
        NSString *isSelf =[NSString stringTransformObject:[dataDic objectForKey:@"ISSELF"]];
        if ([isSelf isEqualToString:@"1"]) {
            NSString *emojiString = [NSString stringWithFormat:@"[zhiying] %@",[NSString stringTransformObject:[dataDic objectForKey:@"NAME"]]];
            UIFont *font = [UIFont systemFontOfSize:FontSize(14)];
            NSDictionary *attributes = @{NSFontAttributeName: font};
            NSAttributedString *attributedString = [NSAttributedString emotionAttributedStringFrom:emojiString attributes:attributes];
            
            self.titleLabel.attributedText =attributedString;
        }else{
            _titleLabel.text = [NSString stringTransformObject:[dataDic objectForKey:@"NAME"]];
        }
        _currentPriceLabel.text =[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"SELLPRICE"]] floatValue]] ;
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"MARKETPRICE"]] floatValue]]];
        [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, attrString.length)];
        _oldPriceLabel.attributedText = attrString;
    }
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

@end
