//
//  SettingFooterView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SettingFooterView
 Created_Date： 20151123
 Created_People： GT
 Function_description： 设置页面底部视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol SettingFooterViewDelegate <NSObject>
- (void)quitUserLogin;
@end
@interface SettingFooterView : UIView
@property(nonatomic, unsafe_unretained)id<SettingFooterViewDelegate>delegate;
@property(nonatomic, strong)NSString *telePhoneNumber;
@property(nonatomic, strong)NSString *openTime;
- (void)changeSuViewShowWithLoginStatus:(BOOL)isLogin;
@end
