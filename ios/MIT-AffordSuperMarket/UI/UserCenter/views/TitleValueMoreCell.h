//
//  MyMarkAndHistory_Cell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/17.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： TitleValueMoreCell
 Created_Date： 20151118
 Created_People： GT
 Function_description： 我的账户里面带有右labelcell
 ***************************************/


#define kCellIdentifier_TitleValueMore @"TitleValueMoreCell"

#import <UIKit/UIKit.h>

@interface TitleValueMoreCell : UITableViewCell
- (void)setTitleStr:(NSString *)title valueStr:(NSString *)value;
@end
