

#define kimgeHight HightScalar(50)
#define kcellHeight HightScalar(72)
#define kleftSpace 15
#define ktopSpace 12
#import "Global.h"
#import "TitleRImageMoreCell.h"

@interface TitleRImageMoreCell ()
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIImageView *userIconView;
@end
@implementation TitleRImageMoreCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        if (!_titleLabel) {
            _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kleftSpace, 0 , 100, kcellHeight)];
            _titleLabel.backgroundColor = [UIColor clearColor];
            _titleLabel.font = [UIFont systemFontOfSize:FontSize(17)];
            _titleLabel.textColor = [UIColor blackColor];
            [self.contentView addSubview:_titleLabel];
        }
        if (!_userIconView) {
            _userIconView = [[UIImageView alloc] initWithFrame:CGRectMake((VIEW_WIDTH - 50)- kleftSpace- 30
                                                                          , ktopSpace
                                                                          , kimgeHight
                                                                          , kimgeHight)];
            [_userIconView.layer setMasksToBounds:YES];
            [_userIconView.layer setCornerRadius:kimgeHight/2];
            
            [self.contentView addSubview:_userIconView];
        }
    }
    return self;
}
- (void)setUserImage:(UIImage *)userImage {
    _userIconView.image = userImage;
}
- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
}
@end
