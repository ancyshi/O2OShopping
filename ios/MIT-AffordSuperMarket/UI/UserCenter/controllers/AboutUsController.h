//
//  AboutUsController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： AboutUsController
 Created_Date： 20151123
 Created_People： GT
 Function_description： 关于我们界面
 ***************************************/

#import "BaseViewController.h"

@interface AboutUsController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIButton *servetItemsBtn;
@property (weak, nonatomic) IBOutlet UIButton *yinDaoYeBtn;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *banQuanLabel;

@end
