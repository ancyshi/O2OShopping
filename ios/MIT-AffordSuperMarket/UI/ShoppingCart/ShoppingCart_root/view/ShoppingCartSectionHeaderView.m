//
//  ShoppingCartSectionHeaderView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/20.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "ShoppingCartSectionHeaderView.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
//model
#import "ShoppingCartSectionHeaderModel.h"
#define ksepLineHight 0.5
@interface ShoppingCartSectionHeaderView ()
@property(nonatomic,strong)UIButton *selectedButton;
@property(nonatomic,strong)UILabel *storeNameLabel;
@property(nonatomic, strong)ShoppingCartSectionHeaderModel *sectionHeaderModel;
@end
@implementation ShoppingCartSectionHeaderView
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    //上部分割线
    UIImage *sepLineImage = [UIImage imageNamed:@"gray_line"];
    UIImageView *sepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
    sepLineImageView.frame = CGRectMake(0, 0, VIEW_WIDTH, ksepLineHight);
    [self.contentView addSubview:sepLineImageView];
    
    
    _selectedButton =[UIButton buttonWithType:UIButtonTypeCustom];
    _selectedButton.frame =CGRectMake(0, (HightScalar(50 - ksepLineHight)-HightScalar(40))/2, HightScalar(40), HightScalar(40));
    [_selectedButton setImage:[UIImage imageNamed:@"checkBoxNormalImage"] forState:UIControlStateNormal];
    [_selectedButton setImage:[UIImage imageNamed:@"checkBoxSelectdImage"] forState:UIControlStateSelected];
    _selectedButton.tag = 100+self.section;
    [_selectedButton addTarget:self action:@selector(classifyButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_selectedButton];
    
    //便利店名字
    _storeNameLabel =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_selectedButton.frame)+10
                                                              , CGRectGetMaxY(sepLineImageView.frame)
                                                              ,VIEW_WIDTH -15-15-10-CGRectGetWidth(_selectedButton.bounds)
                                                              , HightScalar(50) - ksepLineHight)];
    _storeNameLabel.textColor = [UIColor colorWithHexString:@"#000000"];
    _storeNameLabel.font =[UIFont systemFontOfSize:FontSize(17)];
    [self.contentView addSubview:_storeNameLabel];
}
#pragma mark -- button Action
- (void)classifyButtonClick:(id)sender {
    UIButton *button = (UIButton*)sender;
    button.selected = !button.selected;
    if (_delegate && [_delegate respondsToSelector:@selector(selectSectionHeaderWithGoodsType:viewForHeaderInSection:isSelected:)]) {
        [_delegate selectSectionHeaderWithGoodsType:self.sectionHeaderModel.storeType viewForHeaderInSection:self.section isSelected:button.selected];
    }
}
- (void)updateViewWithShoppingCartSectionHeaderModel:(ShoppingCartSectionHeaderModel*)model {
    self.selectedButton.selected = model.isSelected;
    self.storeNameLabel.text = model.title;
    self.sectionHeaderModel = model;
}
@end
