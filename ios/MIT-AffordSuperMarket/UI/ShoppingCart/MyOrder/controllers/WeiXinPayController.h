//
//  WeiXinPayController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/4.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： WeiXinPayController
 Created_Date： 20151.15
 Created_People：JSQ
 Function_description：微信支付页面 
 ***************************************/
#import "BaseViewController.h"

@interface WeiXinPayController : BaseViewController


@property(nonatomic,strong)NSString *orderNum;

@property(nonatomic,strong)NSString *totalPrice;
@property(nonatomic,strong)NSString *goodIDs;
@property(nonatomic,strong)NSString *selfGoodIDs;
@property(nonatomic,strong)NSString *homeMakeType; //家政服务 有数据 说明是家政服务
@property(nonatomic,strong)NSString *requestDataType; //requestDataType  商品订单 是1 服务订单是 0
@end
