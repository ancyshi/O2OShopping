//
//  SelectServiceTimeController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SelectServiceTimeController
 Created_Date： 20151223
 Created_People：gt
 Function_description： 选择服务时间页面
 ***************************************/

#import "BaseViewController.h"

@interface SelectServiceTimeController : BaseViewController

@end
