//
//  MyOrderModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyOrderModel.h"

@implementation MyOrderModel
-(instancetype)initWithDict:(NSDictionary *)dict
{
    if (self =[super init]) {
        //使用kVC
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}
+(instancetype)OrderStateModelWithDict:(NSDictionary *)dict
{
    return [[self alloc]initWithDict:dict];
}
@end
