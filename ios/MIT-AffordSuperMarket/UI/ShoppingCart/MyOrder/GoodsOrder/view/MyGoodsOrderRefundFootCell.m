//
//  MyGoodsOrderRefundFootCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/24.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsOrderRefundFootCell.h"
#import "Global.h"
#import "NSString+TextSize.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"

#define kleftSpace 15
#define ksepLineHight 0.7
@interface MyGoodsOrderRefundFootCell()
@property(nonatomic, strong)UILabel *orderMount;//订单总金额
@property(nonatomic, strong)UILabel *amount;//退款金额
@property(nonatomic, strong)UILabel *refuseReason;//拒退原因
@end
@implementation MyGoodsOrderRefundFootCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSuberView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setUpSuberView {
    //订单号
    _orderMount = [[UILabel alloc]initWithFrame:CGRectZero];
    _orderMount.textColor = [UIColor colorWithHexString:@"#555555"];
    _orderMount.font = [UIFont systemFontOfSize:FontSize(15)];
    [self.contentView addSubview:_orderMount];
    
    _amount = [[UILabel alloc]initWithFrame:CGRectZero];
    _amount.textColor = [UIColor colorWithHexString:@"#db3b34"];
    _amount.font = [UIFont systemFontOfSize:FontSize(15)];
    [self.contentView addSubview:_amount];
    
    
    _refuseReason = [[UILabel alloc]initWithFrame:CGRectZero];
    _refuseReason.textColor = [UIColor colorWithHexString:@"#db3b34"];
    _refuseReason.font = [UIFont systemFontOfSize:FontSize(14)];
    _refuseReason.textAlignment =NSTextAlignmentLeft;
    _refuseReason.numberOfLines =0;
    [self.contentView addSubview:_refuseReason];
}
- (void)updateViewWithData:(MyGoodSOrderRefundModel *)refundModel stateMent:(NSString *)stateMent
{
   _refuseReason.text=@"";
    //退款金额
    NSString *amount=[NSString stringWithFormat:@"退款金额：￥%.2f",[[NSString stringTransformObject:refundModel.amount] floatValue]];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:amount];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#555555"] range:NSMakeRange(0,5)];
    _amount.attributedText =str;
    CGSize amountSize = [amount sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                           maxSize:CGSizeMake(VIEW_WIDTH/2, HightScalar(40))];
    _amount.frame =CGRectMake(VIEW_WIDTH-15 -amountSize.width, 10,amountSize.width,HightScalar(40));

    NSString *orderMount=[NSString stringWithFormat:@"交易金额：￥%.2f",[[NSString stringTransformObject:refundModel.orderMount] floatValue]];
    _orderMount.text =orderMount;
    CGSize orderMountSize = [orderMount sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                         maxSize:CGSizeMake(VIEW_WIDTH/2, HightScalar(40))];
    _orderMount.frame =CGRectMake(CGRectGetMinX(self.amount.frame)-orderMountSize.width-30, 10, orderMountSize.width, CGRectGetHeight(self.amount.bounds));
    
    
    
    
   NSString *refuseReasonText=[self setRefuseReasonTextWithModel:refundModel stateMent:stateMent];
    if (refuseReasonText.length != 0) {
              CGFloat contentW = VIEW_WIDTH - 2 * kleftSpace; // 屏幕宽度减去左右间距
        CGFloat contentH = [refuseReasonText boundingRectWithSize:CGSizeMake(contentW, MAXFLOAT)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:FontSize(14)]}
                                                 context:nil].size.height;
        CGRect frame = CGRectMake(kleftSpace, CGRectGetMaxY(self.orderMount.frame)+10, VIEW_WIDTH-2*kleftSpace, contentH);
        self.refuseReason.frame = frame;
        _refuseReason.text =refuseReasonText;

    }
    
}
-(NSString *)setRefuseReasonTextWithModel:(MyGoodSOrderRefundModel *)refundModel stateMent:(NSString *)stateMent
{
    if ([refundModel.state isEqualToString:@"1"]||[refundModel.state isEqualToString:@"2"]) {//退款申请 和 申请通过不显示 原因
        _refuseReason.hidden=YES;
        return @"";
    }else if([refundModel.state isEqualToString:@"3"]){//不通过
        if (refundModel.refuseReason.length !=0) {
            _refuseReason.hidden=NO;
            _refuseReason.textColor =[UIColor colorWithHexString:@"#db3b34"];
            return [NSString stringWithFormat:@"拒绝理由：%@",refundModel.refuseReason];
        }else{
            return @"";
        }
        
       
    }else if([refundModel.state isEqualToString:@"4"]){//正退款
        _refuseReason.hidden=NO;
         _refuseReason.textColor =[UIColor colorWithHexString:@"#999999"];
        return stateMent;
    }else{
        return @"";
    }
}
- (CGFloat)cellFactHight {
    if (self.refuseReason.hidden ==YES) {
       return   HightScalar(64);
    }else{
        if (self.refuseReason.text.length==0) {
            return HightScalar(64);
        }else{
            return HightScalar(64)+self.refuseReason.bounds.size.height+10;

        }
    }
   
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
