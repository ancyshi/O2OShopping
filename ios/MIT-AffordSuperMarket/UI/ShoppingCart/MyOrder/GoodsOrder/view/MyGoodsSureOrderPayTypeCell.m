//
//  MyGoodsSureOrderPayTypeCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/12.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsSureOrderPayTypeCell.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#define kleftSpace 15
#define krightSpace 15
#define kcellHight HightScalar(47)
#define ksepLineHight 0.5
@interface MyGoodsSureOrderPayTypeCell()
@property(nonatomic, strong)UILabel *titleLabel;
@property(nonatomic, strong)UILabel *contentLabel;
@property(nonatomic, strong)UIImageView *topSepLineImageView;
@property(nonatomic, strong)UIImageView *bottomSepLineImageView;
@end
@implementation MyGoodsSureOrderPayTypeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //初始化视图
        [self setSubView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setSubView {
     UIImage *sepLineImage = [UIImage imageNamed:@"gray_line"];
    
    _topSepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
    _topSepLineImageView.frame = CGRectMake(kleftSpace, 0, VIEW_WIDTH - kleftSpace, ksepLineHight);
    _topSepLineImageView.hidden = YES;
    [self.contentView addSubview:_topSepLineImageView];
    
    _titleLabel = [self setLabelWithTitle:@"" fontSize:FontSize(15) textColor:@"#555555"];
    _titleLabel.frame = CGRectMake(CGRectGetMinX(_topSepLineImageView.frame)
                                   , CGRectGetMaxY(_topSepLineImageView.frame)
                                   , 100
                                   , kcellHight - 2*ksepLineHight);
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_titleLabel];
    
    _contentLabel = [self setLabelWithTitle:@"" fontSize:FontSize(15) textColor:@"#000000"];
    _contentLabel.frame = CGRectMake(VIEW_WIDTH - 100 - krightSpace, 0, 100, CGRectGetHeight(_titleLabel.bounds));
    _contentLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_contentLabel];
    
   
    _bottomSepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
    _bottomSepLineImageView.frame = CGRectMake(0, CGRectGetMaxY(_contentLabel.frame)
                                               , VIEW_WIDTH
                                               , ksepLineHight);
    _bottomSepLineImageView.hidden = YES;
    [self.contentView addSubview:_bottomSepLineImageView];
    
}
- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}
#pragma mark -- 给视图进行赋值
- (void)setupViewWithData:(NSDictionary*)dataDic cellSepLineShowType:(CellSepLineShowType)sepLineShowType; {
    self.titleLabel.text = [dataDic objectForKey:@"title"];
    self.contentLabel.text = [NSString stringWithFormat:@"￥%@",[dataDic objectForKey:@"content"]];
    
    if (sepLineShowType == ktopSepLineShow) {
        _topSepLineImageView.hidden =  NO;
    } else if (sepLineShowType == kbottomSepLineShow) {
        _bottomSepLineImageView.hidden =  NO;
    } else if (sepLineShowType == kallSepLineShow) {
        _topSepLineImageView.hidden = NO;
        _bottomSepLineImageView.hidden = NO;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
