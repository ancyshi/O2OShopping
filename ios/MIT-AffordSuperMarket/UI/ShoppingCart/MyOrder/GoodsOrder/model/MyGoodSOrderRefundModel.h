//
//  MyGoodSOrderRefundModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/23.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyGoodSOrderRefundModel : NSObject
@property(nonatomic, strong)NSString *orderNum;//订单号
@property(nonatomic, strong)NSString *orderMount;//订单总金额
@property(nonatomic, strong)NSString *amount;//退款金额
@property(nonatomic, strong)NSString *orderListID;//要退款的商品在原订单商品列表中的主键ID，非商品ID
@property(nonatomic, strong)NSString *refundNum;//退款单号
@property(nonatomic, strong)NSString *createDate;//记录时间
@property(nonatomic, strong)NSString *state;//退款状态：1：审核中，2：审核通过，3：审核不通过，4：已退款
@property(nonatomic, strong)NSString *refuseReason;//退款不通过原因
@property(nonatomic, strong)NSString *upDateDate;//审核通过或不通过时间
@property(nonatomic, strong)NSString *ID;
@property(nonatomic, strong)NSString *type;//退款类型，1：取消订单，2：单个商品退款，3：零钱退款
@property(nonatomic, strong)NSArray *goodsList; //商品列表
@end
