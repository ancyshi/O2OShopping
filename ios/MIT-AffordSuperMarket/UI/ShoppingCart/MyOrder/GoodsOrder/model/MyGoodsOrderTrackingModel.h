//
//  MyGoodsOrderTrackingModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsOrderTrackingModel
 Created_Date： 20160315
 Created_People：GT
 Function_description： 商品订单追踪cell Model
 ***************************************/

#import <Foundation/Foundation.h>

@interface MyGoodsOrderTrackingModel : NSObject
@property(nonatomic, strong)NSString *orderType;//TYPE
@property(nonatomic, strong)NSString *handlePeople;//操作人
@property(nonatomic, strong)NSString *handleTitle;//操作标题
@property(nonatomic, strong)NSString *handleContent;//日志内容
@property(nonatomic, strong)NSString *handleDate;//创建时间
@property(nonatomic)BOOL isFirst;//第一个
@property(nonatomic)BOOL isLast;//最后一个
@end
