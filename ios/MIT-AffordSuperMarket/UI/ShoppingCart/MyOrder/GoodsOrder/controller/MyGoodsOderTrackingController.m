//
//  MyGoodsOderTrackingController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsOderTrackingController.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "AppDelegate.h"
#import "NSString+Conversion.h"
//vendor
#import "MTA.h"
#import "RDVTabBarController.h"
//view
#import "MyGoodsOrderTrackingHeaderView.h"
#import "MyGoodsOrderTrackingCell.h"
//viewModel
#import "MyGoodsOrderTrackingViewModel.h"
#define ktableHeaderHight HightScalar(98)
#define ksectionHeaderHihgt 12
@interface MyGoodsOderTrackingController ()<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, strong)MyGoodsOrderTrackingHeaderView *tableHeaderView;
@property(nonatomic, strong)MyGoodsOrderTrackingViewModel *viewModel;
@property(nonatomic, strong)UITableView *tableView;
@end

@implementation MyGoodsOderTrackingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    self.title = @"订单跟踪";
    [self addBackNavItem];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:self.tableView];
    [self requestData];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"商品订单追踪"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"商品订单追踪"];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}

#pragma mark -- requestData 
- (void)requestData {
     __weak typeof(self) weakSelf = self;
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    UserInfoModel *userInfoMode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringTransformObject:userInfoMode.token] forKey:@"TOKEN"];
    [parameter setObject:self.orderNumber.length == 0 ? @"" : self.orderNumber forKey:@"ORDERNUM"];
    [self.viewModel parameters:parameter requestSuccess:^(BOOL isSuccess) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        [weakSelf.tableView reloadData];
    } failure:^(NSString *failureInfo) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
         [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:failureInfo inView:weakSelf.view];
        [weakSelf.tableView reloadData];
    }];
}
#pragma mark -- UITableViewDataSource 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    MyGoodsOrderTrackingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[MyGoodsOrderTrackingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [cell updateViewWithData:[self.viewModel.dataSource objectAtIndex:indexPath.row]];
    return cell;
}
#pragma mark -- UITableViewDelegate 

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
   return [(MyGoodsOrderTrackingCell*)cell cellFactHight];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return ksectionHeaderHihgt;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, ksectionHeaderHihgt)];
    view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    [view.layer setBorderWidth:0.7];
    [view.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    return view;
}
#pragma mark -- button Action
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -- 初始化视图
- (MyGoodsOrderTrackingHeaderView*)tableHeaderView {
    if (!_tableHeaderView) {
        CGRect headerFrame = CGRectMake(0, 0, VIEW_WIDTH, ktableHeaderHight);
        NSArray *titles = @[@"支付方式：",@"配送方式：",@"订单编号："];
        MyGoodsOrderTrackingHeaderView *headerView = [[MyGoodsOrderTrackingHeaderView alloc]initWithFrame:headerFrame titles:titles];
        _tableHeaderView = headerView;
        [headerView updateViewWithDatas:@[self.payType,self.peiSongType,self.orderNumber]];
    }
    return _tableHeaderView;
}
- (UITableView*)tableView {
    if (!_tableView) {
        CGRect tableFrame = CGRectMake(0, 0, VIEW_WIDTH, CGRectGetHeight(self.view.bounds) - 64);
        UITableView *tableview = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
        tableview.delegate = self;
        tableview.dataSource = self;
        tableview.allowsSelection = NO;
        tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableview.tableHeaderView = self.tableHeaderView;
        _tableView = tableview;
    }
    return _tableView;
}

- (MyGoodsOrderTrackingViewModel*)viewModel {
    if (!_viewModel) {
        MyGoodsOrderTrackingViewModel *viewmodel = [[MyGoodsOrderTrackingViewModel alloc]init];
        _viewModel = viewmodel;
    }
    return _viewModel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
