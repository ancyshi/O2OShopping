//
//  MyGoodsSureOrderController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/8.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsSureOrderController.h"
//view
#import "MyGoodsSureOrderBottomView.h"
#import "MyGoodsSureOrderTableHeaderView.h"
#import "MyGoodsSureOrderTableFooterView.h"
#import "SureOrderCell.h"
#import "MyGoodsSureOrderPayTypeCell.h"
#import "MyGoodsSureOrderStoreNameCell.h"
#import "MyGoodsSureOredrShiHuiBiCell.h"
//viewModel
#import "MyGoodsSureOrderViewModel.h"
//model
#import "ReceivedAddressModel.h"
//unitls
#import "AppDelegate.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
#import "ManagerGlobeUntil.h"
#include "ManagerHttpBase.h"
#import "FMDBManager.h"
//vendor
#import "RDVTabBarController.h"
//controller
#import "UserCenter_FeedbackAndSuggestionController.h"
#import "ManageReceivedAddressController.h"
#import "WeiXinPayController.h"
#import "MyGoodsOrderController.h"
@interface MyGoodsSureOrderController ()<MyGoodsSureOrderBottomViewDelegate,MyGoodsSureOrderTableHeaderViewDelegate,MyGoodsSureOrderTableFooterViewDelegate,UIAlertViewDelegate,MyGoodsSureOredrShiHuiBiCellDelegate>
{
    NSInteger _payTypeOrPeiSongType;
}
@property(nonatomic, strong)MyGoodsSureOrderViewModel *viewModel;
@property(nonatomic, strong)MyGoodsSureOrderBottomView *bottomView;
@property(nonatomic, strong)MyGoodsSureOrderTableHeaderView *tableHeaderView;
@property(nonatomic, strong)MyGoodsSureOrderTableFooterView *tableFooterView;
@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic)BOOL switchState;//是否选中实惠币开关
@end

@implementation MyGoodsSureOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"确认订单";
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    [self addBackNavItem];
    [self requestData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:YES];
    self.tableFooterView.markInfo = self.markContent;
    if (self.viewModel.isHadRecivedAddress &&self.addressData.lINKNAME.length != 0) {//默认地址存在用户修改地址（把选中的地址信息带过来）
        [self requestReceivedAddressSuccessWithDataDic:self.addressData];
    }
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
#pragma mark -- request Data
- (void)requestData {
    __weak typeof(self) weakSelf = self;
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    self.viewModel.storeGoodsArr = self.storeGoodsArr;
    self.viewModel.selfGoodsArr = self.selfGoodsArr;
    [self.viewModel requestDatasSuccess:^(BOOL isSuccess) {
        [weakSelf setupSubView];
        weakSelf.tableFooterView.goodsPrice = self.viewModel.totalPrice;
        if(self.viewModel.isHadRecivedAddress && self.addressData.lINKNAME.length == 0) {
            [self requestReceivedAddressSuccessWithDataDic:self.viewModel.receivedAddressInfo];
        }
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
    } failure:^(NSString *failureInfo) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:failureInfo inView:weakSelf.view];
    }];
}
- (void)requestReceivedAddressSuccessWithDataDic:(ReceivedAddressOBJECT*)data {
    NSString *address = [NSString stringWithFormat:@"%@",[NSString stringTransformObject:data.aDDRESS]];
    [self.tableHeaderView updataWithName:[NSString stringTransformObject:data.lINKNAME]
                      telNumber:[NSString stringTransformObject:data.tEL]
                        address:address];
}

- (void)loadSubmitOrderData {
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    UserInfoModel *userInfoMode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    XiaoQuAndStoreInfoModel *xiaoQuAndStoreModel = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:userInfoMode.token forKey:@"TOKEN"];
    [parameter setObject:xiaoQuAndStoreModel.storeID forKey:@"STOREID"];
    [parameter setObject:self.viewModel.storeGoodsID forKey:@"STOREGOODSIDS"];
    [parameter setObject:self.viewModel.selfGoodsID forKey:@"SELFGOODSIDS"];
    [parameter setObject:self.viewModel.storeGoodsCount forKey:@"STORECOUNTS"];
    [parameter setObject:self.viewModel.selfGoodsCount forKey:@"SELFCOUNTS"];
    NSString *isRedeem =_switchState?@"1":@"0";
        if([self.tableHeaderView.storeGoodsPayType isEqualToString:@"2"]){
            if ([self.viewModel.selfGoodsCount isEqualToString:@""]) {
                isRedeem = @"0";
            }
        }
    [parameter setObject:self.viewModel.storeGoodsPrice forKey:@"STOREAMOUNT"];
    [parameter setObject:self.viewModel.selfGoodsPrice forKey:@"SELFAMOUNT"];
    [parameter setObject:isRedeem forKey:@"ISREDEEM"];
    [parameter setObject:self.tableHeaderView.storeGoodsPayType forKey:@"STOREPAYTYPE"];
    [parameter setObject:self.tableHeaderView.selfGoodsPayType forKey:@"SELFPAYTYPE"];
    [parameter setObject:self.tableHeaderView.storeGoodsSendProductType forKey:@"STORETAKETYPE"];
    [parameter setObject:self.tableHeaderView.selfGoodsSendProductType forKey:@"SELFTAKETYPE"];
    if (self.addressData.lINKNAME.length != 0) {//用户跳转到地址管理进行改变地址
        [parameter setObject:[NSString stringISNull:self.addressData.lINKNAME] forKey:@"LINKNAME"];
        NSString *sexStr = @"";
        if (self.addressData.sEX == 1) {
            sexStr = @"男";
        } else {
            sexStr = @"女";
        }
        [parameter setObject:sexStr forKey:@"SEX"];
        [parameter setObject:[NSString stringISNull:self.addressData.tEL] forKey:@"TEL"];
        NSString *address = [NSString stringWithFormat:@"%@",[NSString stringISNull:self.addressData.aDDRESS]];
        [parameter setObject:address forKey:@"ADDRESS"];
    } else if (!self.viewModel.isHadRecivedAddress) {//没有收获地址
        [parameter setObject:self.tableHeaderView.sexType forKey:@"SEX"];
        [parameter setObject:self.tableHeaderView.userNameTextField.text forKey:@"LINKNAME"];
        [parameter setObject:self.tableHeaderView.userTelePhoneTextField.text forKey:@"TEL"];
        [parameter setObject:self.tableHeaderView.userAddressTextField.text forKey:@"ADDRESS"];
    } else if (self.addressData.lINKNAME.length == 0 && self.viewModel.isHadRecivedAddress) {//用默认收获地址
        [parameter setObject:[NSString stringISNull:self.viewModel.receivedAddressInfo.lINKNAME] forKey:@"LINKNAME"];
        NSString *sexStr = @"";
        if (self.viewModel.receivedAddressInfo.sEX == 1) {
            sexStr = @"男";
        } else {
            sexStr = @"女";
        }
        [parameter setObject:sexStr forKey:@"SEX"];
        [parameter setObject:[NSString stringISNull:self.viewModel.receivedAddressInfo.tEL] forKey:@"TEL"];
        NSString *address = [NSString stringWithFormat:@"%@",[NSString stringISNull:self.viewModel.receivedAddressInfo.aDDRESS]];
        [parameter setObject:address forKey:@"ADDRESS"];
    }
    [parameter setObject:[NSString stringISNull:self.markContent] forKey:@"DESCRIBE"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"order/save" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                [self orderSumitSuccessWithOrderNumber:[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"ORDERNUM"]] forWardFlag:[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"FORWARDFLAG"]]];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
            
        }
    } failure:^(bool isFailure) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        
    }];
}

- (void)orderSumitSuccessWithOrderNumber:(NSString*)orderNumber forWardFlag:(NSString *)forWardFlag{
    if ([self.tableHeaderView.storeGoodsPayType isEqualToString:@"1"] || [self.tableHeaderView.selfGoodsPayType isEqualToString:@"1"]) {//在线支付
        NSString *totalGoodsPrice = @"";
        if ([self.tableHeaderView.storeGoodsPayType isEqualToString:@"1"] && [self.tableHeaderView.selfGoodsPayType isEqualToString:@"1"]) {
            if (_switchState==YES) {
                totalGoodsPrice = self.viewModel.offsetTotalPrice;
            }else{
                totalGoodsPrice = self.viewModel.totalPrice;
            }
        } else {
           // 选中商品类型:0只包含便利店商品 1只包含自营商品 2包含便利店和自营商品
            if (self.viewModel.selectdGoodsType == 0) {
                if (_switchState==YES) {
                     totalGoodsPrice = self.viewModel.offsetStoreGoodsPrice;
                }else{
                     totalGoodsPrice = self.viewModel.storeGoodsPrice;
                }
               
            } else if (self.viewModel.selectdGoodsType == 1) {
                if (_switchState==YES) {
                    totalGoodsPrice = self.viewModel.offsetSelfGoodsPrice;
                }else{
                     totalGoodsPrice = self.viewModel.selfGoodsPrice;
                }
               
            } else if (self.viewModel.selectdGoodsType == 2) {//包含便利店和自营商品，但是便利店商品为货到付款
                if (_switchState==YES) {
                    totalGoodsPrice = self.viewModel.offsetSelfGoodsPrice;
                }else{
                    totalGoodsPrice = self.viewModel.selfGoodsPrice;
                }
            }
        }
        if (orderNumber.length !=0) {
            [self deleteDataWithFMDBShoppingCatWithOrderNum:orderNumber];
        }
            if ([forWardFlag isEqualToString:@"1"]) {
            WeiXinPayController *weiXinPayVC = [[WeiXinPayController alloc]init];
            weiXinPayVC.orderNum = orderNumber;
            weiXinPayVC.totalPrice = totalGoodsPrice;
            weiXinPayVC.requestDataType = @"1";
            weiXinPayVC.goodIDs = @"";
            weiXinPayVC.selfGoodIDs = @"";
            [self.navigationController pushViewController:weiXinPayVC animated:YES];
        }else if([forWardFlag isEqualToString:@"2"]){
            MyGoodsOrderController *myGoodsOrderVC = [[MyGoodsOrderController alloc]init];
            myGoodsOrderVC.myGoodOrderType = kGoodWholeType;
            myGoodsOrderVC.selectedIndex =0;
            [self.navigationController pushViewController:myGoodsOrderVC animated:YES];
        }
        
    } else {//货到付款
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"恭喜您下单成功~" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
    }
}
//删除数据库
-(void)deleteDataWithFMDBShoppingCatWithOrderNum:(NSString *)orderNum
{
    if (orderNum.length!=0) {
        //        NSLog(@"%@",self.goodIDs);
        
        NSMutableArray * goodIDs=[NSMutableArray arrayWithArray:[self.viewModel.storeGoodsID componentsSeparatedByString:@","]];
        
        XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
        for (NSInteger i=0;i<goodIDs.count; i++) {
            [[FMDBManager sharedManager]deleteOneDataWithShopID:[NSString stringISNull:mode.storeID] goodID:[goodIDs objectAtIndex:i]];
        }
        
        NSMutableArray * selfGoods=[NSMutableArray arrayWithArray:[self.viewModel.selfGoodsID componentsSeparatedByString:@","]];
        for (NSInteger i=0;i<selfGoods.count; i++) {
            [[FMDBManager sharedManager]deleteOneDataWithgoodID:[selfGoods objectAtIndex:i]];
        }
        
    }
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([ self.tableHeaderView.storeGoodsPayType isEqualToString:@"2"] &&self.viewModel.selectdGoodsType == 0) {
           return self.viewModel.dataSource.count;
    }else{
           return self.viewModel.dataSource.count+1;
    }
 
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.viewModel.selectdGoodsType == 0 || self.viewModel.selectdGoodsType == 1) {
        if(section == 1){
            return 1;
        }
    }else if(self.viewModel.selectdGoodsType == 2)
    {
        if(section ==2){
            return 1;
        }
    }
    return [[self.viewModel.dataSource objectAtIndex:section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.viewModel.selectdGoodsType == 0) {//只包含便利店商品
        if (indexPath.section == 0) {
             return [self storeGoodsTableView:tableView cellForRowAtIndexPath:indexPath];
        }else if(indexPath.section == 1){
            return [self shiHuiBiTableView:tableView cellForRowAtIndexPath:indexPath];
        }
    } else if (self.viewModel.selectdGoodsType == 1) {//只包含自营商品
        if (indexPath.section == 0) {
            return [self selfGoodsTableView:tableView cellForRowAtIndexPath:indexPath];
        }else if(indexPath.section == 1){
            return [self shiHuiBiTableView:tableView cellForRowAtIndexPath:indexPath];
        }
    } else if (self.viewModel.selectdGoodsType == 2) {//包含便利店和自营商品
        if (indexPath.section == 0) {//便利店商品
            return [self storeGoodsTableView:tableView cellForRowAtIndexPath:indexPath];
        } else if (indexPath.section == 1) {//自营商品
            return [self selfGoodsTableView:tableView cellForRowAtIndexPath:indexPath];
        }else if(indexPath.section ==2){
            return [self shiHuiBiTableView:tableView cellForRowAtIndexPath:indexPath];

        }
    }
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    return cell;
}
- (UITableViewCell*)storeGoodsTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (([[self.viewModel.dataSource objectAtIndex:indexPath.section] count] - 1) == indexPath.row) {//数据最后一条进行特殊处理(展示商品价格)
        static NSString *cellIdentifier = @"OnlyStoreGoodsPriceIdentifier";
        MyGoodsSureOrderPayTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[MyGoodsSureOrderPayTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSDictionary *dataDic = [[self.viewModel.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [cell setupViewWithData:dataDic cellSepLineShowType:ktopSepLineShow];
        return cell;
    } else {
        static NSString *cellIdentifier = @"OnlyStoreGoodsIdentifier";
        SureOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[SureOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSDictionary *dataDic = [[self.viewModel.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [cell updateGoodsData:dataDic];
        return cell;
    }
    
}

- (UITableViewCell*)selfGoodsTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dataDic = [[self.viewModel.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if (indexPath.row == 0) {//展示便利店名字
        static NSString *cellIdentifier = @"onlySelfGoodsCommonIdentifier";
        MyGoodsSureOrderStoreNameCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[MyGoodsSureOrderStoreNameCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell setupViewWithData:dataDic];
        return cell;
    } else if (([[self.viewModel.dataSource objectAtIndex:indexPath.section] count] - 1) == indexPath.row) {//数据最后一条进行特殊处理(展示商品价格)
        static NSString *cellIdentifier = @"OnlySelfGoodsPriceIdentifier";
        MyGoodsSureOrderPayTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[MyGoodsSureOrderPayTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSDictionary *dataDic = [[self.viewModel.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [cell setupViewWithData:dataDic cellSepLineShowType:ktopSepLineShow];
        return cell;
    }else {//自营商品数据
        static NSString *cellIdentifier = @"OnlySelfGoodsIdentifier";
        SureOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[SureOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSDictionary *dataDic = [[self.viewModel.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [cell updateGoodsData:dataDic];
        return cell;
    }

}
- (UITableViewCell*)shiHuiBiTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *ID =@"titledddd";
    MyGoodsSureOredrShiHuiBiCell *cell =[tableView dequeueReusableCellWithIdentifier:ID];
    if (cell==nil) {
        cell=[[MyGoodsSureOredrShiHuiBiCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.backgroundColor =[UIColor whiteColor];
    }
    cell.delegate = self;
    [cell setUpViewWithShiHuiBi:self.viewModel.shiHuiBiMoney];
    return cell;

}
#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return HightScalar(47);
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (self.viewModel.selectdGoodsType == 2) {
        if (section == 0||section ==1) {
            return 12;
        }
    }else if (self.viewModel.selectdGoodsType == 0 ||self.viewModel.selectdGoodsType == 1){
        if (section ==0) {
            if ([ self.tableHeaderView.storeGoodsPayType isEqualToString:@"2"]) {
                return 0;
            }
            return 12;
        }
    }
    return 0;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (self.viewModel.selectdGoodsType == 2) {
        if (section == 0 || section ==1) {
            UIView *sectionView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 12)];
            [sectionView.layer setBorderColor:[UIColor colorWithHexString:@"#c6c6c6"].CGColor];
            [sectionView.layer setBorderWidth:0.5];
            return sectionView;
        }
    }else{
        if (section== 0) {
            UIView *sectionView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 12)];
            [sectionView.layer setBorderColor:[UIColor colorWithHexString:@"#c6c6c6"].CGColor];
            [sectionView.layer setBorderWidth:0.5];
            return sectionView;
        }
    }
    return nil;
}
#pragma mark -- MyGoodsSureOrderTableHeaderViewDelegate
- (void)selectPayTypeOrSendGoodsTypeWithType:(SelectPayTypeOrSendGoodsType)type {
    _payTypeOrPeiSongType = type;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                      
                                                                 delegate:(id<UIActionSheetDelegate>)self
                                      
                                                        cancelButtonTitle:nil
                                      
                                                   destructiveButtonTitle:nil
                                      
                                                        otherButtonTitles:nil];
    
    NSArray *dataArr =  nil;
    if (type == kselectPayType) {
        dataArr = @[@"在线支付",@"货到付款(仅支持门店)"];
    } else if (type == kselectSendGoodsType) {
        dataArr = @[@"门店配送",@"上门自取(仅支持门店)"];
    }
    for (NSString *title in dataArr) {
        [actionSheet addButtonWithTitle:title];
    }
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"取消"];
    [actionSheet showInView:self.view];

}
- (void)reSetMyGoodsReceivedAddress {
    ManageReceivedAddressController *manageReceivedAddressVC = [[ManageReceivedAddressController alloc]init];
    manageReceivedAddressVC.supperControllerType = kSureOrderPushToManageReceivedAddressController;
    [self.navigationController pushViewController:manageReceivedAddressVC animated:YES];
}
#pragma mark --MyGoodsSureOredrShiHuiBiCellDelegate
-(void)setSHiHuiBiSwitchState:(BOOL)state
{
    _switchState =state;
    [self.bottomView updateViewWithGoodsType:self.viewModel.selectdGoodsType
                           storeGoodsPayType:self.tableHeaderView.storeGoodsPayType
                            selfGoodsPayType:@"" storeGoodsPrice:self.viewModel.storeGoodsPrice selfGoodsPrice:self.viewModel.selfGoodsPrice goodsTotalPrice:self.viewModel.totalPrice offSetstoreGoodsPrice:self.viewModel.offsetStoreGoodsPrice offSetselfGoodsPrice:self.viewModel.offsetSelfGoodsPrice offSetgoodsTotalPrice:self.viewModel.offsetTotalPrice switchState:self.switchState];

}
#pragma mark -- UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"取消"]) {
        return ;
    }
    if (_payTypeOrPeiSongType == kselectPayType) {//选择支付方式
        NSString *peiSongTitle =title;
        if ([peiSongTitle containsString:@"仅支持门店"]) {
            peiSongTitle=@"货到付款";
        }
        self.tableHeaderView.storeGoodsPayType = peiSongTitle;
        [self.bottomView updateViewWithGoodsType:self.viewModel.selectdGoodsType
                                storeGoodsPayType:self.tableHeaderView.storeGoodsPayType
                                    selfGoodsPayType:@"" storeGoodsPrice:self.viewModel.storeGoodsPrice selfGoodsPrice:self.viewModel.selfGoodsPrice goodsTotalPrice:self.viewModel.totalPrice offSetstoreGoodsPrice:self.viewModel.offsetStoreGoodsPrice offSetselfGoodsPrice:self.viewModel.offsetSelfGoodsPrice offSetgoodsTotalPrice:self.viewModel.offsetTotalPrice switchState:self.switchState];
        [self.tableView reloadData];
    } else if (_payTypeOrPeiSongType == kselectSendGoodsType) {//选择配送方式
        XiaoQuAndStoreInfoModel *xiaoQuAndStoreModel = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
       
        self.tableHeaderView.storeGoodsSendProductType = title;
        if ([title isEqualToString:@"门店配送"]) {
            self.tableHeaderView.peiSongIntroduceMessage = xiaoQuAndStoreModel.storePeiSongIntroduce;
        } else if ([title isEqualToString:@"上门自取"]) {
            self.tableHeaderView.peiSongIntroduceMessage = xiaoQuAndStoreModel.storeYingYeTime;
        }
    }
}

#pragma mark -- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 0){
            NSMutableArray * goodIDs=[NSMutableArray arrayWithArray:[self.viewModel.storeGoodsID componentsSeparatedByString:@","]];
            
            XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
            for (NSInteger i=0;i<goodIDs.count; i++) {
                [[FMDBManager sharedManager]deleteOneDataWithShopID:[NSString stringISNull:mode.storeID] goodID:[goodIDs objectAtIndex:i]];
            }
            MyGoodsOrderController *myGoodsOrderVC = [[MyGoodsOrderController alloc]init];
            myGoodsOrderVC.myGoodOrderType = kGoodWholeType;
            myGoodsOrderVC.selectedIndex =0;
            [self.navigationController pushViewController:myGoodsOrderVC animated:YES];
        }
    }
}
#pragma mark -- MyGoodsSureOrderTableFooterViewDelegate
- (void)addOrderMarkInfo {
    UserCenter_FeedbackAndSuggestionController *addRemarksVC = [[UserCenter_FeedbackAndSuggestionController alloc]init];
    addRemarksVC.feedbackOrRemarksType = kRemarksType;
    addRemarksVC.hadSavedMarkInfo = self.markContent;
    [self.navigationController pushViewController:addRemarksVC animated:YES];
}
#pragma mark -- MyGoodsSureOrderBottomViewDelegate 提交订单
- (void)submitOrder {
   
        if (self.viewModel.isHadRecivedAddress) {
            if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
                [self loadSubmitOrderData];
            }
            
        } else {
            if (self.tableHeaderView.userNameTextField.text.length == 0) {
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"联系人姓名不能为空" inView:self.view];
                return;
            } else if (self.tableHeaderView.userTelePhoneTextField.text.length == 0) {
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"联系人电话不能为空" inView:self.view];
                return ;
            } else if (self.tableHeaderView.userAddressTextField.text.length ==0){
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"请填写详细地址" inView:self.view];
                return;
            } else {
                if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
                    [self loadSubmitOrderData];
                }
            }
        }
}
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    _switchState =YES;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self.tableView reloadData];
}

- (UITableView*)tableView {
    if (!_tableView) {
        CGRect frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds)
                                  , CGRectGetHeight(self.view.bounds));
        UITableView *tableview = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
        tableview.delegate = (id<UITableViewDelegate>)self;
        tableview.dataSource = (id<UITableViewDataSource>)self;
        tableview.backgroundColor = [UIColor clearColor];
        tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableview.tableHeaderView = self.tableHeaderView;
        tableview.tableFooterView = self.tableFooterView;
        _tableView = tableview;
    }
    return _tableView;
}
- (MyGoodsSureOrderTableHeaderView*)tableHeaderView {
    if (!_tableHeaderView) {
        CGFloat hight = self.viewModel.tableHeaderHight;
        CGRect frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), hight);
        MyGoodsSureOrderTableHeaderView *headerView = [[MyGoodsSureOrderTableHeaderView alloc]initWithFrame:frame
                                                                                     hadOrdersAddressStatus:self.viewModel.isHadRecivedAddress
                                                                                          selectedGoodsType:self.viewModel.selectdGoodsType];
        headerView.delegate = self;
        _tableHeaderView = headerView;
    }
    return _tableHeaderView;
}

- (MyGoodsSureOrderTableFooterView *)tableFooterView {
    if (!_tableFooterView) {
        CGRect frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), self.viewModel.tableFooterHight);
        MyGoodsSureOrderTableFooterView *footerView = [[MyGoodsSureOrderTableFooterView alloc]initWithFrame:frame];
        footerView.delegate = self;
        _tableFooterView = footerView;
    }
    return _tableFooterView;
}
- (MyGoodsSureOrderBottomView*)bottomView {
    if (!_bottomView) {
        CGRect bottomViewFrame = CGRectMake(0
                                            , CGRectGetHeight(self.view.bounds) - 58
                                            , CGRectGetWidth(self.view.bounds)
                                            , 58);
        MyGoodsSureOrderBottomView *bottomview = [[MyGoodsSureOrderBottomView alloc] initWithFrame:bottomViewFrame goodsType:self.viewModel.selectdGoodsType
                                                                                   storeGoodsPrice:self.viewModel.offsetStoreGoodsPrice
                                                                                    selfGoodsPrice:self.viewModel.offsetSelfGoodsPrice
                                                                                   goodsTotalPrice:self.viewModel.offsetTotalPrice];
        bottomview.backgroundColor = [UIColor blackColor];
        bottomview.delegate = self;
        bottomview.alpha = 0.85;
        _bottomView = bottomview;
    }
    return _bottomView;
}
 - (MyGoodsSureOrderViewModel*)viewModel {
     if (!_viewModel) {
         MyGoodsSureOrderViewModel *viewmodel = [[MyGoodsSureOrderViewModel alloc]init];
         _viewModel = viewmodel;
     }
     return _viewModel;
 }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
