//
//  SelectServiceTimeHeaderView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SelectServiceTimeHeaderView
 Created_Date： 20151224
 Created_People：gt
 Function_description： 选择服务时间头部视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol SelectServiceTimeHeaderViewDelegate<NSObject>
- (void)selectedItemWithIndex:(NSInteger)index;
@end
@interface SelectServiceTimeHeaderView : UIView
@property(nonatomic, unsafe_unretained)id<SelectServiceTimeHeaderViewDelegate>delegate;
@property(nonatomic)NSInteger selectedItemIndex;

- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray*)titles;
@end
