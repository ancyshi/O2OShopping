//
//  SelectServiceTimeHeaderView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SelectServiceTimeHeaderView.h"
#define kleftSpace 20
#define kitemWidth 80
#define kitemHeight 54
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
@interface SelectServiceTimeHeaderView () {
    NSInteger _currentBtnIndex;//当前button下标
    NSInteger _oldBtnIndex;//原来button下标
}
@property(nonatomic, strong)UIScrollView *rootScrollView;
@property(nonatomic, strong)NSMutableArray *items;
@property(nonatomic, strong)UIImageView *bottomArrowView;//底部箭头view
@end
@implementation SelectServiceTimeHeaderView
- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray*)titles {
    self = [super initWithFrame:frame];
    if (self) {
        _items = [[NSMutableArray alloc]init];
        _currentBtnIndex = 0;
        _oldBtnIndex = 0;
        self.selectedItemIndex = 0;
        [self setupSubViewWithTitles:titles];
        [self changeBottomArrowPosition];
    }
    return self;
}

#pragma mark -- 初始化视图
- (void)setupSubViewWithTitles:(NSArray*)titles {
    //底部箭头
    _bottomArrowView = [[UIImageView alloc]initWithFrame:CGRectZero];
    _bottomArrowView.backgroundColor = [UIColor clearColor];
    //左边箭头
    UIButton *leftArrowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftArrowBtn.frame = CGRectMake(0, 0, kleftSpace, kitemHeight);
    [leftArrowBtn setImage:[UIImage imageNamed:@"servicetime_btn_leftArrow"] forState:UIControlStateDisabled];
    leftArrowBtn.backgroundColor = [UIColor colorWithHexString:@"#f2f2f2"];
    leftArrowBtn.enabled = NO;
    //右边箭头
    UIButton *rightArrowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightArrowBtn.frame = CGRectMake(VIEW_WIDTH - kleftSpace, 0, kleftSpace, kitemHeight);
    [rightArrowBtn setImage:[UIImage imageNamed:@"servicetime_btnl_rightArrow"] forState:UIControlStateDisabled];
    rightArrowBtn.backgroundColor = [UIColor colorWithHexString:@"#f2f2f2"];
    rightArrowBtn.enabled = NO;
    
    UIImage *normalImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#f2f2f2"] frame:CGRectMake(0, 0, kitemWidth, kitemHeight)];
    UIImage *selectedImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#d7d7d7"] frame:CGRectMake(0, 0, kitemWidth, kitemHeight)];
    if (titles.count != 0) {
        [self setupRootScrollViewWithTitles:titles];
        for (NSInteger i = 0; i < [titles count]; i++) {
            NSDictionary *dataDic = (NSDictionary*)[titles objectAtIndex:i];
            CGRect frame = CGRectMake(kitemWidth*i, 0, kitemWidth, kitemHeight);
            UIButton *itemBtn = [self setupItemViewWithFrame:CGRectZero
                                                       title:[dataDic objectForKey:@"title"]
                                                    subTitle:[dataDic objectForKey:@"subtitle"]];
            [itemBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
            [itemBtn setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
            [itemBtn setBackgroundImage:selectedImage forState:UIControlStateSelected];
            [itemBtn addTarget:self action:@selector(selectedDateEvent:) forControlEvents:UIControlEventTouchUpInside];
            itemBtn.tag = i;
            itemBtn.frame = frame;
            if (i == 0) {
                itemBtn.selected = YES;
            
            }
            [_items addObject:itemBtn];
            [_rootScrollView addSubview:itemBtn];
        }
    } else {
        return ;
    }
    [_rootScrollView addSubview:_bottomArrowView];
    [self addSubview:leftArrowBtn];
    [self addSubview:rightArrowBtn];
}
- (void)setupRootScrollViewWithTitles:(NSArray*)titles {
    _rootScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(kleftSpace, 0, VIEW_WIDTH - 2*kleftSpace, CGRectGetHeight(self.bounds))];
    _rootScrollView.showsHorizontalScrollIndicator = NO;
    _rootScrollView.showsVerticalScrollIndicator = NO;
    _rootScrollView.bounces = NO;
    //_rootScrollView.delegate = (id<UIScrollViewDelegate>)self;
    _rootScrollView.backgroundColor = [UIColor clearColor];
    _rootScrollView.contentSize = CGSizeMake(kitemWidth*titles.count, CGRectGetHeight(self.bounds));
    [self addSubview:_rootScrollView];
}
- (UIButton*)setupItemViewWithFrame:(CGRect)frame title:(NSString*)title subTitle:(NSString*)subTitle {
    UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UILabel *titleLabel = [self setupLabelWithFrame:CGRectMake(0, 10, kitemWidth, kitemHeight/2 - 10) title:title];
    [titleButton addSubview:titleLabel];
    UILabel *subTitlelabel = [self setupLabelWithFrame:CGRectMake(CGRectGetMinX(titleLabel.frame)
                                                                  , CGRectGetMaxY(titleLabel.frame) + 3
                                                                  , CGRectGetWidth(titleLabel.bounds)
                                                                  , CGRectGetHeight(titleLabel.bounds))
                                                 title:subTitle];
    [titleButton addSubview:subTitlelabel];
    
    return titleButton;
}

- (UILabel*)setupLabelWithFrame:(CGRect)frame title:(NSString*)title {
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.text = title;
    label.textColor = [UIColor colorWithHexString:@"#2e2018"];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:15];
    return label;
}
#pragma mark -- setter method
- (void)setSelectedItemIndex:(NSInteger)selectedItemIndex {
//    _oldBtnIndex = _currentBtnIndex;
//    _currentBtnIndex = selectedItemIndex;
//    [self changeButtonStyle];
}
#pragma mark -- button Action
- (void)selectedDateEvent:(id)sender {
    UIButton *button = (UIButton*)sender;
    if (button.tag != _currentBtnIndex) {
        _oldBtnIndex = _currentBtnIndex;
        _currentBtnIndex = button.tag;
        [self changeButtonStyle];
        if (_delegate && [_delegate respondsToSelector:@selector(selectedItemWithIndex:)]) {
            [_delegate selectedItemWithIndex:_currentBtnIndex];
        }
    }
    
}
- (void)changeButtonStyle {
    [self changeBottomArrowPosition];
    UIButton *currentBtn = (UIButton*)[self.items objectAtIndex:_currentBtnIndex];
    UIButton *oldBtn = (UIButton*)[self.items objectAtIndex:_oldBtnIndex];
    currentBtn.selected = YES;
    oldBtn.selected = NO;
}
- (void)changeBottomArrowPosition{
    UIButton *currentBtn = (UIButton*)[self.items objectAtIndex:_currentBtnIndex];
    UIImage *image = [UIImage imageNamed:@"servicetime_btn_bottomArrow"];
    _bottomArrowView.image = image;
    _bottomArrowView.frame = CGRectMake(CGRectGetMidX(currentBtn.frame) - image.size.width/2
                                        , CGRectGetMaxY(currentBtn.frame)
                                        , image.size.width
                                        , image.size.height);
    [self scrollRectToVisibleCenteredOn:currentBtn.frame animated:YES];
}
#pragma mark -- 设置scrollView的可见部分
- (void)scrollRectToVisibleCenteredOn:(CGRect)visibleRect animated:(BOOL)animated {
    CGRect centeredRect = CGRectMake(visibleRect.origin.x + (visibleRect.size.width - CGRectGetWidth(_rootScrollView.bounds))/2,
                                     visibleRect.origin.y + (visibleRect.size.height - CGRectGetHeight(_rootScrollView.bounds))/2 ,
                                     CGRectGetWidth(_rootScrollView.bounds),
                                     CGRectGetHeight(_rootScrollView.bounds));
    [_rootScrollView scrollRectToVisible:centeredRect animated:animated];
}

@end
