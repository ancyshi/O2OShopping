//
//  EmptyPageView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/1.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： EmptyPageView
 Created_Date： 20160301
 Created_People： JSQ
 Function_description： 空白页面
 ***************************************/
#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, EmptyPageViewType) {
    kServiecListView = 0,//服务商家列表
    kServiceRootView, //服务根页面
    kHistoyView, //浏览历史
    KMarkView, //关注
    kManageReceivedAddress,//管理收货地址
    kHomeGoodListView, //首页活动二级页面
    kGoodsOrderRootView,//商品订单
    kServiceOrderRootView,//服务订单
    kShoppingCarView, //购物车页面
    kSearchResultView, //搜索结果页面
    kStoreRootView, //便利店根视图
    kStoreNoGoodsView, //便利店没有商品视图
    kGoodsOrderRefundView, //退款视图
    kMyWalletNoDataView, //我的钱包空视图
    kMyShiHuiBiView, //我的实惠币无消费记录空视图
    kRecommendFriendView, //推荐的好友无收益空视图

};


@protocol EmptyPageViewDelegate <NSObject>

-(void)buttonClick;

@end
@interface EmptyPageView : UIView
-(id)initWithFrame:(CGRect)frame EmptyPageViewType:(EmptyPageViewType)emptyPageViewType;
@property (nonatomic,assign)id<EmptyPageViewDelegate>delegate;
@property(nonatomic)EmptyPageViewType emptyPageViewType;
@end

