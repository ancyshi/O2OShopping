//
//  Login_Controller.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/9.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Login_Controller
 Created_Date： 20151108
 Created_People： GT
 Function_description： 登录界面
 ***************************************/


#import "BaseViewController.h"
#import "Global.h"
typedef NS_ENUM(NSInteger, Login_ControllerSupperControllerType) {
    kUserCenterPushToLogin_ControllerSupperController = 0,//从个人中心
    kProductDetetailPushToLogin_ControllerSupperController = 1, //从商品详情
    kShoppingCartGoToJieSuanPushToLogin_ControllerSupperController = 3,//从购物车去结算
    kServiceDetailPushtoLogin_ControllerSupperController //从服务详情
};
@interface Login_Controller : BaseViewController
@property(nonatomic, strong)NSString *userName;
@property(nonatomic)Login_ControllerSupperControllerType supperControllerType;
@end
