<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.seller.common.OrderCode,cn.com.dashihui.seller.common.SellerState.DispatchState" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>小票预览</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/app/css/ticket.css"/>
</head>
<body>
	<header class="am-header app-header">
		<h1 class="am-header-title app-title">小票预览</h1>
		<div class="am-header-left am-header-nav">
			<a href="javascript:history.go(-1);"><i class="am-icon-angle-left am-icon-lg"></i></a>
		</div>
		<div id="refreshBtn" class="am-header-right am-header-nav app-title" onclick="javascript:doReload();">
			<span>刷新</span>
		</div>
	</header>
	<div class="app-m-l-10 app-m-r-10 app-m-t-10 app-ticket app-list app-m-t-10" id="ticketContent"></div>
	<div class="app-p-10" style="margin-bottom:38px;">
		<a href="javascript:doPrint();" class="am-btn am-btn-primary am-radius am-fl" style="width:100%;">打印</a>
	</div>
	<div class="am-cf"></div>
	
	<!-- include -->
	<%@include file="../../include.jsp"%>
	<!-- lib -->
	<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
	<!-- app -->
	<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/app/js/plus.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/app/js/ticket.js"></script>
	<script type="html/text" id="dataTpl">
		<div class="app-list-row app-text-mideate"><span>{{storeTitle}}-送货单</span></div>
		<div class="app-list-row app-m-t-10">
			<div>配货员：{{sellerName}}</div>
		</div>
		<div class="app-list-row">
			<div>日期：{{startDate}}</div>
		</div>
		<div class="app-list-row">
			{{if payType == <%=OrderCode.OrderPayType.ON_LINE%>}}
				<div>支付方式：在线支付</div>
			{{else if payType == <%=OrderCode.OrderPayType.ON_DELIVERY%>}}
				<div>支付方式：货到付款</div>
			{{/if}}
		</div>
		<div class="app-list-row">
			{{if takeType == <%=OrderCode.OrderTakeType.DELIVER%>}}
				<div>配送方式：送货上门</div>
			{{else if takeType == <%=OrderCode.OrderTakeType.TAKE_SELF%>}}
				<div>配送方式：门店配送</div>
			{{/if}}
		</div>
		<div class="app-list-row">
			<span>订单号：{{orderNum}}</span>
		</div>
		<div class="app-list-row app-list-divider">
			<div class="app-driver app-divider-double"></div>
		</div>
		<div class="app-list-row app-m-t-5 app-m-b-10">
			<div class="am-u-sm-6">商品名</div>
			<div class="am-u-sm-2">数量</div>
			<div class="am-u-sm-2">单价</div>
			<div class="am-u-sm-2">金额</div>
		</div>
		{{each goodsList as goods}}
			<div class="app-list-row">
				<div>{{goods.name}}</div>
			</div>
			<div class="app-list-row">
				<div class="am-u-sm-6">&nbsp;</div>
				<div class="am-u-sm-2">{{goods.count}}</div>
				<div class="am-u-sm-2">{{goods.price}}</div>
				<div class="am-u-sm-2">{{goods.amount}}</div>
			</div>
		{{/each}}
		<div class="app-list-row app-list-divider">
			<div class="app-driver app-divider-double"></div>
		</div>
		<div class="app-height-10">
			<div class="am-fl">总计：</div>
			<div class="am-fr" style="margin-right:23px;">{{amount}}</div>
			<div class="am-cf"></div>
		</div>
		<div class="app-list-row app-list-divider app-m-t-10">
			<div class="app-driver app-divider-double"></div>
		</div>
		<div class="app-list-row">
			<div class="am-fl">在线支付：</div>
			<div class="am-fr" style="margin-right:23px;">{{amount}}</div>
			<div class="am-cf"></div>
		</div>
		<div class="app-list-row">
			<div class="am-fl">缺货退款：</div>
			<div class="am-fr" style="margin-right:23px;">{{goodsRefundAmount}}</div>
			<div class="am-cf"></div>
		</div>
		<div class="app-list-row">
			<div class="am-fl">差额退款：</div>
			<div class="am-fr" style="margin-right:23px;">{{changeRefundAmount}}</div>
			<div class="am-cf"></div>
		</div>
		<div class="app-list-row app-list-divider">
			<div class="app-driver app-divider-double"></div>
		</div>
		<div class="app-list-row">
			<div class="am-fl">实收款：</div>
			<div class="am-fr" style="margin-right:23px;">{{actualPay}}</div>
			<div class="am-cf"></div>
		</div>
		<div class="app-list-row app-height-5"></div>
		<div class="app-list-row app-text-mideate">
			<欢迎再次光临>
		</div>
		<div class="app-list-row app-height-100 app-m-t-5">
			<div class="am-u-sm-12 app-m-t-10">地址：{{storeAddress}}</div>
			<div class="am-u-sm-12 app-m-t-10">为了您的权益，请妥善保管小票！</div>
			<!-- <div class="am-u-sm-12 app-m-t-10">微信扫一扫，更多优惠手机专享！！</div> -->
		</div>
		<!-- <div class="app-list-row" style="height:130px;">
			<div class="am-u-sm-5">
				<img src="${BASE_PATH}/static/app/img/QR_code_wechat.png" height="100px" class="am-fr">
			</div>
			<div class="am-u-sm-5 am-u-sm-offset-2">
				<img src="${BASE_PATH}/static/app/img/QR_code_app.png" height="100px" class="am-fl">
			</div>
			<div class="am-cf"></div>
		</div>
		<div class="app-list-row">
			<div class="am-u-sm-5 app-text-mideate">微商城</div>
			<div class="am-u-sm-5 app-text-mideate am-u-sm-offset-2">APP</div>
		</div> -->
	</script>
	<script type="text/javascript">
		$(function(){
			//初始化显示小票信息
			Kit.ajax.post("${BASE_PATH}/order/showDeliverTicket",{orderNum:'${orderNum}'},function(result){
				$("#ticketContent").append(template("dataTpl", result.object));
				this.data = result.object;
			});
		});
		
		function doPrint(){
			Kit.ui.alert("暂不支持小票打印");
			//var jsonArr = dataParse();
			//Plus.onReady(function(bridge){
			//	if(Plus.isIOS()){
			//		bridge.callHandler("receiveJSData",jsonArr,function(msg){
			//		});
			//	}else if(Plus.isAndroid()){
			//		bridge.print(JSON.stringify(jsonArr));
			//	}
			//});
		}

		/**
		 * 数据转换
		 * position:位置 1：左对齐，2：居中，3：右对齐
		 * size:大小 1：大号，2：普通
		 */
		function dataParse(){
			var jsonArr = [];
			jsonArr.push(json(data.storeTitle+'-送货单','2'));
			jsonArr.push(json('配货员：'+data.sellerName));
			jsonArr.push(json('日期：'+data.createDate));
			var payType;
			if(data.payType == <%=OrderCode.OrderPayType.ON_LINE%>) {
				payType = '在线支付';
			} else if(data.payType == <%=OrderCode.OrderPayType.ON_DELIVERY%>) {
				payType = '货到付款';
			}
			jsonArr.push(json('支付方式：'+payType));
			var takeType = '';
			if(data.takeType == <%=OrderCode.OrderTakeType.DELIVER%>){
				takeType = '送货上门';
			} else if(data.takeType == <%=OrderCode.OrderTakeType.TAKE_SELF%>) {
				takeType = '门店配送';
			}
			jsonArr.push(json('配货方式：'+takeType));
			jsonArr.push(json('订单号：'+data.orderNum));
			//32个连接线
			jsonArr.push(json('--------------------------------'));
			//空格：4 5 5,汉字：18
			jsonArr.push(json('商品名    数量     单价     金额'));
			var goodsList = data.goodsList;
			for(var i = 0; i < goodsList.length; i++) {
				jsonArr.push(json(goodsList[i].name));
				//空格：8  4 4
				var count = Kit.util.splice(14, String(goodsList[i].count));
				var price = Kit.util.splice(9, String(goodsList[i].price));
				var amount = Kit.util.splice(9, String(goodsList[i].amount));
				jsonArr.push(json(count+price+amount));
			}
			//32个连接线
			jsonArr.push(json('--------------------------------'));
			//空格：6 6,汉字：12
			var amount = Kit.util.splice(26, String(data.amount));
			jsonArr.push(json('总计：'+amount));
			jsonArr.push(json('收货人：'+data.linkName));
			jsonArr.push(json('联系方式：'+data.tel));
			jsonArr.push(json('地址：'+data.address));
			//32个连接线
			jsonArr.push(json('--------------------------------'));
			amount = Kit.util.splice(22, String(data.amount));
			jsonArr.push(json('在线支付：'+amount)); 
			var goodsRefundAmount = Kit.util.splice(22,String(data.goodsRefundAmount));
			jsonArr.push(json('缺货退款：'+goodsRefundAmount));
			var changeRefundAmount = Kit.util.splice(22,String(data.changeRefundAmount));
			jsonArr.push(json('缺货退款：'+changeRefundAmount));
			//32个连接线
			jsonArr.push(json('--------------------------------'));
			var actualPay = Kit.util.splice(24,String(data.actualPay));
			jsonArr.push(json('实收款：'+actualPay));
			jsonArr.push(json('<欢迎再次光临>','2'));
			jsonArr.push(json('地址：'+data.storeAddress));
			return jsonArr;
		}
		
		//封装json数据
		function json(rowContent,position,size){
			if(position == null || position == undefined) {
				position = '1';
			}
			if(size == null || size == undefined) {
				size = '2';
			}
			return {rowContent:rowContent,position:position,size:size};
		}
		
		function doReload(){
			window.location.reload();
		}
	</script>
</body>
</html>