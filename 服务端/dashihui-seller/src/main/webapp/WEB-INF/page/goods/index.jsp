<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.seller.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<title>商品管理</title>
	<!-- amazeui -->
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
	<!-- app -->
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/goods.css" />
</head>
<body style="padding-bottom:80px;">
	<header class="am-header app-header">
		<h1 class="am-header-title app-title">商品查询</h1>
	</header>
	<div style="padding:10px;background-color:white;">
		<form class="am-form" action="${BASE_PATH }/goods/goToList" id="goodsForm" method="post">
			<div class="am-form-group">
				<label>关键字：</label>
				<input type="text" name="keyword" placeholder="输入搜索关键字">
			</div>
			<div class="am-form-group">
				<label>状态：</label>
				<select name="state">
					<option value="1">在售</option>
					<option value="2">已下架</option>
				</select>
			</div>
			<div class="am-form-group">
				<label>商品分类：</label>
				<select id="sCategoryonid" onchange="changeSelect(this);" name="categoryonid">
					<option value="">请选择一级分类</option>
				</select>
				<select id="sCategorytwid" onchange="changeSelect(this);" name="categorytwid" class="app-m-t-5">
					<option value="">请选择二级分类</option>
				</select>
				<select id="sCategorythid" onchange="changeSelect(this);" name="categorythid" class="app-m-t-5">
					<option value="">请选择三级分类</option>
				</select>
				<select id="sCategoryfoid" name="categoryfoid" class="app-m-t-5">
					<option value="">请选择四级分类</option>
				</select>
			</div>
			<div class="am-form-group">
				<label>标签：</label>
				<div>
		    		<c:forEach items="${tagList}" var="tag" varStatus="status">
	    			<div class="am-fl">
    					<input type="checkbox" id="tag" name="tag" value="${tag.id}" class="am-fl">
    					<span class="am-fl app-m-l-5 <c:if test="${status.index!=fn:length(tagList)-1}">app-m-r-10</c:if>">${tag.tagName}</span>
	    				<div class="am-cf"></div>
   					</div>
		    		</c:forEach>
		    		<div class="am-cf"></div>
	    		</div>
			</div>
			<div class="am-form-group">
				<label>商品类型：</label>
				<div>
		    		<div class="am-fl">
						<input type="checkbox" name="goodsType" value="1" class="am-fl">
						<span class="am-fl app-m-l-5 app-m-r-10">普通</span>
			    		<div class="am-cf"></div>
	    			</div>
					<div class="am-fl">
						<input type="checkbox" name="goodsType" value="2" class="am-fl">
						<span class="am-fl app-m-l-5 app-m-r-10">推荐</span>
			    		<div class="am-cf"></div>
	    			</div>
					<div class="am-fl">
						<input type="checkbox" name="goodsType" value="3" class="am-fl">
						<span class="am-fl app-m-l-5 app-m-r-10">限量</span>
			    		<div class="am-cf"></div>
	    			</div>
					<div class="am-fl">
						<input type="checkbox" name="goodsType" value="4" class="am-fl">
						<span class="am-fl app-m-l-5 app-m-r-10">一元购</span>
			    		<div class="am-cf"></div>
	    			</div>
		    		<div class="am-cf"></div>
	    		</div>
			</div>
			<a href="javascript:doSubmit();" class="am-btn am-btn-primary am-radius" style="width: 100%;">开始查询</a>
		</form>
	</div>
	<div class="am-navbar am-cf am-no-layout app-navbar">
		<ul class="am-navbar-nav am-cf am-avg-sm-3">
			<li>
				<a href="${BASE_PATH }/index">
					<img src="${BASE_PATH}/static/app/img/menu_order01.png"/>
					<span class="am-navbar-label">订单管理</span>
				</a>
			</li>
			<li class="app-navbar-cart">
				<a href="javascript:void(0);" class="app-active">
					<img src="${BASE_PATH}/static/app/img/menu_goods12.png"/>
					<span class="am-navbar-label">商品管理</span>
				</a>
			</li>
		</ul>
	</div>
	<!-- include -->
	<%@include file="../include.jsp"%>
	<!-- lib -->
	<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
	<script src='${BASE_PATH}/static/lib/layer/layer.js' type='text/javascript'></script>
	<!-- app -->
	<script src='${BASE_PATH}/static/app/js/kit.js' type='text/javascript'></script>
	<script type="text/javascript">
		//定义一级分类，二级分类，三级分类，四级分类
		var firstLevel = [],secondLevel = [],thirdLevel = [],fourthLevel = [];
		$(function(){
			Kit.ajax.post("${BASE_PATH}/goods/getAllCategory", null, function(result){
				var category = result.object;
				for(var k in category) {
					var type = category[k].categoryType;
					switch(type) {
					case 1:
						firstLevel.push(category[k]);
						$("#sCategoryonid").append("<option value='"+category[k].categoryId+"'>" + category[k].categoryName + "</option>");
					case 2:
						secondLevel.push(category[k]);
					case 3:
						thirdLevel.push(category[k]);
					case 4:
						fourthLevel.push(category[k]);
					}
				}
			});
		});
		
		function changeSelect(obj) {
			var nextData,nextDataId;
			if(obj.value != null || obj.value != '') {
				if(String(obj.id) == 'sCategoryonid') {
					nextData = secondLevel;
					nextDataId = "sCategorytwid";
				}
				if(String(obj.id) == 'sCategorytwid') {
					nextData = thirdLevel;
					nextDataId = 'sCategorythid';
				}
				if(String(obj.id) == 'sCategorythid') {
					nextData = fourthLevel;
					nextDataId = 'sCategoryfoid';
				}
			}
			$("#"+nextDataId).html("<option value=''>--请选择--</option>");
			for(var i in nextData) {
				if(nextData[i].categoryFatherId == obj.value) {
					$("#"+nextDataId).append("<option value='"+nextData[i].categoryId+"'>" + nextData[i].categoryName + "</option>");
				}
			}
		}
		
		function doSubmit() {
			$("#goodsForm").submit();
		}
	</script>
</body>
</html>