package cn.com.dashihui.seller.domain;

public class AjaxResult {
	public static final int FLAG_SUCCESS = 0;
	public static final int FLAG_FAILED = -1;
	public static final int FLAG_NOLOGIN = -2;

	private int flag;
	private String message;
	private Object object;
	
	public AjaxResult(int flag) {
		this.flag = flag;
	}
	
	public AjaxResult(int flag, String message) {
		this.flag = flag;
		this.message = message;
	}
	
	public AjaxResult(int flag, String message, Object object) {
		this.flag = flag;
		this.message = message;
		this.object = object;
	}
	
	public AjaxResult(int flag, Object object) {
		this.flag = flag;
		this.object = object;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
