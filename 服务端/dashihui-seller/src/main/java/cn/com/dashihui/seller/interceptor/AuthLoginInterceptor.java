package cn.com.dashihui.seller.interceptor;

import com.jfinal.aop.Invocation;

import cn.com.dashihui.seller.controller.BaseController;
import cn.com.dashihui.seller.domain.AjaxResult;

/**
 * 拦截用户请求，判断当前是否有用户登录
 */
public class AuthLoginInterceptor extends AjaxInterceptor{
	
	@Override
	public void onAjax(Invocation inv) {
		BaseController c = (BaseController)inv.getController();
		//判断用户是否授权过
		if(c.getCurrentSeller()==null){
			//未登录
			c.renderResult(AjaxResult.FLAG_NOLOGIN);
			return;
		}
		inv.invoke();
	}
	
	@Override
	public void onAjaxNope(Invocation inv) {
		BaseController c = (BaseController)inv.getController();
		//判断用户是否授权过
		if(c.getCurrentSeller()==null){
			//未登录
			c.redirect("/login");
			return;
		}
		inv.invoke();
	}
}
