package cn.com.dashihui.seller.common;

public class SellerState {

	/**
	 * 送货员接单状态
	 */
	public static class DispatchState {
		public final static int  UNACCEPT = 0;
		public final static int  WAIT_PACK = 1;
		public final static int  PACKING = 2;
		public final static int  PACK_FINISH = 3;
		public final static int  DISPATCHING = 4;
		public final static int  WAIT_GET = 5;
		public final static int  FINISH = 6;
	}
	
	/**
	 * 送货员所接订单显示或隐藏标志
	 */
	public static class ShowState {
		public final static int SHOW = 1;
		public final static int HIDE = 2;
	}
}
