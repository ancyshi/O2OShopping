package cn.com.dashihui.seller.kit;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jfinal.core.Const;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Prop;

import net.sf.json.JSONObject;

/**
 * 发短信工具类，调用的是“聚合数据”网接口（http://www.juhe.cn/）
 * 需要事先在“聚合”网上添加相应的短信模板，并通过审核才可使用
 * @author duxikun
 */
public class SMSKit {
	private static Logger logger = Logger.getLogger(SMSKit.class);
	
	private static boolean isDebug(){
		Prop debug = new Prop("realtime.properties",Const.DEFAULT_ENCODING);
		return debug.getBoolean("debug.sms");
	}
	
	/**
	 * 注册时，发送短信验证码
	 * @param msisdn 手机号
	 * @param code 验证码
	 */
	public static boolean sendRegCode(String msisdn, String code){
		if(!isDebug()){
			Map<String, String> params = new HashMap<String, String>();
			params.put("mobile", msisdn);
			//模板ID
			params.put("tpl_id", "7367");
			params.put("tpl_value", "#code#="+code+"&#min#=5分钟");
			params.put("key", "195f4ee17e4ea9ce49270dae01f34e88");
			params.put("dtype", "json");
			logger.info("向手机号"+msisdn+"发送注册验证码短信："+code);
			String resultStr = HttpKit.get("http://v.juhe.cn/sms/send",params);
			JSONObject result = JSONObject.fromObject(resultStr);
			logger.info("向手机号"+msisdn+"发送注册验证码短信返回结果："+resultStr);
			return result.getInt("error_code")==0;
		}else{
			logger.info("向手机号"+msisdn+"发送注册验证码短信："+code);
			return true;
		}
	}
	
	/**
	 * 找回密码时，发送短信验证码
	 * @param msisdn 手机号
	 * @param code 验证码
	 */
	public static boolean sendFindPwdCode(String msisdn, String code){
		if(!isDebug()){
			Map<String, String> params = new HashMap<String, String>();
			params.put("mobile", msisdn);
			//模板ID
			params.put("tpl_id", "7368");
			params.put("tpl_value", "#code#="+code+"&#min#=5分钟");
			params.put("key", "195f4ee17e4ea9ce49270dae01f34e88");
			params.put("dtype", "json");
			logger.info("向手机号"+msisdn+"发送找回密码时验证码短信："+code);
			String resultStr = HttpKit.get("http://v.juhe.cn/sms/send",params);
			JSONObject result = JSONObject.fromObject(resultStr);
			logger.info("向手机号"+msisdn+"发送找回密码时验证码短信返回结果："+resultStr);
			return result.getInt("error_code")==0;
		}else{
			logger.info("向手机号"+msisdn+"发送找回密码时验证码短信："+code);
			return true;
		}
	}
}
