package cn.com.dashihui.seller.common;

public class OrderCode {
	/**
	 * 订单状态
	 * 订单状态，1:正常，2：已完成，3：取消，4：删除，5：过期
	 */
	public static class OrderState{
		public final static int  NORMAL = 1;
		public final static int  FINISH = 2;
		public final static int  CANCEL = 3;
		public final static int  DELETE = 4;
		public final static int  EXPIRE = 5;
	}
	/**
	 * 订单支付类型
	 * 支付方式，1：在线支付，2：货到付款
	 */
	public static class OrderPayType{
		public final static int  ON_LINE   = 1;
		public final static int  ON_DELIVERY = 2;
	}
	/**
	 * 订单支付渠道
	 * 在线支付渠道，1：微信支付，2：支付宝支付
	 */
	public static class OrderPayMethod{
		public final static int  WEIXIN  = 1;
		public final static int  ALIPAY  = 2;
	}
	/**
	 * 订单支付状态
	 * 支付状态，1：待支付，2：已支付
	 */
	public static class OrderPayState{
		public final static int  NO_PAY  = 1;
		public final static int  HAD_PAY = 2;
	}
	/**
	 * 收货方式
	 * 收货方式，1：送货，2：自取
	 */
	public static class OrderTakeType{
		public final static int DELIVER = 1;
		public final static int TAKE_SELF = 2;
	}
	/**
	 * 打包状态
	 * 打包状态，1：未接单，2：已接单，3：打包中，4：打包完成
	 */
	public static class OrderPackState{
		public final static int NO_ACCEPT = 1;
		public final static int HAD_ACCEPT = 2;
		public final static int PACKING = 3;
		public final static int PACK_FINISH = 4;
	}
	/**
	 * 收货状态
	 * 收货状态，1：待发货，2：已经发货
	 */
	public static class OrderDeliverState{
		public final static int NO_DELIVER = 1;
		public final static int HAD_DELIVER= 2;
	}
	/**
	 * 订单操作记录类型
	 * 1：新订单保存，2：支付订单，3：配送员接单，4：配货，5：配货完成，6：配送，7：签收，8：取货，9：取消订单，10：订单过期，11：删除订单，12：催单
	 */
	public static class OrderLogType{
		public final static int SAVE = 1;
		public final static int PAY = 2;
		public final static int ACCEPT = 3;
		public final static int PACK = 4;
		public final static int PACK_FINISH = 5;
		public final static int DISPATCH = 6;
		public final static int SIGN = 7;
		public final static int GET = 8;
		public final static int CANCEL = 9;
		public final static int EXPIRY = 10;
		public final static int DELETE = 11;
		public final static int URGE = 12;

		public final static String SAVE_ACTION = "提交订单";
		public final static String PAY_ACTION = "支付完成";
		public final static String ACCEPT_ACTION = "配送员接单";
		public final static String PACK_ACTION = "开始配货";
		public final static String PACK_FINISH_ACTION = "配货完成";
		public final static String DISPATCH_ACTION = "配送";
		public final static String SIGN_ACTION = "签收";
		public final static String GET_ACTION = "取货";
		public final static String CANCEL_ACTION = "取消订单";
		public final static String EXPIRY_ACTION = "订单过期";
		public final static String DELETE_ACTION = "删除订单";
		public final static String URGE_ACTION = "催单";
	}
	
	/**
	 * 退款类型
	 * 1：取消订单，2：单个商品退款,3:零钱退款
	 */
	public static class RefundType {
		public final static int CANCEL_ORDER = 1;
		public final static int CANCEL_SINGLE = 2;
		public final static int CHANGE_REFUND = 3;
	}
	
	/**
	 * 退款状态
	 * 1：审核中，2：审核通过，3：审核不通过，4：已退款
	 */
	public static class RefundState {
		public final static int VERIFYING = 1;
		public final static int VERIFIED = 2;
		public final static int VERIFY_FAILED = 3;
		public final static int REFUNDED = 4;
	}
}
