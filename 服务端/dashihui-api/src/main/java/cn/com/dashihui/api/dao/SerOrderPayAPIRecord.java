package cn.com.dashihui.api.dao;

import com.jfinal.plugin.activerecord.Model;

/**
 * 家政类服务订单接口日志对象
 */
public class SerOrderPayAPIRecord extends Model<SerOrderPayAPIRecord>{
	private static final long serialVersionUID = 1L;
	private static SerOrderPayAPIRecord me = new SerOrderPayAPIRecord();
	public static SerOrderPayAPIRecord me(){
		return me;
	}

}
