package cn.com.dashihui.api.common;

/**
 * 服务类订单的各项状态
 */

public class ServiceOrderCode {
	/**
	 * 订单状态
	 * 订单状态，1:正常，2：已完成，3：取消，4：删除，5：过期
	 */
	public static class OrderState{
		public final static int  NORMAL = 1;
		public final static int  FINISH = 2;
		public final static int  CANCEL = 3;
		public final static int  DELETE = 4;
		public final static int  EXPIRE = 5;
	}
	/**
	 * 订单支付类型
	 * 支付方式，1：在线支付，2：服务后付款
	 */
	public static class OrderPayType{
		public final static int  ON_LINE   = 1;
		public final static int  AFTER_SERVICE = 2;
	}
	/**
	 * 订单支付渠道
	 * 在线支付渠道，1：微信支付，2：支付宝支付
	 */
	public static class OrderPayMethod{
		public final static int  WEIXIN  = 1;
		public final static int  ALIPAY  = 2;
	}
	/**
	 * 订单支付状态
	 * 支付状态，1：待支付，2：已支付
	 */
	public static class OrderPayState{
		public final static int  NO_PAY  = 1;
		public final static int  HAD_PAY = 2;
	}
	/**
	 * 派单状态
	 * 派单状态，1：待派单，2：已派单
	 */
	public static class OrderDispatchState{
		public final static int NO_DISPATCH = 1;
		public final static int HAD_DISPATCH= 2;
	}
}
