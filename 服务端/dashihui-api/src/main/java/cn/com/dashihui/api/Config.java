package cn.com.dashihui.api;

import java.util.Map;

import com.google.common.collect.Maps;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;

import cn.com.dashihui.api.controller.AdController;
import cn.com.dashihui.api.controller.CommonController;
import cn.com.dashihui.api.controller.GoodsController;
import cn.com.dashihui.api.controller.OrderController;
import cn.com.dashihui.api.controller.OrderPayNotifyController;
import cn.com.dashihui.api.controller.SearchController;
import cn.com.dashihui.api.controller.SerOrderController;
import cn.com.dashihui.api.controller.SerOrderPayNotifyController;
import cn.com.dashihui.api.controller.ServiceOrderController;
import cn.com.dashihui.api.controller.ServiceOrderPayNotifyController;
import cn.com.dashihui.api.controller.ServiceShopController;
import cn.com.dashihui.api.controller.StoreController;
import cn.com.dashihui.api.controller.UserController;
import cn.com.dashihui.api.dao.Ad;
import cn.com.dashihui.api.dao.ApiClient;
import cn.com.dashihui.api.dao.Category;
import cn.com.dashihui.api.dao.Community;
import cn.com.dashihui.api.dao.CommunityDetail;
import cn.com.dashihui.api.dao.Feedback;
import cn.com.dashihui.api.dao.Goods;
import cn.com.dashihui.api.dao.Keyword;
import cn.com.dashihui.api.dao.Order;
import cn.com.dashihui.api.dao.OrderPayAPIRecord;
import cn.com.dashihui.api.dao.OrderRefund;
import cn.com.dashihui.api.dao.SerOrder;
import cn.com.dashihui.api.dao.SerOrderPayAPIRecord;
import cn.com.dashihui.api.dao.ServiceCategory;
import cn.com.dashihui.api.dao.ServiceOrder;
import cn.com.dashihui.api.dao.ServiceOrderPayAPIRecord;
import cn.com.dashihui.api.dao.ServiceShop;
import cn.com.dashihui.api.dao.ServiceShopItem;
import cn.com.dashihui.api.dao.Store;
import cn.com.dashihui.api.dao.StoreAd;
import cn.com.dashihui.api.dao.User;
import cn.com.dashihui.api.dao.UserAddress;
import cn.com.dashihui.api.dao.UserCollection;
import cn.com.dashihui.api.dao.Version;
import cn.com.dashihui.api.handler.ContextParamsHandler;
import cn.com.dashihui.api.handler.SessionHandler;
import cn.com.dashihui.api.interceptor.AuthClientTokenInterceptor;

public class Config extends JFinalConfig {
	
	public void configConstant(Constants me) {
		// 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
		loadPropertyFile("config.properties");
		//设置视图根目录
		me.setBaseViewPath("/WEB-INF/page");
	    //设置字符集
	    me.setEncoding("UTF-8");
	    me.setViewType(ViewType.JSP);
	    //调试模式，会打印详细日志
		me.setDevMode(getPropertyToBoolean("constants.devMode", false));
		//上传文件配置
		//注：此处的目录是上级目录，真正在Controller中获取上传的文件时，需要指定（也可以不指定）要将文件移到什么目录下（）相对于此处指定的目录
		//比如在此设置目录为<E:/>，而在Controller中<getFile("fileName","upload")>，则会将文件移至<E:/upload>目录中，如果不指定则移至<E:/>目录中
		//OreillyCos.init(PathKit.getWebRootPath()+File.separator+"upload", 10*1024*1024, "UTF-8");
	}
	
	public void configRoute(Routes me) {
		me.add("/common", CommonController.class);
		me.add("/search", SearchController.class);
		//用户系列接口
		me.add("/user", UserController.class);
		//店铺系列接口
		me.add("/store", StoreController.class);
		//商品系列接口
		me.add("/goods", GoodsController.class);
		//广告位轮播图系列接口
		me.add("/ad", AdController.class);
		//商品订单系列接口
		me.add("/order", OrderController.class);
		me.add("/notify", OrderPayNotifyController.class);
		//服务系列接口
		me.add("/service", ServiceShopController.class);
		me.add("/service/order", ServiceOrderController.class);
		me.add("/service/notify", ServiceOrderPayNotifyController.class);
		//家政类服务系列接口
		me.add("/ser/order", SerOrderController.class);
		me.add("/ser/notify", SerOrderPayNotifyController.class);
	}
	
	public void configPlugin(Plugins me) {
		//缓存
		EhCachePlugin ecp = new EhCachePlugin();
		me.add(ecp);
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("db.jdbcUrl"), getProperty("db.username"), getProperty("db.password"), getProperty("db.jdbcDriver"));
		c3p0Plugin.setMaxPoolSize(getPropertyToInt("db.maxPoolSize"));
		c3p0Plugin.setMinPoolSize(getPropertyToInt("db.minPoolSize"));
		c3p0Plugin.setInitialPoolSize(getPropertyToInt("db.initialPoolSize"));
		c3p0Plugin.setMaxIdleTime(getPropertyToInt("db.maxIdleTime"));
		c3p0Plugin.setAcquireIncrement(getPropertyToInt("db.acquireIncrement"));
		me.add(c3p0Plugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setDialect(new MysqlDialect());
		//字段名大写
		arp.setContainerFactory(new CaseInsensitiveContainerFactory());
		arp.setShowSql(true);
		me.add(arp);
		//添加model映射
		arp.addMapping("t_api_client", ApiClient.class);
		//公共轮播图
		arp.addMapping("t_bus_ad", Ad.class);
		//社区及社区详情系列
		arp.addMapping("t_dict_community", Community.class);
		arp.addMapping("t_dict_community_detail", CommunityDetail.class);
		//店铺表
		arp.addMapping("t_dict_store", Store.class);
		//搜索关键字
		arp.addMapping("t_dict_keyword", Keyword.class);
		//用户表
		arp.addMapping("t_bus_user", User.class);
		//用户收藏表
		arp.addMapping("t_bus_user_collection", UserCollection.class);
		//用户收货地址
		arp.addMapping("t_bus_user_address", UserAddress.class);
		//商品分类
		arp.addMapping("t_dict_category", Category.class);
		//店铺商品表
		arp.addMapping("t_bus_goods", Goods.class);
		//店铺轮播图
		arp.addMapping("t_bus_store_ad", StoreAd.class);
		//商品订单系列
		arp.addMapping("t_bus_order", Order.class);
		arp.addMapping("t_bus_order_apirecord", OrderPayAPIRecord.class);
		//服务系列
		arp.addMapping("t_bus_service_category", ServiceCategory.class);
		arp.addMapping("t_bus_service_shop", ServiceShop.class);
		arp.addMapping("t_bus_service_shop_item", ServiceShopItem.class);
		arp.addMapping("t_bus_service_order", ServiceOrder.class);
		arp.addMapping("t_bus_service_order_apirecord", ServiceOrderPayAPIRecord.class);
		//家政服务系列
		arp.addMapping("t_bus_ser_order", SerOrder.class);
		arp.addMapping("t_bus_ser_order_apirecord", SerOrderPayAPIRecord.class);
		//意见反馈及版本管理系列
		arp.addMapping("t_sys_feedback", Feedback.class);
		arp.addMapping("t_sys_version", Version.class);
		arp.addMapping("t_bus_order_refund", OrderRefund.class);
	}
	
	public void configInterceptor(Interceptors me) {
		me.add(new SessionInViewInterceptor());
		me.add(new AuthClientTokenInterceptor());
	}
	
	public void configHandler(Handlers me) {
		//可在此设置context_path，解决http://ip:port/context_path的问题
		//因为测试时是在jetty下，所以默认没有context_path，如果部署在tomcat下，会自动加上项目名，所以会用到该配置
		//可自定义context_path，默认下是CONTEXT_PATH，使用如：${CONTEXT_PATH}
		me.add(new ContextPathHandler("BASE_PATH"));

		//需要在页面传递的常量
		Map<String,Object> params = Maps.newHashMap();
		params.put("FTP_PATH", PropKit.get("constants.ftppath"));
		me.add(new ContextParamsHandler(params));
		me.add(new SessionHandler());
	}
}
