package cn.com.dashihui.api.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.api.common.OrderCode;
import cn.com.dashihui.api.common.SerOrderCode;
import cn.com.dashihui.api.common.ServiceOrderCode;
import cn.com.dashihui.api.dao.User;
import cn.com.dashihui.api.dao.UserAddress;

public class UserService {
	
	/**
	 * 根据sessionid查询对应登录用户信息，用于已经登录，只是查询用户基本信息返回给客户端，所以不再查询用户名及密码字段
	 * 注：需要加条件限制state=1，以限制用户不允许多客户端，目前没有加，2016-03-05
	 */
	public User findUserInfoBySessionid(String sessionid){
		return User.me().findFirst("SELECT"
				+ " u.*,"
				+ "dc.id communityid,"
				+ "dc.title communityName,"
				+ "build.id buildid,"
				+ "build.name buildName,"
				+ "unit.id unitid,"
				+ "unit.name unitName,"
				+ "room.id roomid,"
				+ "room.name roomName,"
				+ "(SELECT COUNT(*) FROM t_bus_user_collection buc WHERE buc.userid=u.id AND buc.isCancel=0)!=0 CollectedCount"
				+ " FROM t_bus_user u"
				+ " INNER JOIN t_api_client_session cs ON u.id=cs.userid"
				+ " LEFT JOIN t_dict_community dc ON u.communityid=dc.id"
				+ " LEFT JOIN t_dict_community_detail build ON u.buildid=build.id AND build.type=1"
				+ " LEFT JOIN t_dict_community_detail unit ON u.unitid=unit.id AND unit.type=2"
				+ " LEFT JOIN t_dict_community_detail room ON u.roomid=room.id AND room.type=3"
				+ " WHERE cs.sessionid=?",sessionid);
	}
	
	/**
	 * 查询指定用户名的用户信息，用于登录密码验证或验证指定用户名用户是否存在，所以只查询用户名和密码字段
	 */
	public User findByUsername(String username){
		return User.me().findFirst("SELECT id,username,password FROM t_bus_user WHERE username=?",username);
	}
	
	/**
	 * 查询指定ID的用户信息
	 */
	public User findUserInfoById(int userid){
		return User.me().findFirst("SELECT"
				+ " u.*,"
				+ "dc.id communityid,"
				+ "dc.title communityName,"
				+ "build.id buildid,"
				+ "build.name buildName,"
				+ "unit.id unitid,"
				+ "unit.name unitName,"
				+ "room.id roomid,"
				+ "room.name roomName"
				+ " FROM t_bus_user u"
				+ " LEFT JOIN t_dict_community dc ON u.communityid=dc.id"
				+ " LEFT JOIN t_dict_community_detail build ON u.buildid=build.id AND build.type=1"
				+ " LEFT JOIN t_dict_community_detail unit ON u.unitid=unit.id AND unit.type=2"
				+ " LEFT JOIN t_dict_community_detail room ON u.roomid=room.id AND room.type=3"
				+ " WHERE u.id=?",userid);
	}
	
	/**
	 * 用户注册
	 */
	public boolean save(User user){
		return user.save();
	}
	
	/**
	 * 用户登录
	 */
	@Before(Tx.class)
	public boolean login(String sessionid,int userid,String clientid){
		Db.update("UPDATE T_API_CLIENT_SESSION SET state=2 WHERE clientid=? OR userid=?",clientid,userid);
		Db.update("INSERT INTO T_API_CLIENT_SESSION(sessionid,userid,clientid,state) VALUE(?,?,?,?)",sessionid,userid,clientid,1);
		return true;
	}
	
	/**
	 * 统计用户各状态数量
	 * 1.统计用户收藏的商品数量
	 * 2.统计查询“待付款”的订单数量
	 * 3.统计查询“待发货”的订单数量
	 * 4.统计查询“待签收”的订单数量
	 * 5.统计服务订单“待付款”的订单数量
	 * 6.统计服务订单“待服务”的订单数量
	 */
	public Record getUserState(int userid){
		StringBuffer sqlBuffer = new StringBuffer("SELECT ");
		List<Object> sqlParams = new ArrayList<Object>();
		//1.统计用户收藏的商品数量
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_user_collection buc WHERE buc.userid=? AND buc.isCancel=0) state1,");
		sqlParams.add(userid);
		
		//2.统计“待付款”的商品订单数量（在线支付、未付款、正常）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND payType=? AND payState=? AND orderState=?) state2,");
		sqlParams.add(userid);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);
		sqlParams.add(OrderCode.OrderPayState.NO_PAY);
		sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//3.统计查询“待发货”的订单数量（1：“在线支付+送货上门”、“已支付”、“未发货”、“正常”，2：“货到付款+送货上门”、“未发货”、“正常”）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND takeType=?");
		sqlBuffer.append(" AND (");
		sqlBuffer.append("(payType=? AND payState=?)");
		sqlBuffer.append(" OR payType=?");
		sqlBuffer.append(") AND deliverState=? AND orderState=?) state3,");
		sqlParams.add(userid);
		sqlParams.add(OrderCode.OrderTakeType.DELIVER);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);sqlParams.add(OrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(OrderCode.OrderPayType.ON_DELIVERY);
		sqlParams.add(OrderCode.OrderDeliverState.NO_DELIVER);sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//4.统计查询“待签收”的订单数量（1：“送货上门”、“已发货”、“正常”，2：“在线支付+门店自取”、“已支付”、“正常”，3：“货到付款+门店自取”、“正常”）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=?");
		sqlBuffer.append(" AND (");
		sqlBuffer.append("(takeType=? AND deliverState=?)");
		sqlBuffer.append(" OR (payType=? AND takeType=? AND payState=?)");
		sqlBuffer.append(" OR (payType=? AND takeType=?)");
		sqlBuffer.append(") AND orderState=?) state4,");
		sqlParams.add(userid);
		sqlParams.add(OrderCode.OrderTakeType.DELIVER);sqlParams.add(OrderCode.OrderDeliverState.HAD_DELIVER);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);sqlParams.add(OrderCode.OrderTakeType.TAKE_SELF);sqlParams.add(OrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(OrderCode.OrderPayType.ON_DELIVERY);sqlParams.add(OrderCode.OrderTakeType.TAKE_SELF);
		sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//5.统计查询“待付款”的服务订单数量+家政服务订单数量（在线支付、未付款、正常）
		sqlBuffer.append("(");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_service_order WHERE userid=? AND payType=? AND payState=? AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(ServiceOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(ServiceOrderCode.OrderPayState.NO_PAY);
		sqlParams.add(ServiceOrderCode.OrderState.NORMAL);
		sqlBuffer.append("+");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_ser_order WHERE userid=? AND payType=? AND payState=? AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(SerOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(SerOrderCode.OrderPayState.NO_PAY);
		sqlParams.add(SerOrderCode.OrderState.NORMAL);
		sqlBuffer.append(") state5,");
		
		//6.统计查询“待服务”的服务订单数量+家政服务订单数量（（在线支付、已付款、正常）或（服务后付款、正常））
		sqlBuffer.append("(");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_service_order WHERE userid=? AND ((payType=? AND payState=?) OR payType=?) AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(ServiceOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(ServiceOrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(ServiceOrderCode.OrderPayType.AFTER_SERVICE);
		sqlParams.add(ServiceOrderCode.OrderState.NORMAL);
		sqlBuffer.append("+");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_ser_order WHERE userid=? AND ((payType=? AND payState=?) OR payType=?) AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(SerOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(SerOrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(SerOrderCode.OrderPayType.AFTER_SERVICE);
		sqlParams.add(SerOrderCode.OrderState.NORMAL);
		sqlBuffer.append(") state6");
		return Db.findFirst(sqlBuffer.toString(),sqlParams.toArray());
	}
	public Record getUserState_v131(int storeid,int userid){
		StringBuffer sqlBuffer = new StringBuffer("SELECT ");
		List<Object> sqlParams = new ArrayList<Object>();
		//1.统计用户收藏的商品数量
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_user_collection buc WHERE buc.userid=? AND (buc.storeid=? OR buc.isSelf=1) AND buc.isCancel=0) state1,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		
		//2.统计“待付款”的商品订单数量（在线支付、未付款、正常）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND (storeid=? OR isSelf=1) AND payType=? AND payState=? AND orderState=?) state2,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);
		sqlParams.add(OrderCode.OrderPayState.NO_PAY);
		sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//3.统计查询“待发货”的订单数量（1：“在线支付+送货上门”、“已支付”、“未发货”、“正常”，2：“货到付款+送货上门”、“未发货”、“正常”）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND (storeid=? OR isSelf=1) AND takeType=?");
		sqlBuffer.append(" AND (");
		sqlBuffer.append("(payType=? AND payState=?)");
		sqlBuffer.append(" OR payType=?");
		sqlBuffer.append(") AND deliverState=? AND orderState=?) state3,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(OrderCode.OrderTakeType.DELIVER);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);sqlParams.add(OrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(OrderCode.OrderPayType.ON_DELIVERY);
		sqlParams.add(OrderCode.OrderDeliverState.NO_DELIVER);sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//4.统计查询“待签收”的订单数量（1：“送货上门”、“已发货”、“正常”，2：“在线支付+门店自取”、“已支付”、“正常”，3：“货到付款+门店自取”、“正常”）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND (storeid=? OR isSelf=1)");
		sqlBuffer.append(" AND (");
		sqlBuffer.append("(takeType=? AND deliverState=?)");
		sqlBuffer.append(" OR (payType=? AND takeType=? AND payState=?)");
		sqlBuffer.append(" OR (payType=? AND takeType=?)");
		sqlBuffer.append(") AND orderState=?) state4,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(OrderCode.OrderTakeType.DELIVER);sqlParams.add(OrderCode.OrderDeliverState.HAD_DELIVER);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);sqlParams.add(OrderCode.OrderTakeType.TAKE_SELF);sqlParams.add(OrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(OrderCode.OrderPayType.ON_DELIVERY);sqlParams.add(OrderCode.OrderTakeType.TAKE_SELF);
		sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//5.统计查询“待付款”的服务订单数量+家政服务订单数量（在线支付、未付款、正常）
		sqlBuffer.append("(");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_service_order WHERE userid=? AND storeid=? AND payType=? AND payState=? AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(ServiceOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(ServiceOrderCode.OrderPayState.NO_PAY);
		sqlParams.add(ServiceOrderCode.OrderState.NORMAL);
		sqlBuffer.append("+");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_ser_order WHERE userid=? AND storeid=? AND payType=? AND payState=? AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(SerOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(SerOrderCode.OrderPayState.NO_PAY);
		sqlParams.add(SerOrderCode.OrderState.NORMAL);
		sqlBuffer.append(") state5,");
		
		//6.统计查询“待服务”的服务订单数量+家政服务订单数量（（在线支付、已付款、正常）或（服务后付款、正常））
		sqlBuffer.append("(");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_service_order WHERE userid=? AND storeid=? AND ((payType=? AND payState=?) OR payType=?) AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(ServiceOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(ServiceOrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(ServiceOrderCode.OrderPayType.AFTER_SERVICE);
		sqlParams.add(ServiceOrderCode.OrderState.NORMAL);
		sqlBuffer.append("+");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_ser_order WHERE userid=? AND storeid=? AND ((payType=? AND payState=?) OR payType=?) AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(SerOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(SerOrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(SerOrderCode.OrderPayType.AFTER_SERVICE);
		sqlParams.add(SerOrderCode.OrderState.NORMAL);
		sqlBuffer.append(") state6");
		return Db.findFirst(sqlBuffer.toString(),sqlParams.toArray());
	}
	public Record getUserState_v132(int storeid,int userid){
		StringBuffer sqlBuffer = new StringBuffer("SELECT ");
		List<Object> sqlParams = new ArrayList<Object>();
		//1.统计用户收藏的商品数量
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_user_collection buc WHERE buc.userid=? AND (buc.storeid=? OR buc.isSelf=1) AND buc.isCancel=0) state1,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		
		//2.统计“待付款”的商品订单数量（在线支付、未付款、正常）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND (storeid=? OR isSelf=1) AND payType=? AND payState=? AND orderState=?) state2,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);
		sqlParams.add(OrderCode.OrderPayState.NO_PAY);
		sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//3.统计查询“待收/取货”的订单数量（1：“在线支付”、“已支付”、“正常”，2：“货到付款”、“正常”）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND (storeid=? OR isSelf=1)");
		sqlBuffer.append(" AND (");
		sqlBuffer.append("(payType=? AND payState=?)");
		sqlBuffer.append(" OR payType=?");
		sqlBuffer.append(") AND orderState=?) state3,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);sqlParams.add(OrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(OrderCode.OrderPayType.ON_DELIVERY);
		sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//4.统计查询“待评价”的订单数量（1：“已完成”、“未评价”）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND (storeid=? OR isSelf=1) AND orderState=? AND evalState=0) state4,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(OrderCode.OrderState.FINISH);
		
		//5.统计查询“待付款”的服务订单数量+家政服务订单数量（在线支付、未付款、正常）
		sqlBuffer.append("(");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_service_order WHERE userid=? AND storeid=? AND payType=? AND payState=? AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(ServiceOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(ServiceOrderCode.OrderPayState.NO_PAY);
		sqlParams.add(ServiceOrderCode.OrderState.NORMAL);
		sqlBuffer.append("+");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_ser_order WHERE userid=? AND storeid=? AND payType=? AND payState=? AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(SerOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(SerOrderCode.OrderPayState.NO_PAY);
		sqlParams.add(SerOrderCode.OrderState.NORMAL);
		sqlBuffer.append(") state5,");
		
		//6.统计查询“待服务”的服务订单数量+家政服务订单数量（（在线支付、已付款、正常）或（服务后付款、正常））
		sqlBuffer.append("(");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_service_order WHERE userid=? AND storeid=? AND ((payType=? AND payState=?) OR payType=?) AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(ServiceOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(ServiceOrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(ServiceOrderCode.OrderPayType.AFTER_SERVICE);
		sqlParams.add(ServiceOrderCode.OrderState.NORMAL);
		sqlBuffer.append("+");
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_ser_order WHERE userid=? AND storeid=? AND ((payType=? AND payState=?) OR payType=?) AND orderState=?)");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(SerOrderCode.OrderPayType.ON_LINE);
		sqlParams.add(SerOrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(SerOrderCode.OrderPayType.AFTER_SERVICE);
		sqlParams.add(SerOrderCode.OrderState.NORMAL);
		sqlBuffer.append(") state6");
		return Db.findFirst(sqlBuffer.toString(),sqlParams.toArray());
	}
	
	/**
	 * 用户信息更新
	 */
	public boolean update(User user){
		return user.update();
	}
	/**
	 * 更新用户登录密码
	 */
	public boolean updatePwdByUsername(String password,String username){
		String sql = "UPDATE t_bus_user SET password=? WHERE username=?";
		return Db.update(sql, password, username)==1;
	}
	/**
	 * 用户收藏指定商品<br/>
	 * 因为收藏表中记录为了统计，所以用户取消收藏是不会将记录删除的，只是修改了标识isCancel为1<br/>
	 * 所以在此业务中需要判断同一用户同一商品是否已经存在，如果存在且是取消收藏状态，则只需要修改标识为0即可<br/>
	 * 如果不存在，则需要新插入记录<br/>
	 * 否则认为同一用户已经收藏过同一商品，直接返回true
	 */
	public void doCollect(int storeid,int userid,String[] goodsids,String[] isSelfs){
		//批量删除
		String delSql = "DELETE FROM t_bus_user_collection WHERE userid=? AND (storeid=? OR isSelf=1) AND goodsid=?";
		Object[][] params1 = new Object[goodsids.length][3];
		for(int i=0;i<goodsids.length;i++){
			params1[i][0] = userid;
			params1[i][1] = storeid;
			params1[i][2] = Integer.valueOf(goodsids[i]);
		}
		String addSql = "INSERT INTO t_bus_user_collection(userid,storeid,goodsid,isSelf) VALUES(?,?,?,?)";
		Db.batch(delSql, params1, goodsids.length);
		
		//批量插入
		Object[][] params2 = new Object[goodsids.length][4];
		for(int i=0;i<goodsids.length;i++){
			params2[i][0] = userid;
			params2[i][1] = storeid;
			params2[i][2] = Integer.valueOf(goodsids[i]);
			params2[i][3] = Integer.valueOf(isSelfs[i]);
		}
		Db.batch(addSql, params2, goodsids.length);
	}
	
	/**
	 * 用户取消收藏指定商品<br/>
	 */
	public void cancelCollect(int userid,String[] goodsids){
		String sql = "UPDATE t_bus_user_collection SET isCancel=1 WHERE userid=? AND goodsid=?";
		Object[][] params = new Object[goodsids.length][2];
		for(int index = 0;index<goodsids.length;index++){
			params[index][0] = userid;
			params[index][1] = goodsids[index];
		}
		Db.batch(sql, params, goodsids.length);
	}
	
	/**
	 * 查询用户收藏商品列表
	 * @param storeid 店铺ID
	 * @param categoryCode 主分类代码
	 * @param type 优惠类型
	 * @param pageNum 页码
	 * @param pageSize 数量
	 */
	public Page<Record> findCollectionByPage(int storeid,int userid,int pageNum,int pageSize){
		String sqlSelect = "SELECT coled.* ";
		String sqlExceptSelect = "FROM (SELECT bg.id,bg.name,bg.spec,bg.marketPrice,bg.sellPrice,bg.thumb,bg.type,bg.urv,bg.isSelf,uc.createDate collectDate"
					+ " FROM t_bus_user_collection uc"
					+ " INNER JOIN t_bus_goods bg ON uc.goodsid=bg.id"
					+ " WHERE uc.userid=? AND (uc.storeid=? OR uc.isSelf=1) AND uc.isCancel=0) coled ORDER BY coled.collectDate DESC";
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlExceptSelect, userid, storeid);
	}
	
	/**
	 * 查询用户收货地址列表
	 * @param userid 用户ID
	 */
	public List<UserAddress> findAddress(int userid){
		return UserAddress.me().find("SELECT ua.id,ua.linkName,ua.sex,ua.tel,"
				+ "ua.storeid,"
				+ "store.title storeTitle,"
				+ "ua.address,"
				+ "ua.isDefault"
				+ " FROM t_bus_user_address ua"
				+ " LEFT JOIN t_dict_store store ON ua.storeid=store.id"
				+ " WHERE ua.userid=?",userid);
	}
	
	/**
	 * 添加收货地址<br/>
	 * 1.如果本次添加的收货地址，要求设置为默认，则更新该用户的其他收货地址为非默认（在此不再查询用户是否还有其他收货地址，直接UPDATE）
	 * 2.如果本次添加的收货地址，没有要求设置为默认，则查询该用户是否还有其他收货地址，如果没有，则同样设置本次为默认（因为只有一条收货地址时，应当设置为默认）
	 */
	@Before(Tx.class)
	public boolean addAddress(int userid,UserAddress address,int isDefault){
		address.set("isDefault", 0);
		if(isDefault==1){
			Db.update("UPDATE t_bus_user_address SET isDefault=0 WHERE userid=?",userid);
			address.set("isDefault", 1);
		}else{
			Record addrCount = Db.findFirst("SELECT count(*) total FROM t_bus_user_address WHERE userid=?",userid);
			if(addrCount.getLong("total").intValue()==0){
				address.set("isDefault", 1);
			}
		}
		return address.save();
	}
	
	/**
	 * 删除收货地址
	 */
	public void delAddress(int userid,String[] addressids){
		String sql = "DELETE FROM t_bus_user_address WHERE userid=? AND id=?";
		Object[][] params = new Object[addressids.length][2];
		for(int index = 0;index<addressids.length;index++){
			params[index][0] = userid;
			params[index][1] = addressids[index];
		}
		Db.batch(sql, params, addressids.length);
	}
	
	/**
	 * 修改收货地址<br/>
	 * 如果本次添加的收货地址，要求设置为默认，则更新该用户的其他收货地址为非默认（在此不再查询用户是否还有其他收货地址，直接UPDATE）
	 */
	public boolean updateAddress(int userid,UserAddress address,int isDefault){
		address.set("isDefault", 0);
		if(isDefault==1){
			Db.update("UPDATE t_bus_user_address SET isDefault=0 WHERE userid=?",userid);
			address.set("isDefault", 1);
		}
		return address.update();
	}
	
	/**
	 * 查找指定ID的收货地址信息
	 */
	public UserAddress findAddressById(int addressid){
		return UserAddress.me().findFirst("SELECT ua.id,ua.linkName,ua.sex,ua.tel,"
				+ "ua.storeid,"
				+ "store.title storeTitle,"
				+ "ua.address,"
				+ "ua.isDefault"
				+ " FROM t_bus_user_address ua"
				+ " LEFT JOIN t_dict_store store ON ua.storeid=store.id"
				+ " WHERE ua.id=?",addressid);
	}
	
	/**
	 * 查找指定用户的默认收货地址信息
	 */
	public UserAddress findDefaultAddressByUser(int userid){
		return UserAddress.me().findFirst("SELECT ua.id,ua.linkName,ua.sex,ua.tel,"
				+ "ua.storeid,"
				+ "store.title storeTitle,"
				+ "ua.address,"
				+ "ua.isDefault"
				+ " FROM t_bus_user_address ua"
				+ " LEFT JOIN t_dict_store store ON ua.storeid=store.id"
				+ " WHERE ua.isDefault=1 AND ua.userid=?",userid);
	}
	
	/**
	 * 查询用户服务订单列表，包含家政类订单及其他类订单
	 * @param storeid 店铺ID
	 * @param userid 用户ID
	 * @param flag 0：全部，1：待付款，2：待服务，3：已完成订单
	 */
	public Page<Record> findServiceOrderByPage(int storeid,int userid,int flag,int pageNum,int pageSize){
		StringBuffer sqlBuffer = new StringBuffer("FROM (");
		//根据不同查询条件，拼上不同的状态条件
		if(flag==1){
			//1:待付款（“在线支付”、“未支付”、“正常”）
			sqlBuffer.append("SELECT ");
			sqlBuffer.append("sjo.storeid,sjo.orderNum,1 type,sjo.userid,(CASE WHEN sjo.type=1 THEN CONCAT('日常保洁',title) ELSE CONCAT('深度保洁',title) END) serviceTitle,");
			sqlBuffer.append("sjo.payType,sjo.payState,sjo.deliverState,sjo.orderState,sjo.serTime,sjo.address,sjo.proSerName,sjo.amount,sjo.startDate,sjo.createDate");
			sqlBuffer.append(" FROM t_bus_ser_order sjo");
			sqlBuffer.append(" WHERE sjo.storeid=").append(storeid);
			sqlBuffer.append(" AND sjo.orderState=").append(SerOrderCode.OrderState.NORMAL);
			sqlBuffer.append(" AND sjo.payType=").append(SerOrderCode.OrderPayType.ON_LINE);
			sqlBuffer.append(" AND sjo.payState=").append(SerOrderCode.OrderPayState.NO_PAY);
			sqlBuffer.append(" UNION ");
			sqlBuffer.append("SELECT ");
			sqlBuffer.append("soo.storeid,soo.orderNum,2 type,soo.userid,sool.serviceTitle,soo.payType,soo.payState,soo.deliverState,soo.orderState,soo.serTime,soo.address,sool.shopName proSerName,soo.amount,soo.startDate,soo.createDate");
			sqlBuffer.append(" FROM t_bus_service_order soo");
			sqlBuffer.append(" INNER JOIN t_bus_service_order_list sool ON soo.orderNum=sool.orderNum");
			sqlBuffer.append(" WHERE soo.storeid=").append(storeid);
			sqlBuffer.append(" AND soo.orderState=").append(ServiceOrderCode.OrderState.NORMAL);
			sqlBuffer.append(" AND soo.payType=").append(ServiceOrderCode.OrderPayType.ON_LINE);
			sqlBuffer.append(" AND soo.payState=").append(ServiceOrderCode.OrderPayState.NO_PAY);
		}else if(flag==2){
			//2：待服务（1：“在线支付”、“已支付”、“正常”，2：“服务后付款”、“正常”）
			sqlBuffer.append("SELECT ");
			sqlBuffer.append("sjo.storeid,sjo.orderNum,1 type,sjo.userid,(CASE WHEN sjo.type=1 THEN CONCAT('日常保洁',title) ELSE CONCAT('深度保洁',title) END) serviceTitle,");
			sqlBuffer.append("sjo.payType,sjo.payState,sjo.deliverState,sjo.orderState,sjo.serTime,sjo.address,sjo.proSerName,sjo.amount,sjo.startDate,sjo.createDate");
			sqlBuffer.append(" FROM t_bus_ser_order sjo");
			sqlBuffer.append(" WHERE sjo.storeid=").append(storeid);
			sqlBuffer.append(" AND sjo.orderState=").append(SerOrderCode.OrderState.NORMAL);
			sqlBuffer.append(" AND ((sjo.payType=").append(SerOrderCode.OrderPayType.ON_LINE);
			sqlBuffer.append(" AND sjo.payState=").append(SerOrderCode.OrderPayState.HAD_PAY).append(")");
			sqlBuffer.append(" OR sjo.payType=").append(SerOrderCode.OrderPayType.AFTER_SERVICE).append(")");
			sqlBuffer.append(" UNION ");
			sqlBuffer.append("SELECT ");
			sqlBuffer.append("soo.storeid,soo.orderNum,2 type,soo.userid,sool.serviceTitle,soo.payType,soo.payState,soo.deliverState,soo.orderState,soo.serTime,soo.address,sool.shopName proSerName,soo.amount,soo.startDate,soo.createDate");
			sqlBuffer.append(" FROM t_bus_service_order soo");
			sqlBuffer.append(" INNER JOIN t_bus_service_order_list sool ON soo.orderNum=sool.orderNum");
			sqlBuffer.append(" WHERE soo.storeid=").append(storeid);
			sqlBuffer.append(" AND soo.orderState=").append(ServiceOrderCode.OrderState.NORMAL);
			sqlBuffer.append(" AND ((soo.payType=").append(ServiceOrderCode.OrderPayType.ON_LINE);
			sqlBuffer.append(" AND soo.payState=").append(ServiceOrderCode.OrderPayState.HAD_PAY).append(")");
			sqlBuffer.append(" OR soo.payType=").append(ServiceOrderCode.OrderPayType.AFTER_SERVICE).append(")");
		}else if(flag==3){
			//3：已完成（“已完成”）
			sqlBuffer.append("SELECT ");
			sqlBuffer.append("sjo.storeid,sjo.orderNum,1 type,sjo.userid,(CASE WHEN sjo.type=1 THEN CONCAT('日常保洁',title) ELSE CONCAT('深度保洁',title) END) serviceTitle,");
			sqlBuffer.append("sjo.payType,sjo.payState,sjo.deliverState,sjo.orderState,sjo.serTime,sjo.address,sjo.proSerName,sjo.amount,sjo.startDate,sjo.createDate");
			sqlBuffer.append(" FROM t_bus_ser_order sjo");
			sqlBuffer.append(" WHERE sjo.storeid=").append(storeid);
			sqlBuffer.append(" AND sjo.orderState=").append(SerOrderCode.OrderState.FINISH);
			sqlBuffer.append(" UNION ");
			sqlBuffer.append("SELECT ");
			sqlBuffer.append("soo.storeid,soo.orderNum,2 type,soo.userid,sool.serviceTitle,soo.payType,soo.payState,soo.deliverState,soo.orderState,soo.serTime,soo.address,sool.shopName proSerName,soo.amount,soo.startDate,soo.createDate");
			sqlBuffer.append(" FROM t_bus_service_order soo");
			sqlBuffer.append(" INNER JOIN t_bus_service_order_list sool ON soo.orderNum=sool.orderNum");
			sqlBuffer.append(" WHERE soo.storeid=").append(storeid);
			sqlBuffer.append(" AND soo.orderState=").append(ServiceOrderCode.OrderState.FINISH);
		}else{
			//0：全部，不显示“删除”、“过期”的订单
			sqlBuffer.append("SELECT ");
			sqlBuffer.append("sjo.storeid,sjo.orderNum,1 type,sjo.userid,(CASE WHEN sjo.type=1 THEN CONCAT('日常保洁',title) ELSE CONCAT('深度保洁',title) END) serviceTitle,");
			sqlBuffer.append("sjo.payType,sjo.payState,sjo.deliverState,sjo.orderState,sjo.serTime,sjo.address,sjo.proSerName,sjo.amount,sjo.startDate,sjo.createDate");
			sqlBuffer.append(" FROM t_bus_ser_order sjo");
			sqlBuffer.append(" WHERE sjo.storeid=").append(storeid);
			sqlBuffer.append(" AND (sjo.orderState=").append(SerOrderCode.OrderState.NORMAL);
			sqlBuffer.append(" OR sjo.orderState=").append(SerOrderCode.OrderState.FINISH);
			sqlBuffer.append(" OR sjo.orderState=").append(SerOrderCode.OrderState.CANCEL);
			sqlBuffer.append(") UNION ");
			sqlBuffer.append("SELECT ");
			sqlBuffer.append("soo.storeid,soo.orderNum,2 type,soo.userid,sool.serviceTitle,soo.payType,soo.payState,soo.deliverState,soo.orderState,soo.serTime,soo.address,sool.shopName proSerName,soo.amount,soo.startDate,soo.createDate");
			sqlBuffer.append(" FROM t_bus_service_order soo");
			sqlBuffer.append(" INNER JOIN t_bus_service_order_list sool ON soo.orderNum=sool.orderNum");
			sqlBuffer.append(" WHERE soo.storeid=").append(storeid);
			sqlBuffer.append(" AND (soo.orderState=").append(ServiceOrderCode.OrderState.NORMAL);
			sqlBuffer.append(" OR soo.orderState=").append(ServiceOrderCode.OrderState.FINISH);
			sqlBuffer.append(" OR soo.orderState=").append(ServiceOrderCode.OrderState.CANCEL);
			sqlBuffer.append(")");
		}
		sqlBuffer.append(")");
		sqlBuffer.append(" v INNER JOIN t_dict_store ds ON v.storeid=ds.id WHERE v.userid=? ORDER BY createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT v.*,ds.title storeName ",sqlBuffer.toString(),userid);
	}
	
	/**
	 * 查询用户实惠币变动记录
	 * @param pageNum
	 * @param pageSize
	 * @param userid
	 * @param searchType 1:我的购物收益明细; 2:好友购物收益明细; 3:我的消费记录; 4:好友购物收益合计
	 * @return
	 */
	public Page<Record> findUserExpenseRecord(int pageNum, int pageSize, int userid,int searchType){
		String sqlSelect = "SELECT buvl.* ";
		StringBuffer sqlExceptSelect = new StringBuffer("FROM t_bus_user_value_log buvl ");
		if(searchType == 1){
			sqlExceptSelect.append("WHERE buvl.userid=? AND buvl.flag=1 ");
			sqlExceptSelect.append("ORDER BY createDate DESC");
		} else if(searchType == 2){
			sqlSelect = "SELECT buvl.*,bu.nickName";
			sqlExceptSelect.append("LEFT JOIN t_bus_user bu ON buvl.fromUserid = bu.id ");
			sqlExceptSelect.append("WHERE buvl.userid=? AND buvl.flag=2 ");
			sqlExceptSelect.append("ORDER BY createDate DESC");
		} else if(searchType == 3){
			sqlExceptSelect.append("WHERE buvl.userid=? AND buvl.flag=3 ");
			sqlExceptSelect.append("ORDER BY createDate DESC");
		} else if(searchType == 4){
			sqlSelect = "SELECT g.nickName,SUM(g.amount) amount,g.fromUserid,g.createDate ";
			sqlExceptSelect.delete(0,sqlExceptSelect.length());
			sqlExceptSelect.append("FROM ( SELECT buvl.id,buvl.userid,buvl.flag,buvl.amount,buvl.fromUserid,buvl.createDate,bu.nickName ");
			sqlExceptSelect.append("FROM t_bus_user_value_log buvl INNER JOIN t_bus_user bu ");
			sqlExceptSelect.append("ON buvl.fromUserid = bu.id ");
			sqlExceptSelect.append("WHERE bu.inviteUserid = ? AND buvl.flag = 2 ) g ");
			sqlExceptSelect.append("GROUP BY g.nickName,g.fromUserid,g.createDate ");
			sqlExceptSelect.append("ORDER BY g.createDate DESC");
		}
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlExceptSelect.toString(), userid);
	}
	
	/**
	 * 查询用户推荐的好友的数量
	 * @param userid
	 * @return
	 */
	public Record findInviteUserCount(int userid){
		String sql = "SELECT count(1) AS inviteCount FROM t_bus_user WHERE inviteUserid = ?";
		return Db.findFirst(sql,userid);
	}
}
