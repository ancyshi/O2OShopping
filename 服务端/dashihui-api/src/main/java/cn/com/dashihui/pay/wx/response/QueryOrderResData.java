package cn.com.dashihui.pay.wx.response;

public class QueryOrderResData {
	
	 //协议层
    private String return_code = "";  //此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
    private String return_msg = "";
    
    //协议返回的具体数据（以下字段在return_code 为SUCCESS 的时候有返回）
    private String appid = ""; 			//公众账号ID y
    private String mch_id = "";			// 商户号 y
    private String nonce_str = "";		// 随机字符串y
    private String sign = "";			//签名y
    private String result_code = "";	//业务结果y
    
    //以下字段在return_code 和result_code都为SUCCESS的时候有返回 
    private String openid = "";			//用户在商户appid下的唯一标识
    private String trade_type =  "";	//交易类型
    private String trade_state = "";	//交易状态
    private String bank_type = "";		//付款银行
    private String total_fee = "";		//总金额
    private String cash_fee = "";		//现金支付金额
    private String transaction_id = "";	//微信支付订单号
    private String out_trade_no = "";	//商户系统的订单号，与请求一致。
    private String time_end = "";		//支付完成时间
    private String trade_state_desc = "";//交易状态描述
    
    
	public String getReturn_code() {
		return return_code;
	}
	public void setReturn_code(String return_code) {
		this.return_code = return_code;
	}
	public String getReturn_msg() {
		return return_msg;
	}
	public void setReturn_msg(String return_msg) {
		this.return_msg = return_msg;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getMch_id() {
		return mch_id;
	}
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}
	public String getNonce_str() {
		return nonce_str;
	}
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getTrade_type() {
		return trade_type;
	}
	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}
	public String getTrade_state() {
		return trade_state;
	}
	public void setTrade_state(String trade_state) {
		this.trade_state = trade_state;
	}
	public String getBank_type() {
		return bank_type;
	}
	public void setBank_type(String bank_type) {
		this.bank_type = bank_type;
	}
	public String getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}
	public String getCash_fee() {
		return cash_fee;
	}
	public void setCash_fee(String cash_fee) {
		this.cash_fee = cash_fee;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getTime_end() {
		return time_end;
	}
	public void setTime_end(String time_end) {
		this.time_end = time_end;
	}
	public String getTrade_state_desc() {
		return trade_state_desc;
	}
	public void setTrade_state_desc(String trade_state_desc) {
		this.trade_state_desc = trade_state_desc;
	}
    
    
}
