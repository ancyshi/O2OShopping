package cn.com.dashihui.api.service;

import java.util.List;

import cn.com.dashihui.api.dao.Ad;
import cn.com.dashihui.api.dao.StoreAd;

public class AdService {
	
	/**
	 * 查询公共广告位
	 */
	public List<Ad> findPublicAd(){
		return Ad.me().find("SELECT id,thumb,link,1 type,null goodsid FROM t_bus_ad ORDER BY orderNo");
	}
	
	/**
	 * 查询指定店铺的广告位（只显示在APP显示的）<br/>
	 * 注：goodsid值暂时更名为id，下一版需要删除或更正，2016-03-04
	 */
	public List<StoreAd> findStoreAd(int storeid){
		return StoreAd.me().find("SELECT goodsid id,thumb,link,type,goodsid FROM t_bus_store_ad WHERE storeid=? AND isShowOnAPP=1 ORDER BY orderNo",storeid);
	}
}
