			mui.init({
				pullRefresh: {
					container: '#indexListSubpullrefresh',
					down: {
						contentrefresh: '正在刷新最新订单...',
						callback: pulldownRefresh
					},
					up: {
						contentrefresh: '加载更多订单...',
						callback: pullupRefresh
					}
				}
			});
			var orderContainer = $("#indexListSuborderList");
			var upPageNum = 1;
			var downPageNum = 1; //不会改变 因为只会加载最新数据 
			var pageSize = 3;
			var shopid = "";
			
			/**
			 * 下拉加载最新订单
			 */
			function pulldownRefresh() { 
				setTimeout(function() {
					var orderItem = document.getElementsByClassName("orderItem");
					if(!orderItem || orderItem.length <=0){//防止数据过少下拉时加载更多的数据还没有加载到文档上 
						mui('#indexListSubpullrefresh').pullRefresh().endPulldownToRefresh(); //有最新数据
						return;
					}
					var lastdataNode = $(".startDate").eq(0);
					var lastDate = null;
					if(lastdataNode){
						//lastDate = lastdataNode.text();
						lastDate = lastdataNode.attr("data-val");
					}
					getAjaxData(pageSize,downPageNum,lastDate,"down");
				},1000);
			}
			
			/**
			 * 上拉加载更多订单 
			 */
			function pullupRefresh() {
				setTimeout(function(){
					var lastdataNode = $(".startDate").eq(-1);
					var lastDate = null;
					if(lastdataNode){
						//lastDate = lastdataNode.text();
						lastDate = lastdataNode.attr("data-val");
					}
					//mui.alert(lastDate);
					getAjaxData(pageSize,upPageNum,lastDate,'up');
				},1000);
			}
			
			/**
			 * 用户执行的接单操作
			 * @param {Object} obj
			 */
			function deliver(obj){
				Kit.ui.confirm("接单","你确定要接此订单吗？",function(){
					Kit.ui.showLoading();
					var orderNum = $(obj).attr("data-val");
					API.accept(shopid,orderNum,function(data){
						setTimeout(function(){
						
						var my= plus.webview.getWebviewById('my/index.html');
						mui.fire(my,"myOrderCount",{orderCount:1});
						//时间间隔太短 无法刷新出数据
						var manage= plus.webview.getWebviewById('order/manage.html'); 
						mui.fire(manage,'getManageData',{shopid:shopid});
						$("#"+orderNum).remove(); 
						var hasChilren = $(".orderItem");
						if(!hasChilren || hasChilren.length<=0){
							getAjaxData(pageSize,upPageNum,null,"refresh");//再次加载数据
						}
						Kit.ui.closeLoading();
						},1000);
				});
				});
			}
			
			/**
			 * 用户执行的拒单操作
			 * @param {Object} obj
			 */
			function refuse(obj){
				Kit.ui.confirm("拒单","你确定要拒绝此订单吗？",function(){
					Kit.ui.showLoading();
					var orderNum = $(obj).attr("data-val");
					API.refuse(shopid,orderNum,function(data){
						$("#"+orderNum).remove(); 
						var hasChilren = $(".orderItem");
						if(!hasChilren || hasChilren.length<=0){
							getAjaxData(pageSize,upPageNum,null,"refresh");
						}
					});
					Kit.ui.closeLoading();
				});
			}
			
			/**
			 * 页面数据请求
			 * @param {Object} pageSize 一页显示的个数
			 * @param {Object} pageNum 当前页数
			 * @param {Object} beginDate 开始时间
			 * @param {Object} flag down：下拉刷新  up：上拉加载更多  refresh：页面刷新
			 */
			function getAjaxData(pageSize,pageNum,beginDate,flag){ 
				if(flag == "down"){
					API.getOrderDown(shopid,pageSize,pageNum,1,beginDate, function(data){
						pullDown(data);
					});
				}else{
					API.getOrderUp(shopid,pageSize,pageNum,1,beginDate, function(data){
						if(flag == "up"){ 
							pullUp(data);
						}else{
							refresh(data);
						}
					});
				}
			}
			
			/**
			 * 下拉加载最新订单
			 * @param {Object} data 后台获得数据
			 */
			function pullDown(data){
				//服务器返回响应，
				var totalPage = data.TOTALPAGE;
				if(totalPage == 0 ){
					mui('#indexListSubpullrefresh').pullRefresh().endPulldownToRefresh(); //没有最新数据了
					return;
				}else{
					var list = data.LIST;
					var content = appendOrder("down",list);
					orderContainer.prepend(content);
				}
			    mui('#indexListSubpullrefresh').pullRefresh().endPulldownToRefresh();  //结束雪花加载
			}
			
			/**
			 * 上拉加载更多
			 * @param {Object} data 后台获得数据
			 */
			function pullUp(data){
				//var totalPage = data.TOTALPAGE;
				var list = data.LIST;
				if(list.length <= 0){
					//TODO
					var hasChilren = $(".orderItem");
					if(!hasChilren || hasChilren.length<=0){
						appendEmpty(); 
					}
					mui('#indexListSubpullrefresh').pullRefresh().endPullupToRefresh(true); //参数为true代表没有更多数据了。
				}else{
					var content = appendOrder("up",list);
					orderContainer.append(content);
					//有数据显示下拉加载
					mui('#indexListSubpullrefresh').pullRefresh().endPullupToRefresh(false);
				}
			}
			/**
			 * 刷新页面 相当于执行了上拉操作
			 * @param {Object} data 后台获得数据
			 */
			function refresh(data){
				var totalPage = data.TOTALPAGE;
				var list = data.LIST;
				if(list.length <= 0 || totalPage == 0){
					appendEmpty();  
				}else{
					mui('#indexListSubpullrefresh').pullRefresh().refresh(true);
					mui('#indexListSubpullrefresh').pullRefresh().setStopped(false);//开启禁止滚动
					orderContainer.append(appendOrder("up",list));
				}
			}
			
			/**
			 * 订单页面模板
			 * @param {Object} flag down 刷新最新订单  up 加载更多订单 refresh 刷新页面
			 * @param {Object} list 后台获得的数据
			 */
			function appendOrder(flag,list){ 
				var content ="";
				var index = -1;
				if(flag =="down"){
					index = list.length;
				}
				for(var i = 0;i<list.length;i++){
					if(flag =="down"){
						--index;
					}else{ 
						++index;  
					}
					var order = list[index];
					var orderNum = order.ORDERNUM;
					var payType = order.PAYTYPE==1?"在线支付":"服务后付款";
					var payState = order.PAYSTATE==1?"待付款":"已付款";
					var startDate = order.STARTDATE;
					var storeDeliverDate = order.STOREDELIVERDATE;
					var userName = order.LINKNAME;
					var tel = order.TEL;
					var address = order.ADDRESS;
					var serTime= order.SERTIME;
					var createDate = order.CREATEDATE;
					
					var type = order.TYPE == 1?"日常保洁":"深度保洁";
					var title = order.TITLE;
					var unitPrice = order.AMOUNT;
					var amount = order.AMOUNT;
					var demo = order.DESCRIBE==null?"":order.DESCRIBE;
					//template
					content +=
						"<div class='orderItem' id="+orderNum+">"+
						    "<!--订单标题-->"+
						    "<ul class='orderTitle' >"+
								"<li ></li>"+
								"<li >等待接单</li>"+
								"<li class='createDate' data-val="+createDate+"><span >下单时间: </span><span class='startDate' data-val='"+storeDeliverDate+"'>"+startDate+"</span></li>"+
						    "</ul>"+
						    "<!--下单人员信息-->"+
						    "<div class='mui-card' style='border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; '>"+
						    	"<ul class='mui-table-view'>"+
									"<li class='mui-table-view-cell  pay-state' >"+
										"<span class='mui-badge mui-pull-left mui-badge-red'>"+payType+"</span>"+
									"</li>"+
									"<li class='mui-table-view-cell'>"+
										"<span>"+userName+"</span>&nbsp;&nbsp;&nbsp;"+
										"<span>"+tel+"</span>"+
									"</li>"+
									"<li class='mui-table-view-cell'>"+
									    "<span>地址：</span>"+
									    "<span>"+address+"</span>"+
									"</li>"+
									"<li class='mui-table-view-cell'>"+
									     "<span>服务时间：</span>"+
									     "<span>"+serTime+"</span>"+
									"</li>"+
						     	"</ul>"+
						    "</div> "+
						   	"<!--服务详情-->"+
						    "<div class='mui-card service-detail' style='border-top-right-radius: 0px; border-top-left-radius: 0px;border-top:0px;'>"+
						    	"<ul class='mui-table-view'>"+
									"<li class='mui-table-view-cell'>"+
										"<span>"+type+"</span>"+
										"<div >"+
										   "<span style='margin-right:10px;'>"+title+"</span>"+
										   "<span>￥"+unitPrice+"</span>"+
										"</div>"+
										
									"</li>"+
									"<li class='mui-table-view-cell'>"+
									    "<span>总计</span>"+
									    "<div>"+
										    "<span>("+payState+")</span>"+
										    "<span style='color:red;'>￥"+amount+"</span>"+
									    "</div>"+
									   
									"</li>"+
									"<li class='mui-table-view-cell'>"+ 
									     "<span>备注：</span>"+
									     "<span >"+demo+"</span>"+
									"</li>"+
						     	"</ul>"+
						   	"</div>"+
						    "<!--接单或者是拒单操作-->"+
						    "<div class='oper-button' >"+
								"<button type='button' class='mui-btn mui-btn-blue mui-col-sm-5 mui-col-xs-5 refuse' data-amount-val="+amount+" data-val="+orderNum+" onclick='refuse(this);' >拒单</button>&nbsp;&nbsp;&nbsp;"+
							    "<button type='button' class='mui-btn mui-btn-red mui-col-sm-5 mui-col-xs-5 deliver'  data-amount-val="+amount+" data-val="+orderNum+" onclick='deliver(this);' >接单</button>"+
						    "</div>"+
						"</div>";
				}
				$("div[class='mui-content mui-text-center empty']").remove(); 
				return content; 
			}
			
			/**
			 * 空白页面 
			 * 页面没有数据时显示
			 */
			function appendEmpty(){ 
				//开启禁止滑动用户只能点击页面的刷新按钮刷新
				mui('#indexListSubpullrefresh').pullRefresh().setStopped(true); //暂时禁止滚动
				var content =
					 "<div class='mui-content mui-text-center empty'> "+
					    "<ul>"+
					    	"<li><img style='width:100px;' src='../../static/app/img/menu_bot04_empty.png'/></li>"+
					    	"<li><span>您还没有收到订单</span></li>"+
					    	"<li><button type='button' class='mui-btn mui-btn-red'>刷新</button></li>"+
					    "</ul>"+
					"</div>";
				//显示到页面
		   		orderContainer.empty().append(content);  
			}
			
			/**
			 * 进入此页必须传shopid
			 */
			mui.plusReady(function() {  
				//打开此页面需要传入得shopid
				var self = plus.webview.currentWebview();
				shopid = self.shopid;
				var firstOrderDate =null; //传null在后台会处理成服务器时间
				//getAjaxData(pageSize,upPageNum,firstOrderDate,"refresh");
				if (mui.os.plus) {
					mui.plusReady(function() {
						setTimeout(function() {
							mui('#indexListSubpullrefresh').pullRefresh().pullupLoading();
						}, 500);
					});
				} else {
					mui.ready(function() {
						mui('#indexListSubpullrefresh').pullRefresh().pullupLoading();
					});
				}
			    mui("#indexListSuborderList").on("tap",".empty",function(){
					Kit.ui.showLoading();
					setTimeout(function(){
					//获得刷新后的数据并显示在页面上 
						getAjaxData(pageSize,upPageNum,firstOrderDate,"refresh");   
						Kit.ui.closeLoading();
					},2000)
				});
			});
