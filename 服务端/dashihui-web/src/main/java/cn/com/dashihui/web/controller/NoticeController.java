package cn.com.dashihui.web.controller;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Notice;
import cn.com.dashihui.web.service.NoticeService;
import cn.com.dashihui.web.tag.SubString;

public class NoticeController extends BaseController{
	private NoticeService service = new NoticeService();
    
	/**
	 * 新闻公告列表页
	 */
    public void index(){
    	setAttr("type", getParaToInt(0,1));
        this.render("index.jsp");
    }
    
    /**
	 * 新闻公告列表异步分页加载
	 */
    public void page(){
    	int type = 1;
    	String typeStr = getPara("type");
    	if(!StrKit.isBlank(typeStr)&&(typeStr.equals("1")||typeStr.equals("2"))){
    		type = Integer.valueOf(typeStr);
    	}
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		Page<Notice> page = service.findByPage(type, pageNum, pageSize);
		if(page!=null&&page.getList()!=null){
			for(Notice notice : page.getList()){
				notice.set("context", SubString.substring(SubString.clearHTML(notice.getStr("context")),100));
			}
		}
		renderSuccess(page);
    }
    
    /**
	 * 新闻公告详情页
	 */
    public void detail(){
    	Notice notice = service.findById(getParaToInt(0));
    	Document doc = Jsoup.parse(notice.getStr("context"));
    	Elements imgs = doc.getElementsByTag("img");
    	for(Element img : imgs){
    		img.attr("width","100%");
    	}
    	notice.set("context", doc.html());
    	setAttr("detail", notice);
    	render("detail.jsp");
    }
}
