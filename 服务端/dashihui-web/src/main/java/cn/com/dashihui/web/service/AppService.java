package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class AppService {
	
	/**
	 * 查询安卓版客户端最新版记录
	 * @return
	 */
	public Record findAndroidNewest(){
		return Db.findFirst("SELECT * FROM t_sys_version WHERE TYPE='A' ORDER BY releaseDate DESC");
	}
}
