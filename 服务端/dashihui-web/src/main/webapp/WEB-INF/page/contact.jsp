<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="mtag" uri="mytag"%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>大实惠云商服务平台 社区o2o平台</title>
		<meta name="Keywords" content="河南大实惠,大实惠云商,社区O2O,O2O创业，智慧社区,社区便利店,便利店加盟,连锁便利店,河南便利店" />
		<meta name="Description" content="河南大实惠电子商务有限公司是一家突破型电商平台，通过社区O2O模式，为小区业主提供更便捷、更安全、更舒适的生活服务体验，大实惠云商用户通过大实惠云商APP其他社区业主沟通和分享、发起交易、组织社区活动等，全面成为社区居民的智慧生活管家！" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script type="text/javascript">window.BASE_PATH = '${BASE_PATH}';</script>
		<link rel="alternate icon" type="images/png" href="${BASE_PATH }/static/img/bsh_logo1.png">
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/css.css">
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/foot.css">
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/banner.css">
		<script type="text/javascript" src="${BASE_PATH }/static/js/common.js"></script>
		<script type="text/javascript" src="${BASE_PATH }/static/js/jquery.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/plugins/layer/layer.js"></script>
		<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.5&ak=SEzMz0laDrUzqrnvIzwGr9lx"></script>
		<style>
			.curr a{color:#fff !important;}
			.con_img{margin:2% 0 0 15%;width:85%;float:left;color:#555;}
			.con_img figure{width:25%;float: left;}
			.con{width:100%;}
			.con_map{text-align:left;margin: 2% 0 1.5% 20%;float:left;color:#555;}
			.con_map figure p{padding-bottom: 10px; text-indent: 2em;}
		</style>
</head>
<body>
	<center>
		<header class="header" style="height: 83px;">
			<div class="header_left">
				<a name="coop_new"><img src="${BASE_PATH }/static/img/bsh_logo.png" /></a>
			</div>
			<div class="header_right">
				<ul>
					<li><a href="${BASE_PATH }/" class="button wobble-horizontal">网站首页</a></li>
					<li><a href="${BASE_PATH }/cooperate" class="button wobble-horizontal">合作加盟</a></li>
					<li><a href="${BASE_PATH }/about" class="button wobble-horizontal">关于我们</a></li>
					<li><a href="${BASE_PATH }/notice" class="button wobble-horizontal">新闻动态</a></li>
					<li class="nav-active"><a class="button wobble-horizontal">联系我们</a></li>
				</ul>
			</div>
		</header>
		<div class="banner">		
		    <img class="moving-bg" src="${BASE_PATH }/static/img/banner_d.jpg" alt="deepengine">
		    <div class="banner-mask"></div>
		    <div class="banner-box">
		        <h2></h2>
		        <p class="info"></p>		        
		        <a href="#" class="btn btn-curve">		        
		        
		        </a>
			</div>
    	</div>
		<section class="con">
			<div class="con_img">
				<figure>
					<img src="${BASE_PATH }/static/img/409.jpg" />
					<h1>小区合作</h1>
					<%-- <p>
						联系人：都敏俊<br />邮箱：dashihui2015@163.com
					</p> --%>
				</figure>
				<figure>
					<img src="${BASE_PATH }/static/img/410.jpg" />
					<h1>媒体合作</h1>
					<%-- <p>
						联系人：流川枫<br />邮箱：dashihui2015@163.com
					</p> --%>
				</figure>
				<figure>
					<img src="${BASE_PATH }/static/img/411.jpg" />
					<h1>客户帮助</h1>
					<%-- <p>
						联系人：海贼王<br />邮箱：dashihui2015@163.com
					</p> --%>
				</figure>
			</div>
			<div class="con_map">
				<figure>
					<h2><img src="${BASE_PATH }/static/img/con_map_icon.png"/>河南大实惠电子商务有限公司&nbsp;&nbsp;总部</h2>
					<p>河南省郑州市郑东新区绿地之窗尚峰座5层</p>
					<div id="map" style="width:1053px;height:466px;"></div>
				</figure>
			</div>
		</section>
		<!--在线留言-->
		<footer style="height: 450px;">
			<div style="width:55%; float: left;margin: 1% 0 0 2%;">
			<h1 style="">在线留言</h1>
			<h3>请您填写以下相关信息</h3>
			</div>
			<div style="width:40%;float: left;margin-top: 3%;">
				<img src="${BASE_PATH }/static/img/images/c_34.png" style="width:60% ;" />
			</div>
			<div class="wbox  business margin100 clrfix">
				<div class="left">
					<form action="">
						<dl>
							<dt>
								<input type="text" id="name" name="name" maxlength="8" placeholder="请输入您的姓名" class="text"/>
							</dt>
							<dd>
								<input type="text" id="msisdn" name="msisdn" maxlength="11" placeholder="请输入您的手机号" class="text"/>
							</dd>
							<dt>
								<input type="text" id="mail" name="mail" maxlength="30" placeholder="请输入您的邮箱" class="text"/>
							</dt>
							<dd>
								<input type="text" id="qq" maxlength="10" name="qq" placeholder="请输入您的QQ" class="text" maxlength="10"/>
							</dd>
							<dd class="height178">
								<textarea id="context" name="context" maxlength="200" placeholder="请输入您的留言" class="text long height178"></textarea>
							</dd>
							<dd>
								<input type="button" id="save" class="submit" value="提交" onclick="javascript:doJoin();">
							</dd>
						</dl>
					</form>
				</div>
				<div class="right">
					<font>河南大实惠电子商务有限公司<br />联系电话：400-0079661/0371-86563519<br />电子邮箱：dashihui2015@163.com<br />公司地址：郑东新区绿地之窗尚峰座5层<br />
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/wscm.png" />
							</dt>
							<dd>微商城订阅号</dd>
						</dl>
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/a.png" />
							</dt>
							<dd>客户端二维码</dd>
						</dl>
					</font>
				</div>
			</div>
			<p class="big_foot_span" style="bottom:0;">
	  		    <span>Copyright©2015-2015.All Rights Reserved</span>
				河南大实惠电子商务有限公司版权所有&nbsp;&nbsp;豫ICP备15031706号 
			</p>
		</footer>
	</center>
	<div style="background: url(${BASE_PATH }/static/img/images/c_33.png) repeat-x;width: 100%; float: left;">
		<div style="width:188px;margin:0 auto;">
			<a href="#coop_new" style="display:block;">
				<img src="${BASE_PATH }/static/img/images/c_32.png" style="margin:0;float: left;" />
			</a>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
		    var vx = 34.767068;
		    var vy = 113.778679;
		    //百度地图API功能
// 			var sContent ="河南省郑州市郑东新区绿地之窗尚峰座5层";
			var map = new BMap.Map("map");
			var point = new BMap.Point(vy, vx);
			map.centerAndZoom(point, 15);
// 			var infoWindow = new BMap.InfoWindow(sContent);  //创建信息窗口对象
// 			map.openInfoWindow(infoWindow,point); //开启信息窗口
			map.enableScrollWheelZoom(true); //可缩放
			//设置marker
			var myIcon = new BMap.Icon("${BASE_PATH}/static/img/star.gif",new BMap.Size(23,22));
			var marker = new BMap.Marker(point,{icon:myIcon});  //创建标注
			map.addOverlay(marker);
			//点击事件
// 			marker.addEventListener("click", function(){     
// 				map.openInfoWindow(infoWindow, point);
// 			});
		});
    </script>
</body>
</html>