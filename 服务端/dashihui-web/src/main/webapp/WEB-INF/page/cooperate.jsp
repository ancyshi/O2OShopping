<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="mtag" uri="mytag"%>
<!DOCTYPE html>
<html>
<head>
	<title>大实惠云商服务平台 社区o2o平台</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Keywords" content="河南大实惠,大实惠云商,社区O2O,O2O创业，智慧社区,社区便利店,便利店加盟,连锁便利店,河南便利店" />
	<meta name="Description" content="河南大实惠电子商务有限公司是一家突破型电商平台，通过社区O2O模式，为小区业主提供更便捷、更安全、更舒适的生活服务体验，大实惠云商用户通过大实惠云商APP其他社区业主沟通和分享、发起交易、组织社区活动等，全面成为社区居民的智慧生活管家！" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="text/javascript">window.BASE_PATH = '${BASE_PATH}';</script>
	<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/css.css">
	<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/foot.css">
	<link rel="alternate icon" type="images/png" href="${BASE_PATH }/static/img/bsh_logo1.png">
	<script type="text/javascript" src="${BASE_PATH }/static/js/common.js"></script>
	<script type="text/javascript" src="${BASE_PATH }/static/js/jquery.min.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/plugins/layer/layer.js"></script>
	<style>
		.ab_left li a:link{font-size: 16px;color: #666 !important;}
	</style>
</head>
<body>
	<center class="ab_ct">
		<header class="header">
			<div class="header_left">
				<a><img src="${BASE_PATH }/static/img/bsh_logo.png" /></a>
			</div>
			<div class="header_right">
				<ul>
					<li><a href="${BASE_PATH }/" class="button wobble-horizontal">网站首页</a></li>
					<li class="nav-active"><a class="button wobble-horizontal">合作加盟</a></li>
					<li><a href="${BASE_PATH }/about" class="button wobble-horizontal">关于我们</a></li>
					<li><a href="${BASE_PATH }/notice" class="button wobble-horizontal">新闻动态</a></li>
					<li><a href="${BASE_PATH }/contact" class="button wobble-horizontal">联系我们</a></li>
				</ul>
			</div>
		</header>
		<section class="coo_img">
				<a name="coop_new"><img src="${BASE_PATH }/static/img/images/c_01.png" /></a>
				<div style="width: 1350px;">
				<img src="${BASE_PATH }/static/img/images/c_02.png" />
				<img src="${BASE_PATH }/static/img/images/c_03.png" />
				<img src="${BASE_PATH }/static/img/images/c_04.png" />
				<img src="${BASE_PATH }/static/img/images/c_07.png" />
				<img src="${BASE_PATH }/static/img/images/c_08.png" />
				<img src="${BASE_PATH }/static/img/images/c_09.png" />
				<img src="${BASE_PATH }/static/img/images/c_10.png" />
				<img src="${BASE_PATH }/static/img/images/c_11.png" />
				<img src="${BASE_PATH }/static/img/images/c_12.png" />
				<img src="${BASE_PATH }/static/img/images/c_13.png" />
				<img src="${BASE_PATH }/static/img/images/c_14.png" />
				<img src="${BASE_PATH }/static/img/images/c_15.png" />
				<img src="${BASE_PATH }/static/img/images/c_16.png" />
				<img src="${BASE_PATH }/static/img/images/c_17.png" />
				<img src="${BASE_PATH }/static/img/images/c_18.png" />
				<img src="${BASE_PATH }/static/img/images/c_19.png" />
				<img src="${BASE_PATH }/static/img/images/c_20.png" />
				<img src="${BASE_PATH }/static/img/images/c_21.png" />
				<img src="${BASE_PATH }/static/img/images/c_22.png" />
				<img src="${BASE_PATH }/static/img/images/c_23.png" />
				<img src="${BASE_PATH }/static/img/images/c_24.png" />
				<img src="${BASE_PATH }/static/img/images/c_25.png" />
				<img src="${BASE_PATH }/static/img/images/c_26.png" />
				<img src="${BASE_PATH }/static/img/images/c_27.png" />
				<img src="${BASE_PATH }/static/img/images/c_28.png" />
				<img src="${BASE_PATH }/static/img/images/c_29.jpg" />
				<img src="${BASE_PATH }/static/img/images/c_30.png" />
				<img src="${BASE_PATH }/static/img/images/c_31.png" />
				</div>

				<!--<img src="images/images/01_25.png" />-->
			</section>
		<!--在线留言-->
		<footer style="height: 450px;">
			<div style="width:55%; float: left;margin: 1% 0 0 2%;">
			<h1 style="">合作加盟</h1>
			<h3>请您填写以下相关信息</h3>
			</div>
			<div style="width:40%;float: left;margin-top: 3%;">
				<img src="${BASE_PATH }/static/img/images/c_34.png" style="width:60% ;" />
			</div>
			<div class="wbox  business margin100 clrfix">
				<div class="left">
					<form action="http://www.91dashihui.com/cooperate;jsessionid=546C1F14B3C702877DA1F8B841AEFF93#">
						<dl>
							<dt>
								<input type="text" id="name" name="name" maxlength="8" placeholder="请输入您的姓名" class="text"/>
							</dt>
							<dd>
								<input type="text" id="msisdn" name="msisdn" maxlength="11" placeholder="请输入您的手机号" class="text"/>
							</dd>
							<dt>
								<input type="text" id="mail" name="mail" maxlength="30" placeholder="请输入您的邮箱" class="text"/>
							</dt>
							<dd>
								<input type="text" id="qq" maxlength="10" name="qq" placeholder="请输入您的QQ" class="text" maxlength="10"/>
							</dd>
							<dd class="height178">
								<textarea id="context" name="context" maxlength="200" placeholder="请输入您的留言" class="text long height178"></textarea>
							</dd>
							<dd>
								<input type="button" id="save" class="submit" value="提交" onclick="javascript:doJoin();">
							</dd>
						</dl>
					</form>
				</div>
				<div class="right">
					<font>河南大实惠电子商务有限公司<br />联系电话：400-0079661/0371-86563519<br />电子邮箱：dashihui2015@163.com<br />公司地址：郑东新区绿地之窗尚峰座5层<br />
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/wscm.png" />
							</dt>
							<dd>微商城订阅号</dd>
						</dl>
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/a.png" />
							</dt>
							<dd>客户端二维码</dd>
						</dl>
					</font>
				</div>
			</div>
			<p class="big_foot_span" style="bottom:0;">
	  		    <span>Copyright©2015-2015.All Rights Reserved</span>
				河南大实惠电子商务有限公司版权所有&nbsp;&nbsp;豫ICP备15031706号 
			</p>
		</footer>
	</center>
	<div style="background: url(${BASE_PATH }/static/img/images/c_33.png) repeat-x;width: 100%; float: left;">
		<div style="width:188px;margin:0 auto;">
			<a href="#coop_new" style="display:block;">
				<img src="${BASE_PATH }/static/img/images/c_32.png" style="margin:0;float: left;" />
			</a>
		</div>
	</div>
	<script type="text/javascript"> var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://"); document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F60f951c38f2066e065bb57e48654c4ac' type='text/javascript'%3E%3C/script%3E")) </script>
</body>
</html>