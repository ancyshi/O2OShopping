<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="mtag" uri="mytag"%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>大实惠云商服务平台 社区o2o平台</title>
		<meta name="Keywords" content="河南大实惠,大实惠云商,社区O2O,O2O创业，智慧社区,社区便利店,便利店加盟,连锁便利店,河南便利店" />
		<meta name="Description" content="河南大实惠电子商务有限公司是一家突破型电商平台，通过社区O2O模式，为小区业主提供更便捷、更安全、更舒适的生活服务体验，大实惠云商用户通过大实惠云商APP其他社区业主沟通和分享、发起交易、组织社区活动等，全面成为社区居民的智慧生活管家！" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script type="text/javascript">window.BASE_PATH = '${BASE_PATH}';</script>
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/amazeui.min.css">
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/css.css">
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/foot.css">
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/others.css">
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/banner.css">
		<link rel="alternate icon" type="images/png" href="${BASE_PATH }/static/img/bsh_logo1.png">
		<script type="text/javascript" src="${BASE_PATH }/static/js/common.js"></script>
		<script type="text/javascript" src="${BASE_PATH }/static/js/TweenMax.js"></script>
		<script type="text/javascript" src="${BASE_PATH }/static/js/jquery.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/plugins/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/plugins/pagination/bootstrap-pagination.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/plugins/pagination/bootstrap-pagination-ext.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/plugins/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/plugins/template/template-ext.js"></script>
		<style>				
			.nqli{border: 1px solid #ddd;}
			.pagination {width:45%;}
		</style>
</head>
<body>
	<center>
		<header class="header" style="height: 83px;">
			<div class="header_left">
				<a name="coop_new"><img src="${BASE_PATH }/static/img/bsh_logo.png" /></a>
			</div>
			<div class="header_right">
				<ul>
					<li><a href="${BASE_PATH }/" class="button wobble-horizontal">网站首页</a></li>
					<li><a href="${BASE_PATH }/cooperate" class="button wobble-horizontal">合作加盟</a></li>
					<li><a href="${BASE_PATH }/about" class="button wobble-horizontal">关于我们</a></li>
					<li class="nav-active"><a class="button wobble-horizontal">新闻动态</a></li>
					<li><a href="${BASE_PATH }/contact" class="button wobble-horizontal">联系我们</a></li>
				</ul>
			</div>
		</header>
		<div class="banner">		
		    <img class="moving-bg" src="${BASE_PATH }/static/img/banner_d.jpg" alt="deepengine">
		    <div class="banner-mask"></div>
		    <div class="banner-box">
		        <h2></h2>
		        <p class="info"></p>		        
		        <a href="#" class="btn btn-curve">		        
		        
		        </a>
			</div>
    	</div> 
		<section class="news_list">
			<div class="nqBody">
				<div class="nqType">
					<ul class="nqtWrap">
						<li id="news_type_1"><a onclick="javascript:syncData('1');">公司动态</a></li>
						<li id="news_type_2"><a onclick="javascript:syncData('2');">行业前瞻</a></li>
					</ul>
				</div>
				<ul class="nqlWrap" id="news"></ul>
				<div id="paginationContainer"></div>
			</div>
		</section>
		<!--在线留言-->
		<footer style="height: 450px;">
			<div style="width:55%; float: left;margin: 1% 0 0 2%;">
			<h1 style="">在线留言</h1>
			<h3>请您填写以下相关信息</h3>
			</div>
			<div style="width:40%;float: left;margin-top: 3%;">
				<img src="${BASE_PATH }/static/img/images/c_34.png" style="width:60% ;" />
			</div>
			<div class="wbox  business margin100 clrfix">
				<div class="left">
					<form
						action="http://www.91dashihui.com/cooperate;jsessionid=546C1F14B3C702877DA1F8B841AEFF93#">
						<dl>
							<dt>
								<input type="text" id="name" name="name" maxlength="8" placeholder="请输入您的姓名" class="text"/>
							</dt>
							<dd>
								<input type="text" id="msisdn" name="msisdn" maxlength="11" placeholder="请输入您的手机号" class="text"/>
							</dd>
							<dt>
								<input type="text" id="mail" name="mail" maxlength="30" placeholder="请输入您的邮箱" class="text"/>
							</dt>
							<dd>
								<input type="text" id="qq" maxlength="10" name="qq" placeholder="请输入您的QQ" class="text" maxlength="10"/>
							</dd>
							<dd class="height178">
								<textarea id="context" name="context" maxlength="200" placeholder="请输入您的留言" class="text long height178"></textarea>
							</dd>
							<dd>
								<input type="button" id="save" class="submit" value="提交" onclick="javascript:doJoin();">
							</dd>
						</dl>
					</form>
				</div>
				<div class="right">
					<font>河南大实惠电子商务有限公司<br />联系电话：400-0079661/0371-86563519<br />电子邮箱：dashihui2015@163.com<br />公司地址：郑东新区绿地之窗尚峰座5层<br />
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/wscm.png" />
							</dt>
							<dd>微商城订阅号</dd>
						</dl>
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/a.png" />
							</dt>
							<dd>客户端二维码</dd>
						</dl>
					</font>
				</div>
			</div>
			<p class="big_foot_span" style="bottom:0;">
	  		    <span>Copyright©2015-2015.All Rights Reserved</span>
				河南大实惠电子商务有限公司版权所有&nbsp;&nbsp;豫ICP备15031706号 
			</p>
		</footer>
	</center>
	<div style="background: url(${BASE_PATH }/static/img/images/c_33.png) repeat-x;width: 100%; float: left;">
		<div style="width:188px;margin:0 auto;">
			<a href="#coop_new" style="display:block;">
				<img src="${BASE_PATH }/static/img/images/c_32.png" style="margin:0;float: left;" />
			</a>
		</div>
	</div>
	<script type="text/html" id="dataTpl">
		{{each list as news}}
			<li class="nqli">
				<div class="nqlDate" style="background-color: rgb(120, 120, 120);">
					<span class="nqldDay">{{news.createDate | dateFormat:'dd'}}</span>
					<span class="nqldMy">[{{news.createDate | dateFormat:'yyyy-MM'}}]</span>
				</div>
				<a class="nqlTitle" href="${BASE_PATH}/notice/detail/{{news.id}}" target="_blank">{{news.title}}</a>
				<div class="nqlSum">{{news.context}}</div>
				<div class="nqlDetDate">[{{news.createDate | dateFormat:'yyyy-MM-dd'}}]</div>
			</li>
		{{/each}}
	</script>
	<script type="text/javascript">
		$(function(){
			syncData('1');
		});
		var pageNum = 0,orderNum;
		//切换标签
		function syncData(type){
			if(type == null || type == undefined){
				type = '1';
			}
			if(type == '1'){
				$("#news_type_1").addClass("curr");
				$("#news_type_2").removeClass("curr");
			} else {
				$("#news_type_2").addClass("curr");
				$("#news_type_1").removeClass("curr");
			}
			$("#paginationContainer").bootstrapPaginator({
				selector:"#paginationContainer",
				href:"${BASE_PATH}/notice/page",
				params:"type="+type,
				callback:function(result){
					$("#news").empty().append(template("dataTpl", result.object));
				}
			});
			
			//鼠标放在标题上改变颜色
			$("#news").on("mouseenter", ".nqli", function(){
				var self = $(this);
				TweenMax.to( self.find(".nqlDate"), 0.5, {
					"backgroundColor" : "#EC173A"
				});
			}).on("mouseleave", ".nqli", function(){
				var self = $(this);
				TweenMax.to( self.find(".nqlDate"), 0.5, {
					"backgroundColor" : "#787878"
				});
			});
		}
	</script>
</body>
</html>