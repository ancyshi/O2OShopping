<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		rules:{
			username: {
				isMobile:true
			},
			tel: {
				isTel:true
			}
		},
		messages:{
			username: {required: "请输入用户名",isMobile: "请输入正确的手机号码"},
			nickName: {required: "请输入昵称"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 2:
						Kit.alert("用户名格式不正确");return;
					case 3:
						Kit.alert("用户名已经存在");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						editDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="editForm" action="${BASE_PATH}/seller/doEdit" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">用户名</label>
	    <div class="col-lg-9">
        	<input type="text" name="username" value="${object.username}" class="form-control" placeholder="请输入用户名（11位手机号码）" required maxlength="11">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">姓名</label>
	    <div class="col-lg-9">
        	<input type="text" name="name" value="${object.name}" class="form-control" placeholder="请输入姓名" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">性别</label>
	    <div class="col-lg-9">
	    	<select name="sex" class="selectpicker form-control">
				<option value="1" <c:if test="${object.sex==1}">selected</c:if>>男</option>
				<option value="2" <c:if test="${object.sex==2}">selected</c:if>>女</option>
	        </select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">联系电话</label>
	    <div class="col-lg-9">
        	<input type="text" name="tel" value="${object.tel}" class="form-control" placeholder="请输入联系电话" maxlength="13">
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="sellerid" value="${object.id}"/>
</form>