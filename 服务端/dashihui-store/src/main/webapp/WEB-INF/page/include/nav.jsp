<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 导航 -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<!-- 左侧Logo及功能 -->
	<div class="navbar-header">
		<!-- 窗口变窄时出现的控制菜单栏显示隐藏的按钮 -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">显示/隐藏菜单栏</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <!-- 左侧平台Logo -->
        <span class="navbar-brand-img"><img src="${BASE_PATH}/static/images/logo_big.png"/></span>
        <span class="navbar-brand">管理平台</span>
    </div>

	<!-- 右侧功能 -->
    <ul class="nav navbar-top-links navbar-right">
    	<!-- 待办事项或通知 -->
    	<!-- 
        <li class="dropdown">
            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" title="待办事项">
                <i class="fa fa-bell fa-fw"></i>
                <span class="label label-success">16</span>
            </a>
            <ul class="dropdown-menu dropdown-alerts">
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-comment fa-fw"></i> 新的评论
                            <span class="pull-right text-muted small">4分钟前</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-twitter fa-fw"></i> 3 个关注
                            <span class="pull-right text-muted small">12分钟前</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-envelope fa-fw"></i> 消息已发送
                            <span class="pull-right text-muted small">4分钟前</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-tasks fa-fw"></i> 新的任务
                            <span class="pull-right text-muted small">4分钟前</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-upload fa-fw"></i> 服务已重启
                            <span class="pull-right text-muted small">4分钟前</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a class="text-center" href="#">
                        <strong>查看所有通知</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
        </li>
         -->
        <li><a><i class="fa fa-clock-o fa-fw"></i> <span id="clock"></span></a></li>
        <li><a href="javascript:toEditMineInfo();" title="点击修改信息"><i class="fa fa-user fa-fw"></i> <span id="currentUserTitle">${CurrentUser.title}</span></a></li>
		<li><a href="javascript:toEditMinePwd();"><i class="fa fa-key fa-fw"></i> 修改密码</a></li>
		<li><a href="<c:url value="/logout"/>"><i class="fa fa-sign-out fa-fw"></i> 退出</a></li>
    </ul>
</nav>
<script type="text/javascript">
<!--
var editMinePwdDialog;
function toEditMinePwd(id){
	editMinePwdDialog = Kit.dialog("修改密码","${BASE_PATH}/toEditMinePwd").open();
}
var editMineInfoDialog;
function toEditMineInfo(id){
	editMineInfoDialog = Kit.dialog("修改信息","${BASE_PATH}/toEditMineInfo").open();
}
function onMineInfoEdited(info){
	$("#currentUserTitle").text(info.title);
}
//时钟
setInterval(function() {
    $('#clock').html("当前时间："+Kit.curentTime());
}, 1000);
//-->
</script>
<jsp:include page="../include/nav-menu.jsp"></jsp:include>