<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="menu-wrapper">
	<div class="navbar-default sidebar" role="navigation">
	    <div class="sidebar-nav navbar-collapse">
	        <ul class="nav" id="side-menu">
	            <li class="active">
	                <a href="#">商品订单<span class="fa arrow"></span></a>
	                <ul class="nav nav-second-level">
	                    <li><a href="${BASE_PATH}/order/index">全部订单</a></li>
	                    <li><a href="${BASE_PATH}/order/eval/index">订单评价</a></li>
	                    <li><a href="${BASE_PATH}/order/refund/index">退款单</a></li>
	                    <li><a href="${BASE_PATH}/report/orderCount">成交情况</a></li>
	                    <li><a href="${BASE_PATH}/report/goodsRank">商品排名</a></li>
	                </ul>
	            </li>
	            <li class="active">
	                <a href="#">服务订单<span class="fa arrow"></span></a>
	                <ul class="nav nav-second-level">
	                    <li><a href="${BASE_PATH}/ser/order/index">全部订单(家政)</a></li>
	                    <li><a href="${BASE_PATH}/service/order/index">全部订单(其他)</a></li>
	                    <li><a href="${BASE_PATH}/report/serviceOrderCount">成交情况</a></li>
	                    <li><a href="${BASE_PATH}/report/serviceShopRank">商家排名</a></li>
	                    <li><a href="${BASE_PATH}/report/serviceItemRank">服务项排名</a></li>
	                </ul>
	            </li>
	            <li class="active">
	                <a href="#">业务管理<span class="fa arrow"></span></a>
	                <ul class="nav nav-second-level">
	                	<li><a href="${BASE_PATH}/tip">店铺通知</a></li>
	                	<li><a href="${BASE_PATH}/goods/tag">商品标签</a></li>
	                    <li><a href="${BASE_PATH}/goods/index">商品管理</a></li>
	                    <li><a href="${BASE_PATH}/seller/index">配送员管理</a></li>
	                    <li><a href="${BASE_PATH}/ad/index">轮播展示</a></li>
	                    <li><a href="${BASE_PATH}/service/category">服务分类</a></li>
	                    <li><a href="${BASE_PATH}/ser/shop">服务商家（家政）</a></li>
	                    <li><a href="${BASE_PATH}/service/shop">服务商家（其他）</a></li>
	                </ul>
	            </li>
	            <li class="active">
	                <a href="#">系统设置<span class="fa arrow"></span></a>
	                <ul class="nav nav-second-level">
	                    <li><a href="${BASE_PATH}/community/index">楼幢信息</a></li>
	                    <li><a href="${BASE_PATH}/keyword/index">热搜关键字</a></li>
	                    <li><a href="${BASE_PATH}/log/keyword/index">搜索历史</a></li>
	                </ul>
	            </li>
	        </ul>
		</div>
	</div>
</div>