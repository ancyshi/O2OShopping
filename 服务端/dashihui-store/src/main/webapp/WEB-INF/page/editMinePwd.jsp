<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editMinePwdForm").validate({
		rules:{
			passwordNewConfirm: {
				equalTo: "#passwordNew"
			}
		},
		messages:{
			passwordOld: {required: "请输入原密码"},
			passwordNew: {required: "请输入密码"},
			passwordNewConfirm: {required: "请再次输入密码",equalTo: "两次密码输入不相同"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 4:
						Kit.alert("原密码输入不正确");return;
					case 0:
						editMinePwdDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="editMinePwdForm" action="${BASE_PATH}/doEditMinePwd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">原密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordOld" name="passwordOld" value="" class="form-control" placeholder="请输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordNew" name="passwordNew" value="" class="form-control" placeholder="请输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">确认密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordNewConfirm" name="passwordNewConfirm" value="" class="form-control" placeholder="请再次输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editMinePwdDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>