<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">欢迎使用管理平台</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">商品订单信息</div>
					<div class="panel-body hint">
						<div class="col-xs-2 col-md-2 col-lg-2">商品共有 <span class="label label-info" id="goodsCount"></span> 件</div>
						<div class="col-xs-2 col-md-2 col-lg-2">待付款订单 <span class="label label-info" id="chart1State1"></span> 笔</div>
				    	<div class="col-xs-2 col-md-2 col-lg-2">待发货订单 <span class="label label-info" id="chart1State2"></span> 笔</div>
						<div class="col-xs-2 col-md-2 col-lg-2">待签收订单 <span class="label label-info" id="chart1State3"></span> 笔</div>
						<div class="col-xs-2 col-md-2 col-lg-2">待取货订单 <span class="label label-info" id="chart1State4"></span> 笔</div>
						<div class="col-xs-2 col-md-2 col-lg-2">被催订单 <span class="label label-info" id="chart1State5"></span> 笔</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">十天内商品订单量统计</div>
					<div class="panel-body" id="chart1" style="min-height:300px"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">服务订单信息</div>
					<div class="panel-body hint">
						<div class="col-xs-2 col-md-2 col-lg-2">待付款订单 <span class="label label-info" id="chart2State1"></span> 笔</div>
				    	<div class="col-xs-2 col-md-2 col-lg-2">待派订单（其他） <span class="label label-info" id="chart2State2_1"></span> 笔</div>
				    	<div class="col-xs-2 col-md-2 col-lg-2">待接订单（家政） <span class="label label-info" id="chart2State2_2"></span> 笔</div>
				    	<div class="col-xs-2 col-md-2 col-lg-2">待派订单（家政） <span class="label label-info" id="chart2State2_3"></span> 笔</div>
						<div class="col-xs-2 col-md-2 col-lg-2">待确认订单 <span class="label label-info" id="chart2State3"></span> 笔</div>
						<div class="col-xs-2 col-md-2 col-lg-2">被催订单 <span class="label label-info" id="chart2State4"></span> 笔</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">十天内服务订单量统计</div>
					<div class="panel-body" id="chart2" style="min-height:300px"></div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="include/footer.jsp"></jsp:include>
</div>
<jsp:include page="include/javascripts.jsp"></jsp:include>
<script src="${BASE_PATH}/static/plugins/echarts/echarts.js"></script>
<script type="text/javascript">
$(function(){
	$.getJSON("<c:url value="/report/orderStateCount"/>",function(result){
		if(result.flag==0){
			$("#goodsCount").text(result.object.goodsCount);
			$("#chart1State1").text(result.object.state1);
			$("#chart1State2").text(result.object.state2);
			$("#chart1State3").text(result.object.state3);
			$("#chart1State4").text(result.object.state4);
			$("#chart1State5").text(result.object.state5);
		}
	});
	$.getJSON("<c:url value="/report/orderStateChart"/>",function(result){
		if(result.flag==0){
			//创建图表
			var $echart = $("#chart1");
			$echart.attr("width",($echart.parent().width() - 15));
			require.config({paths: {echarts: "${BASE_PATH}/static/plugins/echarts"}});
			require(['echarts','echarts/theme/macarons','echarts/chart/bar'],
				function (ec,theme) {
			        var myChart = ec.init($echart[0],theme);
			        myChart.showLoading({text : '渲染中...',effect : 'spin',textStyle : {fontSize : 20}});
			       	var options = {title:{show:false},toolbox:{show:false}};
			   		options = $.extend(options,{
			   			tooltip: {trigger: "axis"},
			   			legend: {show:true,data:["总订单量","成功订单量"]},
		       			grid: {x:30,y:30,x2:20,y2:30},
			   			xAxis: [{type: "category",data: result.object.labels}],
			   	    	yAxis: [{type: "value",splitArea: {show: true}}],
			   			series: [{name: "总订单量",type: "bar",data: result.object.datas1},{name: "成功订单量",type: "bar",data: result.object.datas2}]
			   		});
			        myChart.setOption(options);
			        myChart.hideLoading();
			        //set chart resize on window resize
			        $(window).resize(myChart.resize);
				}
			);
		}
	});
	$.getJSON("<c:url value="/report/serviceOrderStateCount"/>",function(result){
		if(result.flag==0){
			$("#chart2State1").text(result.object.state1);
			$("#chart2State2_1").text(result.object.state2_1);
			$("#chart2State2_2").text(result.object.state2_2);
			$("#chart2State2_3").text(result.object.state2_3);
			$("#chart2State3").text(result.object.state3);
			$("#chart2State4").text(result.object.state4);
		}
	});
	$.getJSON("<c:url value="/report/serviceOrderStateChart"/>",function(result){
		if(result.flag==0){
			//创建图表
			var $echart = $("#chart2");
			$echart.attr("width",($echart.parent().width() - 15));
			require.config({paths: {echarts: "${BASE_PATH}/static/plugins/echarts"}});
			require(['echarts','echarts/theme/macarons','echarts/chart/bar'],
				function (ec,theme) {
			        var myChart = ec.init($echart[0],theme);
			        myChart.showLoading({text : '渲染中...',effect : 'spin',textStyle : {fontSize : 20}});
			       	var options = {title:{show:false},toolbox:{show:false}};
			   		options = $.extend(options,{
			   			tooltip: {trigger: "axis"},
			   			legend: {show:true,data:["总订单量","成功订单量"]},
		       			grid: {x:30,y:30,x2:20,y2:30},
			   			xAxis: [{type: "category",data: result.object.labels}],
			   	    	yAxis: [{type: "value",splitArea: {show: true}}],
			   			series: [{name: "总订单量",type: "bar",data: result.object.datas1},{name: "成功订单量",type: "bar",data: result.object.datas2}]
			   		});
			        myChart.setOption(options);
			        myChart.hideLoading();
			        //set chart resize on window resize
			        $(window).resize(myChart.resize);
				}
			);
		}
	});
});
</script>
</body>
</html>