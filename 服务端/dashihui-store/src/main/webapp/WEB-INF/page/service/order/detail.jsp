<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<div style="min-height:300px;">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">订单信息</a></li>
		<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">服务项</a></li>
		<li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">备注</a></li>
		<li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">收货人</a></li>
		<li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">订单日志</a></li>
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active container-fluid" id="tab1" style="padding-top:15px;">
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">订单号：</label>
				    <div class="text-content">${obj.orderNum}</div>
				</div>
			    <div class="col-lg-6">
				    <label class="text-label">下单时间：</label>
				    <div class="text-content"><fmt:formatDate value="${obj.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">订单状态：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.orderState==1}">正常</c:when>
					    	<c:when test="${obj.orderState==2}">已完成</c:when>
					    	<c:when test="${obj.orderState==3}">已取消</c:when>
					    	<c:when test="${obj.orderState==4}">已删除</c:when>
					    	<c:when test="${obj.orderState==5}">已过期</c:when>
					    	<c:when test="${obj.orderState==6}">已关闭</c:when>
				    	</c:choose>
				    </div>
			    </div>
				<div class="col-lg-6">
				    <label class="text-label">共计金额：</label>
				    <div class="text-content">${obj.amount} (元)</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
					<label class="text-label">支付方式：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.payType==1}">在线支付</c:when>
					    	<c:when test="${obj.payType==2}">服务后付款</c:when>
				    	</c:choose>
				    </div>
				</div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<c:choose>
			<c:when test="${obj.orderState==3}">
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">取消理由：</label>
				    <div class="text-content">${obj.reason}</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			</c:when>
			<c:when test="${obj.orderState==1||obj.orderState==2}">
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">支付状态：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.payState==1}">待支付</c:when>
					    	<c:when test="${obj.payState==2}">已支付</c:when>
				    	</c:choose>
				    </div>
			    </div>
				<div class="col-lg-6">
				    <label class="text-label">支付时间：</label>
				    <div class="text-content"><fmt:formatDate value="${obj.payDate}" pattern="yyyy-MM-dd HH:mm:ss"/></div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">签收状态：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.orderState==1}">待签收</c:when>
					    	<c:when test="${obj.orderState==2}">已签收</c:when>
				    	</c:choose>
				    </div>
				</div>
				<div class="col-lg-6">
				    <label class="text-label">完成时间：</label>
				    <div class="text-content"><fmt:formatDate value="${obj.signDate}" pattern="yyyy-MM-dd HH:mm:ss"/></div>
				</div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			</c:when>
			</c:choose>
		</div>
		<div role="tabpanel" class="tab-pane" id="tab2" style="padding-top:10px;">
			<div class="table-responsive">
	    		<table class="table ">
					 <thead>
			         	<tr>
			         		<th width="15%"></th>
			         		<th width="35%">服务商家</th>
							<th width="35%">服务项</th>
							<th width="15%">金额(元)</th>
			         	</tr>
			         </thead>
			         <tbody >
				       <c:if test="${serviceList!=null}">
							<c:forEach items="${serviceList}" var="service">
								<tr>
									<td><a href="javascript:void(0)" onclick="Kit.photo('${FTP_PATH}${service.shopThumb}')" class="center-block pull-left thumbnail thumbnail-none-margin width50"><img src="${FTP_PATH}${service.shopThumb}"></a></td>
									<td>${service.shopName}</td>
									<td>${service.serviceTitle}</td>
									<td>${service.amount}</td>
				         		</tr>
							</c:forEach>
						</c:if>
			         </tbody>
				</table>
			</div>
		</div>
	    <div role="tabpanel" class="tab-pane" id="tab3" style="padding-top:15px;">
	    	${obj.describe}
	    </div>
	    <div role="tabpanel" class="tab-pane" id="tab4" style="padding-top:15px;">
	    	<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">买家：</label>
				    <div class="text-content">${obj.linkName} ${obj.sex}</div>
				</div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">买家电话：</label>
				    <div class="text-content">${obj.tel}</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">买家地址：</label>
				    <div class="text-content">${obj.address}</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="tab5" style="padding-top:10px;">
			<div class="table-responsive">
	    		<table class="table ">
					 <thead>
			         	<tr>
							<th width="20%">时间</th>
							<th width="15%">操作人</th>
							<th width="15%">动作</th>
							<th width="50%">内容</th>
			         	</tr>
			         </thead>
			         <tbody >
				       <c:if test="${logList!=null}">
							<c:forEach items="${logList}" var="log">
								<tr>
									<td><fmt:formatDate value="${log.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
									<td>${log.user}</td>
									<td>${log.action}</td>
									<td style="white-space:normal;">${log.content}</td>
				         		</tr>
							</c:forEach>
						</c:if>
			         </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row" style="display:none;">
	<div class="col-lg-offset-2 col-lg-9">
		<button class="btn btn-warning col-sm-2 pull-left m-l-10" type="button" <c:if test="${obj.payState!=1||obj.orderState!=1}">disabled</c:if>>支付</button>
		<button class="btn btn-success col-sm-2 pull-left m-l-10" type="button" <c:if test="${obj.payState!=2||obj.deliverState!=1||obj.orderState!=1}">disabled</c:if>>发货</button>
		<button class="btn btn-info col-sm-2 pull-left m-l-10" type="button" <c:if test="${obj.payState!=2||obj.deliverState!=2||obj.orderState!=1}">disabled</c:if>>完成</button>
		<button class="btn btn-default col-sm-2 pull-left m-l-10" type="button" <c:if test="${obj.payState!=1||obj.orderState!=1}">disabled</c:if>>作废</button>
		<button class="btn btn-default col-sm-2 pull-left m-l-10" type="button" onclick="javascript:showOrderDialog.close();" autocomplete="off">关闭</button>
	</div>
</div>