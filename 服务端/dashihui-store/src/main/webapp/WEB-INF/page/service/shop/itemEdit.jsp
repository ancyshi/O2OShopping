<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#editItemForm").validate({
		rules: {
			marketPrice: {isMoney: true},
			sellPrice: {isMoney: true}
		},
		messages:{
			title: {required: "请输入标题",maxlength:"标题不能超过100个字符"},
			marketPrice: {isMoney: "请输入正确的市场价"},
			sellPrice: {isMoney: "请输入正确的销售价"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 2:
						Kit.alert("标题不能为空");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						loadItem();
						editItemDialog.close();
					}
				}
			});
		}
	});
});

</script>
<form id="editItemForm" action="${BASE_PATH}/service/shop/doItemEdit" method="post" class="form-horizontal">
<input type="hidden" name="id" value="${obj.id}"/>
	<div class="form-group">
	    <label class="col-lg-2 control-label">标题</label>
	    <div class="col-lg-9">
        	<input type="text" name="title" value="${obj.title}" class="form-control" placeholder="请输入标题" required maxlength="100">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">市场价</label>
	    <div class="col-lg-9">
        	<input type="text" name="marketPrice" value="${obj.marketPrice}" class="form-control" placeholder="请输入市场价">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">销售价</label>
	    <div class="col-lg-9">
        	<input type="text" name="sellPrice" value="${obj.sellPrice}" class="form-control" placeholder="请输入销售价">
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button>
		</div>
		<div class="col-lg-6">
			<button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editItemDialog.close();" autocomplete="off">取消</button>
		</div>
	</div>
	
</form>