package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.web.dao.Goods;
import cn.com.dashihui.web.dao.GoodsImages;
import cn.com.dashihui.web.dao.GoodsTag;

public class GoodsService {
	
	/**
	 * 查询所有可用的商品标签，并判断指定商品有哪些已经选中
	 * @param goodsid
	 */
	public List<GoodsTag> getAllEnabledTag(int storeid,int goodsid){
		if(goodsid==0){
			return GoodsTag.me().find("SELECT A.id,A.tagName,A.code,0 checked FROM t_bus_goods_tag A WHERE A.enabled=1 AND A.storeid=? ORDER BY A.orderNo",storeid);
		}else{
			return GoodsTag.me().find("SELECT A.id,A.tagName,A.code,(CASE WHEN B.goodsid IS NULL THEN 0 ELSE 1 END) checked FROM t_bus_goods_tag A LEFT JOIN t_bus_goods_tag_rel B ON A.id=B.tagid AND B.goodsid=? WHERE A.enabled=1 AND A.storeid=? ORDER BY A.orderNo",goodsid,storeid);
		}
	}
	
	/**
	 * 分页查找店铺产品
	 */
	public Page<Record> findByPage(int pageNum,int pageSize,int storeid,int s,int c1,int c2,int c3,int c4,int type,String tags,String keyword){
		StringBuffer sBuffer = new StringBuffer("FROM t_bus_goods A WHERE A.storeid=?");
		List<Object> params = new ArrayList<Object>();
		params.add(storeid);
		if(s!=0){
			sBuffer.append(" AND A.state=?");
			params.add(s);
		}
		if(c4!=0){
			sBuffer.append(" AND A.categoryonid=? AND A.categorytwid=? AND A.categorythid=? AND A.categoryfoid=?");
			params.add(c1);params.add(c2);params.add(c3);params.add(c4);
		}else if(c3!=0){
			sBuffer.append(" AND A.categoryonid=? AND A.categorytwid=? AND A.categorythid=?");
			params.add(c1);params.add(c2);params.add(c3);
		}else if(c2!=0){
			sBuffer.append(" AND A.categoryonid=? AND A.categorytwid=?");
			params.add(c1);params.add(c2);
		}else if(c1!=0){
			sBuffer.append(" AND A.categoryonid=?");
			params.add(c1);
		}
		if(type!=0){
			sBuffer.append(" AND A.type=?");
			params.add(type);
		}
		if(!StrKit.isBlank(tags)){
			sBuffer.append(" AND EXISTS (SELECT 1 FROM t_bus_goods_tag_rel B WHERE B.goodsid=A.id AND B.tagid IN ("+tags+"))");
		}
		if(!StrKit.isBlank(keyword)){
			sBuffer.append(" AND A.name LIKE ?");
			params.add("%"+keyword+"%");
		}
		sBuffer.append(" ORDER BY createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT A.id, A.state,A.name,A.marketPrice,A.sellPrice,A.type,A.thumb,A.createDate", sBuffer.toString(), params.toArray());
	}

	/**
	 * 查找店铺产品
	 */
	public Goods findById(int id){
		return Goods.me().findById(id);
	}

	/**
	 *增加便利店商品
	 */
	@Before(Tx.class)
	public boolean add(Goods goods,String[] tags){
		//保存商品信息
		goods.save();
		//保存相应的商品标签关联
		if(tags!=null&&tags.length!=0){
			int batchSize = tags.length;
			List<String> sqlList = new ArrayList<String>();
			for(String tagid : tags){
				sqlList.add("INSERT INTO t_bus_goods_tag_rel(goodsid,tagid) VALUES("+goods.get("id")+","+tagid+")");
			}
			Db.batch(sqlList,batchSize);
		}
		return true;
	}

	/**
	 *修改便利店商品
	 */
	@Before(Tx.class)
	public boolean update(Goods goods,String[] tags){
		//更新商品信息
		goods.update();
		List<String> sqlList = new ArrayList<String>();
		//删除所有商品标签关联
		int batchSize = 1;
		sqlList.add("DELETE FROM t_bus_goods_tag_rel WHERE goodsid="+goods.getStr("id"));
		//保存相应的商品标签关联
		if(tags!=null&&tags.length!=0){
			batchSize = batchSize + tags.length;
			for(String tagid : tags){
				sqlList.add("INSERT INTO t_bus_goods_tag_rel(goodsid,tagid) VALUES("+goods.getStr("id")+","+tagid+")");
			}
		}
		Db.batch(sqlList,batchSize);
		return true;
	}

	/**
	 *查找出便利店商品
	 */
	public Record findGoodsById(int id){
		String sql="SELECT A.id,A.describe From t_bus_goods A where id=?";
	    return Db.findFirst(sql, id);
	}
	
	/**
	 * 图片排序
	 * @param sortMap
	 * @return
	 */
	public boolean sortImages(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_bus_goods_images SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	/**
	 * 商品图片列表
	 * @param goodsid
	 * @return
	 */
	public List<GoodsImages> findAllImages(int goodsid){
		return GoodsImages.me().find("SELECT A.* FROM t_bus_goods_images A WHERE A.goodsid=? ORDER BY A.orderNo",goodsid);
	}
	
	/**
	 * 添加图片
	 * @param newObject
	 * @return
	 */
	public boolean addImage(GoodsImages newObject){
		return newObject.save();
	}
	
	/**
	 * 删除图片
	 * @param id
	 * @return
	 */
	public boolean delImage(int id){
		return GoodsImages.me().deleteById(id);
	}
	
	/**
	 * 设置图片为商品logo
	 * @param id
	 * @return
	 */
	@Before(Tx.class)
	public boolean setImageLogo(int goodsid,int id){
		List<String> sqlList = new ArrayList<String>();
		//更新该商品所有LOGO的isLogo标识为0
		sqlList.add("UPDATE t_bus_goods_images SET isLogo=0 WHERE goodsid="+goodsid);
		//更新当前LOGO的isLogo标识为1
		sqlList.add("UPDATE t_bus_goods_images SET isLogo=1 WHERE goodsid="+goodsid+" AND id="+id);
		//更新该商品的LOGO字段值为当前LOGO的地址
		sqlList.add("UPDATE t_bus_goods SET thumb=(SELECT thumb FROM t_bus_goods_images WHERE id="+id+") WHERE id="+goodsid);
		Db.batch(sqlList, sqlList.size());
		return true;
	}
}