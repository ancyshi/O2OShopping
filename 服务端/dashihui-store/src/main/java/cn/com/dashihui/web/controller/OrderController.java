package cn.com.dashihui.web.controller;

import com.jfinal.aop.Before;
import com.jfinal.aop.Duang;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.kit.DatetimeKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.common.DispatchState;
import cn.com.dashihui.web.common.OrderCode;
import cn.com.dashihui.web.dao.Order;
import cn.com.dashihui.web.dao.Seller;
import cn.com.dashihui.web.service.OrderService;

public class OrderController extends BaseController {
	//使用Duang.duang进行封装，使普通类具有事务的功能
	private OrderService service = Duang.duang(OrderService.class);
	/**
	 * 订单列表页面
	 */
	public void index(){
		render("index.jsp");
	}
	/**
	 * 获得订单列表数据
	 */
	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		//订单编号
		String orderNum = getPara("orderNum");
		//下单时间范围
		String beginDate = getPara("beginDate");
		String endDate = getPara("endDate");
		//订单状态
		int state = getParaToInt("state",0);
		//收货地址
		String address = getPara("address");
		//买家电话
		String tel = getPara("tel");
		//配送方式
		int takeType=getParaToInt("takeType",0);
		//支付方式
		int payType=getParaToInt("payType",0);
		renderResult(0,service.findByPage(pageNum,pageSize,getStoreid(),orderNum,beginDate,endDate,state,address,tel,takeType,payType));
	}
	/**
	 * 查询订单详情
	 */
	public void detail(){
		String orderNum = getPara("orderNum");
		Order order = service.getOrderByOrderNum(orderNum);
		if(StrKit.notNull(order)){
			setAttr("obj", order);
			setAttr("goodsList", service.getGoodsListByOrderNum(orderNum));
			setAttr("logList", service.getLogListByOrderNum(orderNum));
			setAttr("eval", service.getEvalByOrderNum(orderNum));
		}
		render("detail.jsp");
	}
	
	/**
	 * 取消订单
	 * @param ORDERNUM 订单号
	 * @param REASON 取消订单理由
	 */
	public void doCancel(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
    		return;
		}else{
			Order order = service.getOrderByOrderNum(orderNum);
			if(order!=null){
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=OrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许取消");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int packState = order.getInt("packState");
				int takeType = order.getInt("takeType");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付+送货上门，打包状态为“未接单”时可取消订单
				if(payType==OrderCode.OrderPayType.ON_LINE&&takeType==OrderCode.OrderTakeType.DELIVER){
					if(packState!=OrderCode.OrderPackState.NO_ACCEPT){
						renderFailed("此订单当前不允许取消");
			    		return;
					}
				}else
				//2.2.在线支付+门店自取，打包状态为“未接单”时可取消订单
				if(payType==OrderCode.OrderPayType.ON_LINE&&takeType==OrderCode.OrderTakeType.TAKE_SELF){
					if(packState!=OrderCode.OrderPackState.NO_ACCEPT){
						renderFailed("此订单当前不允许取消");
			    		return;
					}
				}else
				//2.3.货到付款+送货上门时，发货状态为“未发货”时可取消订单
				if(payType==OrderCode.OrderPayType.ON_DELIVERY&&takeType==OrderCode.OrderTakeType.DELIVER){
					if(deliverState!=OrderCode.OrderDeliverState.NO_DELIVER){
						renderFailed("此订单当前不允许取消");
			    		return;
					}
				}else
				//2.4.货到付款+门店自取时，无限制
				if(payType==OrderCode.OrderPayType.ON_DELIVERY&&takeType==OrderCode.OrderTakeType.TAKE_SELF){
					
				}
				
				try {
					//在service中进行取消订单(修改订单状态、生成退款订单)的一系列操作,保证事务回滚
					service.cancelOrder(getCurrentUser(),order,null,payType,payState,orderNum);
					renderSuccess(order);
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}
				//3.其他情况
				renderFailed("取消订单失败");
				return;
			}else{
				renderFailed("订单不存在！");
				return;
			}
		}
	}
	
	/**
	 * 在线支付订单修改为已支付
	 */
	@Before(Tx.class)
	public void doPay(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			Order order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=OrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//2.判断订单为“在线支付”、“未支付”
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				if(payType!=OrderCode.OrderPayType.ON_LINE&&payState!=OrderCode.OrderPayState.NO_PAY){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//3.修改订单状态为“已支付”，并记录日志
				order.set("payState", OrderCode.OrderPayState.HAD_PAY).set("payDate", DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"));
				if(order.update()){
					service.log(orderNum,OrderCode.OrderLogType.PAY,"管理员："+getCurrentUser().getStr("username"),OrderCode.OrderLogType.PAY_ACTION, "订单支付成功，等待系统确认");
					//查询对应的商品清单
					order.put("goodsList", service.getGoodsListByOrderNum(orderNum));
					renderSuccess(order);
					return;
				}
			}
		}
		//其他情况
		renderFailed("操作失败");
		return;
	}
	
	/**
	 * 选择配送员
	 */
	public void toSetSeller(){
		setAttr("orderNum", getPara("orderNum"));
		render("setSeller.jsp");
	}
	
	/**
	 * 确认配送员
	 */
	@Before(Tx.class)
	public void doSetSeller(){
		String orderNum = getPara("orderNum");
		int sellerid = getParaToInt("sellerid");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			Order order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=OrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//2.判断订单打包状态为未接单
				int packState = order.getInt("packState");
				if(packState!=OrderCode.OrderPackState.NO_ACCEPT){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//3.判断配送员信息存在
				Seller seller = Seller.me().findById(sellerid);
				if(seller==null){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//4.添加配送员与订单关联关系，并设置订单打包状态为已接单
				if(Db.update("INSERT INTO t_dict_store_seller_rel(sellerid,orderNum,state) VALUES(?,?,?)",sellerid,orderNum,DispatchState.WAIT_PACK)==1){
					//更新订单表，记录配送员ID及打包状态为：已接单
					order.set("sellerid", sellerid).set("packState", OrderCode.OrderPackState.HAD_ACCEPT).update();
					service.log(orderNum,OrderCode.OrderLogType.ACCEPT,"管理员："+getCurrentUser().getStr("username"),OrderCode.OrderLogType.ACCEPT_ACTION, "系统已确认，等待商品打包");
					//查询对应的商品清单
					order.put("goodsList", service.getGoodsListByOrderNum(orderNum));
					order.put("sellerName",seller.getStr("name")).put("sellerTel",seller.getStr("tel"));
					renderSuccess(order);
					return;
				}
			}
		}
		//5.其他情况
		renderFailed("操作失败");
		return;
	}
	
	/**
	 * 开始配货
	 */
	@Before(Tx.class)
	public void doStartPack(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			Order order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=OrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//2.判断订单打包状态为已接单
				int packState = order.getInt("packState");
				if(packState!=OrderCode.OrderPackState.HAD_ACCEPT){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//3.获取配送员信息存在
				int sellerid = order.getInt("sellerid");
				Seller seller = Seller.me().findById(sellerid);
				//4.更新配送员与订单关联关系表中打包状态为开始配货
				if(Db.update("UPDATE t_dict_store_seller_rel SET state=? WHERE sellerid=? AND orderNum=? AND state=?",DispatchState.PACKING,sellerid,orderNum,DispatchState.WAIT_PACK)==1){
					//更新订单表，修改打包状态为：打包中
					order.set("packState", OrderCode.OrderPackState.PACKING).update();
					service.log(orderNum,OrderCode.OrderLogType.PACK,"管理员："+getCurrentUser().getStr("username"),OrderCode.OrderLogType.PACK_ACTION, "商品正在打包");
					//查询对应的商品清单
					order.put("goodsList", service.getGoodsListByOrderNum(orderNum));
					order.put("sellerName",seller.getStr("name")).put("sellerTel",seller.getStr("tel"));
					renderSuccess(order);
					return;
				}
			}
		}
		//其他情况
		renderFailed("操作失败");
		return;
	}
	
	/**
	 * 配货完成
	 */
	@Before(Tx.class)
	public void doPackFinish(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			Order order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=OrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//2.判断订单打包状态为打包中
				int packState = order.getInt("packState");
				if(packState!=OrderCode.OrderPackState.PACKING){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//3.获取配送员信息存在
				int sellerid = order.getInt("sellerid");
				Seller seller = Seller.me().findById(sellerid);
				//4.根据配送方式不同，操作不同
				if(order.getInt("takeType")==OrderCode.OrderTakeType.TAKE_SELF){
					//4-1.门店自取时，更新配送员与订单关联关系表中打包状态为待取货
					if(Db.update("UPDATE t_dict_store_seller_rel SET state=? WHERE sellerid=? AND orderNum=? AND state=?",DispatchState.WAIT_PACK,sellerid,orderNum,DispatchState.PACKING)==1){
						//更新订单表，修改打包状态为：打包完成
						order.set("packState", OrderCode.OrderPackState.PACK_FINISH).update();
						service.log(orderNum,OrderCode.OrderLogType.PACK_FINISH,"管理员："+getCurrentUser().getStr("username"),OrderCode.OrderLogType.PACK_FINISH_ACTION, "商品打包完成，请在营业时间内上门取货");
						//查询对应的商品清单
						order.put("goodsList", service.getGoodsListByOrderNum(orderNum));
						order.put("sellerName",seller.getStr("name")).put("sellerTel",seller.getStr("tel"));
						renderSuccess(order);
						return;
					}
				}else{
					//4-2.门店配送时，更新配送员与订单关联关系表中打包状态为配货完成
					if(Db.update("UPDATE t_dict_store_seller_rel SET state=? WHERE sellerid=? AND orderNum=? AND state=?",DispatchState.PACK_FINISH,sellerid,orderNum,DispatchState.PACKING)==1){
						//更新订单表，修改打包状态为：打包完成
						order.set("packState", OrderCode.OrderPackState.PACK_FINISH).update();
						service.log(orderNum,OrderCode.OrderLogType.PACK_FINISH,"管理员："+getCurrentUser().getStr("username"),OrderCode.OrderLogType.PACK_FINISH_ACTION, "商品打包完成，正在安排配送");
						//查询对应的商品清单
						order.put("goodsList", service.getGoodsListByOrderNum(orderNum));
						order.put("sellerName",seller.getStr("name")).put("sellerTel",seller.getStr("tel"));
						renderSuccess(order);
						return;
					}
				}
			}
		}
		//其他情况
		renderFailed("操作失败");
		return;
	}
	
	/**
	 * 为订单发货
	 */
	public void doSend(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			Order order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=OrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许发货");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int takeType = order.getInt("takeType");
				int packState = order.getInt("packState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付+送货上门，支付状态为“已付款”，打包状态为“打包完成”，发货状态为“未发货”时可发货
				if(payType==OrderCode.OrderPayType.ON_LINE&&takeType==OrderCode.OrderTakeType.DELIVER){
					if(payState!=OrderCode.OrderPayState.HAD_PAY||packState!=OrderCode.OrderPackState.PACK_FINISH||deliverState!=OrderCode.OrderDeliverState.NO_DELIVER){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}else
				//2.2.在线支付+门店自取，不可发货
				if(payType==OrderCode.OrderPayType.ON_LINE&&takeType==OrderCode.OrderTakeType.TAKE_SELF){
					renderFailed("此订单当前不允许操作");
					return;
				}else
				//2.3.货到付款+送货上门时，打包状态为“打包完成”，发货状态为“未发货”时可发货
				if(payType==OrderCode.OrderPayType.ON_DELIVERY&&takeType==OrderCode.OrderTakeType.DELIVER){
					if(deliverState!=OrderCode.OrderDeliverState.NO_DELIVER||packState!=OrderCode.OrderPackState.PACK_FINISH){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}else
				//2.4.货到付款+门店自取时，不可发货
				if(payType==OrderCode.OrderPayType.ON_DELIVERY&&takeType==OrderCode.OrderTakeType.TAKE_SELF){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//3.获取配送员信息存在
				int sellerid = order.getInt("sellerid");
				Seller seller = Seller.me().findById(sellerid);
				//4.更新配送员与订单关联关系表中打包状态为配送中
				if(Db.update("UPDATE t_dict_store_seller_rel SET state=? WHERE sellerid=? AND orderNum=? AND state=?",DispatchState.DISPATCHING,sellerid,orderNum,DispatchState.PACK_FINISH)==1){
					//更新订单表，修改发货状态为：已发货
					order.set("deliverState", OrderCode.OrderDeliverState.HAD_DELIVER).set("deliverDate", DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss")).update();
					service.log(orderNum,OrderCode.OrderLogType.DISPATCH,"管理员："+getCurrentUser().getStr("username"),OrderCode.OrderLogType.DISPATCH_ACTION, "商品正在配送，请注意保持电话畅通，大实惠配送员【"+seller.getStr("name")+"】，联系电话【"+seller.getStr("tel")+"】");
					//查询对应的商品清单
					order.put("goodsList", service.getGoodsListByOrderNum(orderNum));
					order.put("sellerName",seller.getStr("name")).put("sellerTel",seller.getStr("tel"));
					renderSuccess(order);
					return;
				}
			}
		}
		//其他情况
		renderFailed("操作失败");
		return;
	}
	
	/**
	 * 收货订单
	 */
	public void doSign(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			Order order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=OrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int takeType = order.getInt("takeType");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付+送货上门，支付状态为“已付款”，发货状态为“已发货”时可签收
				if(payType==OrderCode.OrderPayType.ON_LINE&&takeType==OrderCode.OrderTakeType.DELIVER){
					if(payState!=OrderCode.OrderPayState.HAD_PAY||deliverState!=OrderCode.OrderDeliverState.HAD_DELIVER){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}else
				//2.2.在线支付+门店自取，不可签收
				if(payType==OrderCode.OrderPayType.ON_LINE&&takeType==OrderCode.OrderTakeType.TAKE_SELF){
					renderFailed("此订单当前不允许操作");
					return;
				}else
				//2.3.货到付款+送货上门时，发货状态为“已发货”时可签收
				if(payType==OrderCode.OrderPayType.ON_DELIVERY&&takeType==OrderCode.OrderTakeType.DELIVER){
					if(deliverState!=OrderCode.OrderDeliverState.HAD_DELIVER){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}else
				//2.4.货到付款+门店自取时，不可签收
				if(payType==OrderCode.OrderPayType.ON_DELIVERY&&takeType==OrderCode.OrderTakeType.TAKE_SELF){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//3.签收订单操作，并记录日志
				try {
					//在service中进行订单签收(修改订单状态、商品返利、用户是否升级为黄金会员)的一系列操作,保证事务回滚
					service.receiveOrder(getCurrentUser(),order);
					//查询对应的商品清单
					order.put("goodsList", service.getGoodsListByOrderNum(orderNum));
					renderSuccess(order);
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}
				//4.其他情况
				renderFailed("操作失败");
				return;
			}
		}
	}
}
