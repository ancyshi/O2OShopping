package cn.com.dashihui.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.upload.UploadFile;

import cn.com.dashihui.kit.DirKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.StoreAd;
import cn.com.dashihui.web.service.StoreAdService;

public class AdController extends BaseController{
	private static final Logger logger = Logger.getLogger(AdController.class);
	private StoreAdService service = new StoreAdService();
    
    public void index(){
    	setAttr("adList", service.findAll(getStoreid()));
        render("index.jsp");
    }
    
    public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		service.sortAd(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }
    
	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：没有关联商品，2：图片为空，3：图片上传失败
	 */
	public void doAdd(){
		UploadFile thumb = getFile("thumb");
		//类型
		int type = getParaToInt("type");
		//链接URL
		String link = getPara("link");
		//关联商品
		String goodsid = getPara("goodsid");
		//是否在APP显示
		int isShowOnAPP = getParaToInt("isShowOnAPP",0);
		//是否在微商城显示
		int isShowOnWX = getParaToInt("isShowOnWX",0);
		if(type==2&&StrKit.isBlank(goodsid)){
			renderResult(1);
			return;
		}else{
			//保存
			StoreAd ad = new StoreAd()
				.set("storeid", getCurrentUser().get("id"))
				.set("type", type)
				.set("link", link)
				.set("goodsid", goodsid)
				.set("isShowOnAPP", isShowOnAPP)
				.set("isShowOnWX", isShowOnWX);
			//如果上传了图片，则上传至FTP，并记录图片文件名
			if(thumb!=null){
				String thumbFileName;
				String dir = DirKit.getDir(DirKit.STORE_AD);
				try {
					thumbFileName = uploadToFtp(dir,thumb);
				} catch (IOException e) {
					e.printStackTrace();
					renderResult(3);
					return;
				}
				ad.set("thumb", dir.concat(thumbFileName));
				if(service.addAd(ad)){
					renderSuccess(service.findById(ad.getInt("id")));
					return;
				}
			}else{
				renderResult(2);
				return;
			}
		}
	}
	
	public void toSetGoods(){
		render("setGoods.jsp");
	}
	
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：没有关联商品，2：图片上传失败
	 */
	public void doEdit(){
		UploadFile thumb = getFile("thumb");
		//ID
		String adid = getPara("adid");
		//类型
		int type = getParaToInt("type");
		//链接URL
		String link = getPara("link");
		//关联商品
		String goodsid = getPara("goodsid");
		//旧图片
		String thumbOld = getPara("thumbOld");
		//是否在APP显示
		int isShowOnAPP = getParaToInt("isShowOnAPP",0);
		//是否在微商城显示
		int isShowOnWX = getParaToInt("isShowOnWX",0);
		if(type==2&&StrKit.isBlank(goodsid)){
			renderResult(1);
			return;
		}else{
			//保存
			StoreAd ad = new StoreAd()
				.set("id", adid)
				.set("type", type)
				.set("link", link)
				.set("goodsid", goodsid)
				.set("isShowOnAPP", isShowOnAPP)
				.set("isShowOnWX", isShowOnWX);
			if(thumb!=null){
				//如果修改了图片，则上传新图片，删除旧图片
				String thumbFileName;
				String dir = DirKit.getDir(DirKit.STORE_AD);
				try {
					thumbFileName = uploadToFtp(dir,thumb);
				} catch (IOException e) {
					e.printStackTrace();
					renderResult(2);
					return;
				}
				ad.set("thumb", dir.concat(thumbFileName));
				if(service.editAd(ad)){
					try {
						deleteFromFtp(thumbOld);
					} catch (IOException e) {
						logger.error("更新时，旧图片"+thumbOld+"删除失败！");
						e.printStackTrace();
					}
					renderSuccess(service.findById(Integer.valueOf(adid)));
					return;
				}
			}else if(service.editAd(ad)){
				renderSuccess(service.findById(Integer.valueOf(adid)));
				return;
			}
		}
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0){
			StoreAd ad = service.findById(id);
			//先判断社区如果上传了图片，则删除图片
			if(!StrKit.isBlank(ad.getStr("thumb"))){
				try {
					deleteFromFtp(ad.getStr("thumb"));
				} catch (IOException e) {
					logger.error("删除时，相关图片"+ad.getStr("thumb")+"删除失败！");
					e.printStackTrace();
				}
			}
			//然后再对记录进行删除
			ad.delete();
			logger.info("删除广告位【"+id+"】，及其图片");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
