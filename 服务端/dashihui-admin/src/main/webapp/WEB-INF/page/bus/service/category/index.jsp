<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>服务分类管理</title>
    <jsp:include page="../../../include/header.jsp"></jsp:include>
	<!-- jquery tree table -->
	<link href='${BASE_PATH}/static/plugins/jquery-treetable/css/jquery.treetable.css' media='all' rel='stylesheet' type='text/css' />
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">服务分类</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6 col-lg-offset-6">
						<div class="pull-right">
							<button type="button" class="btn btn-link" onclick="toAdd(0,0)"><span class="fa fa-plus"></span> 添加</button>
							<button type="button" class="btn btn-link" onclick="doSort()"><span class="fa fa-save"></span> 保存排序</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<form id="sortForm" action="${BASE_PATH}/bus/service/category/doSort" method="post">
					<input type="hidden" name="sortKey" value="orderNo">
					<table id="dataTable" class="table table-hover">
						<thead>
							<tr>
								<th width="20%">分类名称</th>
								<th width="20%">分类编码</th>
								<th width="20%">是否可用</th>
								<th width="15%">分类排序 <a href="#" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="排序操作仅作用于同级之间"><span class="fa fa-warning"></span></a></th>
								<th width="15%">操作</th>
							</tr>
						</thead>
						<tbody>
						<!-- level1 -->
						<c:if test="${list!=null&&fn:length(list)!=0}">
							<c:forEach items="${list}" var="level1">
								<tr id="item${level1.id}" data-id="${level1.id}" data-node-id="${level1.id}" data-node-parent-id="${level1.parentid}">
									<td>${level1.name}</td>
									<td>${level1.code}</td>
									<td><c:if test="${level1.enabled==1}">可用</c:if><c:if test="${level1.enabled!=1}">不可用</c:if></td>
									<td><input name="orderNo${level1.id}" value="${level1.orderNo}" size="3" class="order form-control width50" ></td>
									<td>
										<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toAdd('${level1.id}',1)" title="添加"><span class="fa fa-plus fa-lg"></span></button> 
										<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('${level1.id}',1)" title="编辑"><span class="fa fa-edit fa-lg"></span></button>
										<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('${level1.id}',1)" title="删除"><span class="fa fa-remove fa-lg"></span></button>
									</td>
								</tr>
								<!-- level2 -->
				                <c:if test="${level1.children!=null&&fn:length(level1.children)!=0}">
				                	<c:forEach items="${level1.children}" var="level2">
					                	<tr id="item${level2.id}" data-id="${level2.id}" data-node-id="${level2.id}" data-node-parent-id="${level2.parentid}">
											<td>${level2.name}</td>
											<td>${level2.code}</td>
											<td><c:if test="${level2.enabled==1}">可用</c:if><c:if test="${level2.enabled!=1}">不可用</c:if></td>
											<td><input name="orderNo${level2.id}" value="${level2.orderNo}" size="3" class="order form-control width50" ></td>
											<td> 
												<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('${level2.id}',2)" title="编辑"><span class="fa fa-edit fa-lg"></span></button> 
												<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('${level2.id}',2)" title="删除"><span class="fa fa-remove fa-lg"></span></button>
											</td>
										 </tr>
			                	     </c:forEach>
			                      </c:if>
				               </c:forEach>
			               </c:if>
				         </tbody>
			          </table>
		            </form>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	<tr id="item{{id}}" data-id="{{id}}" data-node-id="{{id}}" data-node-parent-id="{{parentid}}">
        <td>{{name}}</td>		
        <td>{{code}}</td>
       	<td>{{enabled | flagTransform:1,'可用',0,'不可用'}}</td>
		<td><input name="orderNo{{id}}" value="{{orderNo}}" size="3" class="order form-control width50" required></td>
		<td>
			{{if type!=2}}
				<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toAdd('{{id}}',{{type}})" title="添加"><span class="fa fa-plus fa-lg"></span></button>  
			{{/if}}
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{id}}',{{type}})" title="编辑"><span class="fa fa-edit fa-lg"></span></button> 
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{id}}','{{code}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button>
		</td>
	</tr>
</script>
<!-- jquery tree table -->
<script src='${BASE_PATH}/static/plugins/jquery-treetable/js/jquery.treetable.js' type='text/javascript'></script>
<script type="text/javascript">
$(function(){
	$("#dataTable").treetable({expandable:true,clickableNodeNames:true,nodeIdAttr:"nodeId",parentIdAttr:"nodeParentId"}).treetable("collapseAll");
});
function onCategoryAdded(parentid,category){
	$("#dataTable").treetable("loadBranch",(parentid!=0?$("#dataTable").treetable("node",parentid):null),template("dataTpl",category));
}
function onCategoryEdited(category){
	$("#dataTable").treetable("updateNode",$("#dataTable").treetable("node",category.id),template("dataTpl",category));
}
var addDialog;
function toAdd(parentid,type){
	addDialog = Kit.dialog("添加","${BASE_PATH}/bus/service/category/toAdd/"+parentid+"?type="+(type+1)).open();
	
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/bus/service/category/toEdit/"+id).open();
}
function toDelete(id,code){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/bus/service/category/doDelete",{"id":id,"code":code},function(result){
			$("#dataTable").treetable("removeNode",id);
		});
	});
}
function doSort(){
	$("#sortForm").ajaxSubmit({
		success:function(data){
			switch(data.flag){
			case -1:
				Kit.alert("系统异常，请重试");return;
			case 0:
				window.location.reload();
			}
		}
	});
}
</script>
</body>
</html>