<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		rules:{link:{url:true}},
		messages:{
			link: {required: "请输入正确的链接地址"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						onEditSuccess(data.object);
						editDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="editForm" action="${BASE_PATH}/bus/ad/doEdit" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group">
		<label class="col-lg-2 control-label">图片</label>
		<div class="col-lg-9">
			<input type="file" id="thumb" name="thumb" accept="image/jpg" title="选择文件" <c:if test="${object.thumb!=null}">data-val="${object.thumb}"</c:if>>
		</div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">链接</label>
	    <div class="col-lg-9">
        	<input type="text" name="link" value="${object.link}" class="form-control" placeholder="请输入链接地址" maxlength="200">
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="thumbOld" value="${object.thumb}"/>
	<input type="hidden" name="adid" value="${object.id}"/>
</form>