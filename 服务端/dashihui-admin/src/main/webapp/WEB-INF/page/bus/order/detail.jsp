<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div style="min-height:300px;">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">订单信息</a></li>
		<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">商品列表</a></li>
		<li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">备注</a></li>
		<li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">收货人</a></li>
		<li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">订单日志</a></li>
		<c:if test="${eval!=null}">
		<li role="presentation"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">评价</a></li>
		</c:if>
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active container-fluid" id="tab1" style="padding-top:15px;">
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">订单号：</label>
				    <div class="text-content">${obj.orderNum}</div>
				</div>
			    <div class="col-lg-6">
				    <label class="text-label">下单时间：</label>
				    <div class="text-content"><fmt:formatDate value="${obj.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">订单状态：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.orderState==1}">正常</c:when>
					    	<c:when test="${obj.orderState==2}">已完成</c:when>
					    	<c:when test="${obj.orderState==3}">已取消</c:when>
					    	<c:when test="${obj.orderState==4}">已删除</c:when>
					    	<c:when test="${obj.orderState==5}">已过期</c:when>
				    	</c:choose>
				    </div>
			    </div>
				<div class="col-lg-6">
				    <label class="text-label">共计金额：</label>
				    <div class="text-content">${obj.amount} (元)</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-6">
					<label class="text-label">支付方式：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.payType==1}">在线支付</c:when>
					    	<c:when test="${obj.payType==2}">货到付款</c:when>
				    	</c:choose>
				    </div>
				</div>
				<div class="col-lg-6">
				    <label class="text-label">收货方式：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.takeType==1}">门店配送</c:when>
					    	<c:when test="${obj.takeType==2}">上门自取</c:when>
				    	</c:choose>
				    </div>
				</div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<c:choose>
			<c:when test="${obj.orderState==3}">
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">取消理由：</label>
				    <div class="text-content">${obj.reason}</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			</c:when>
			<c:when test="${obj.orderState==1||obj.orderState==2}">
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">支付状态：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.payState==1}">待支付</c:when>
					    	<c:when test="${obj.payState==2}">已支付</c:when>
				    	</c:choose>
				    </div>
			    </div>
				<div class="col-lg-6">
				    <label class="text-label">支付时间：</label>
				    <div class="text-content"><fmt:formatDate value="${obj.payDate}" pattern="yyyy-MM-dd HH:mm:ss"/></div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">打包状态：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.packState==1}">未接单</c:when>
					    	<c:when test="${obj.packState==2}">已接单</c:when>
					    	<c:when test="${obj.packState==3}">打包中</c:when>
					    	<c:when test="${obj.packState==4}">打包完成</c:when>
				    	</c:choose>
				    </div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<c:if test="${obj.takeType==1}">
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">发货状态：</label>
				    <div class="text-content">
			    		<c:choose>
					    	<c:when test="${obj.deliverState==1}">待发货</c:when>
					    	<c:when test="${obj.deliverState==2}">已发货</c:when>
				    	</c:choose>
				    </div>
			    </div>
				<div class="col-lg-6">
					<label class="text-label">发货时间：</label>
				    <div class="text-content">
						<fmt:formatDate value="${obj.deliverDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				    </div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			</c:if>
			<div class="row">
				<div class="col-lg-6">
				    <label class="text-label">收货状态：</label>
				    <div class="text-content">
				    	<c:choose>
					    	<c:when test="${obj.takeType==1}">
						    	<c:choose>
							    	<c:when test="${obj.orderState==1}">待收货</c:when>
							    	<c:when test="${obj.orderState==2}">已签收</c:when>
						    	</c:choose>
					    	</c:when>
					    	<c:when test="${obj.takeType==2}">
					    		<c:choose>
							    	<c:when test="${obj.orderState==1}">待取货</c:when>
							    	<c:when test="${obj.orderState==2}">已取货</c:when>
						    	</c:choose>
					    	</c:when>
				    	</c:choose>
				    </div>
				</div>
				<div class="col-lg-6">
				    <label class="text-label">完成时间：</label>
				    <div class="text-content"><fmt:formatDate value="${obj.signDate}" pattern="yyyy-MM-dd HH:mm:ss"/></div>
				</div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			</c:when>
			</c:choose>
		</div>
		<div role="tabpanel" class="tab-pane" id="tab2" style="padding-top:10px;">
			<div class="table-responsive">
	    		<table class="table ">
					 <thead>
			         	<tr>
							<th width="40%">商品名称</th>
							<th width="10%">单价(元)</th>
							<th width="10%">数量</th>
							<th width="20%">金额(元)</th>
			         	</tr>
			         </thead>
			         <tbody >
				       <c:if test="${goodsList!=null}">
							<c:forEach items="${goodsList}" var="goods">
								<tr>
									<td>${goods.name}</td>
									<td>${goods.price}</td>
									<td>${goods.count}</td>
									<td>${goods.amount}</td>
				         		</tr>
							</c:forEach>
						</c:if>
			         </tbody>
				</table>
			</div>
		</div>
	    <div role="tabpanel" class="tab-pane" id="tab3" style="padding-top:15px;">
	    	${obj.describe}
	    </div>
	    <div role="tabpanel" class="tab-pane" id="tab4" style="padding-top:15px;">
	    	<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">收货人：</label>
				    <div class="text-content">${obj.linkName}</div>
				</div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">性别：</label>
				    <div class="text-content">${obj.sex}</div>
				</div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">收货人电话：</label>
				    <div class="text-content">${obj.tel}</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">收货人地址：</label>
				    <div class="text-content">${obj.address}</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="tab5" style="padding-top:10px;">
			<div class="table-responsive">
	    		<table class="table ">
					 <thead>
			         	<tr>
							<th width="20%">时间</th>
							<th width="20%">操作人</th>
							<th width="15%">动作</th>
							<th width="45%">内容</th>
			         	</tr>
			         </thead>
			         <tbody >
				       <c:if test="${logList!=null}">
							<c:forEach items="${logList}" var="log">
								<tr>
									<td><fmt:formatDate value="${log.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
									<td>${log.user}</td>
									<td>${log.action}</td>
									<td style="white-space:normal;">${log.content}</td>
				         		</tr>
							</c:forEach>
						</c:if>
			         </tbody>
				</table>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="tab6" style="padding-top:15px;">
	    	<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">商品评分：</label>
				    <div class="text-content">
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval1>=1}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval1>=2}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval1>=3}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval1>=4}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval1>=5}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    </div>
				</div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">配送速度：</label>
				    <div class="text-content">
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval2>=1}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval2>=2}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval2>=3}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval2>=4}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval2>=5}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    </div>
				</div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">服务质量：</label>
				    <div class="text-content">
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval3>=1}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval3>=2}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval3>=3}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval3>=4}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    	<i class="fa text-danger text-lg <c:choose><c:when test="${eval.eval3>=5}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
				    </div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">评价内容：</label>
				    <div class="text-content">${eval.content}</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
			<div class="row">
				<div class="col-lg-12">
				    <label class="text-label">评价时间：</label>
				    <div class="text-content">${eval.createDate}</div>
			    </div>
			    <div class="clearfix"></div>
			</div>
			<hr class="hr-line"/>
	    </div>
	</div>
</div>
<div class="row" style="display:none;">
	<div class="col-lg-offset-2 col-lg-9">
		<button class="btn btn-warning col-sm-2 pull-left m-l-10" type="button" <c:if test="${obj.payState!=1||obj.orderState!=1}">disabled</c:if>>支付</button>
		<button class="btn btn-success col-sm-2 pull-left m-l-10" type="button" <c:if test="${obj.payState!=2||obj.deliverState!=1||obj.orderState!=1}">disabled</c:if>>发货</button>
		<button class="btn btn-info col-sm-2 pull-left m-l-10" type="button" <c:if test="${obj.payState!=2||obj.deliverState!=2||obj.orderState!=1}">disabled</c:if>>完成</button>
		<button class="btn btn-default col-sm-2 pull-left m-l-10" type="button" <c:if test="${obj.payState!=1||obj.orderState!=1}">disabled</c:if>>作废</button>
		<button class="btn btn-default col-sm-2 pull-left m-l-10" type="button" onclick="javascript:showOrderDialog.close();" autocomplete="off">关闭</button>
	</div>
</div>