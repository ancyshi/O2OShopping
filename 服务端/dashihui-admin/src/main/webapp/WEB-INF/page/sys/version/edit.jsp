<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		rules: {       
			versionPart1:{digits:true},
			versionPart2:{digits:true},
			versionPart3:{digits:true},
			downurl:{       
	            required:true,       
	            url:false,
	            maxlength:200
	        }  
		},
		messages:{
			type: {required: "请输入类别"},
			versionPart1: {required: ""},
			versionPart2: {required: ""},
			versionPart3: {required: ""},
			downurl: {required: "请输入下载地址",url: "请输入合法的下载地址"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
						case -1:
							Kit.alert("系统异常，请重试");
							return;
						case 0:
							editDialog.close();
							dataPaginator.loadPage(1);
					}
				}
			});
		}
	});
	$(".dropdown-item[data-dropdown-selected]").trigger("click");
});
</script>
<form id="editForm" action="${BASE_PATH}/sys/version/doEdit" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">平台</label>
	    <div class="col-lg-9">
	    	<select name="type" class="selectpicker form-control">
				<option value="A" <c:if test="${object.type=='A'}">selected</c:if>>安卓</option>
				<option value="B" <c:if test="${object.type=='B'}">selected</c:if>>iPhone</option>
	        </select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">版本号</label>
	    <div class="col-lg-9">
	    	<div class="input-group">
	        	<input type="text" name="versionPart1" class="form-control" value="${object.versionPart1}" required maxlength="3" size="3" data-error-place="#versionError">
	        	<span class="input-group-btn"></span>
	        	<input type="text" name="versionPart2" class="form-control" value="${object.versionPart2}" required maxlength="3" size="3" data-error-place="#versionError">
	        	<span class="input-group-btn"></span>
	        	<input type="text" name="versionPart3" class="form-control" value="${object.versionPart3}" required maxlength="5" size="5" data-error-place="#versionError">
	    	</div>
        	<div id="versionError"></div>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">下载地址</label>
        <div class="col-lg-9 controls textarea">
        	<textarea name="downurl" class="form-control" placeholder="请输入下载地址">${object.downurl}</textarea>
        </div>
	</div>
	<div class="form-group">
        <label class="col-lg-2 control-label"></label>
        <div class="col-lg-9">
        	<input name="updateFlag" type="checkbox" value="1" <c:if test="${object.updateFlag=='1'}">checked="checked"</c:if>> 强制升级
        </div>
    </div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">升级内容</label>
        <div class="col-lg-9 controls textarea">
            <textarea name='updatelog' class="form-control" placeholder="请输入升级内容" maxlength="200">${object.updatelog}</textarea>
        </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">发布时间</label>
       	<div class="col-lg-9" >
       		<fmt:formatDate value="${object.releaseDate}" pattern="yyyy-MM-dd HH:mm:ss" var="releaseDate" />
	        <input class="form-control datetimepicker" id="releaseDate" name="releaseDate" placeholder="请选择发布时间，为空则以系统时间为准" type="text" data-format="yyyy-mm-dd hh:ii" readonly="readonly" value="${releaseDate}">
	        <span class="help-block m-b-none label label-info">*APP对外公布的发布时间，根据该值排序，取日期最近的版本为最新版本</span>
    	</div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" value="${object.id}" name="versionid">
</form>