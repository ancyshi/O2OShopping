<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">文章分类</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-offset-6 col-lg-6">
						<div class="pull-right">
						<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
						<button type="button" class="btn btn-link" onclick="doSort()"><span class="fa fa-save"></span> 保存排序</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<form id="sortForm" action="${BASE_PATH}/wx/article/category/doSort" method="post">
					<input type="hidden" name="sortKey" value="orderNo">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
			                    <th>分类</th>
			                    <th width="15%">归属</th>
			                    <th width="10%">排序</th>
			                    <th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
			        </form>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.name}} <span class="label label-info">{{item.articleNum}}</span></td>
		<td>{{item.mp | flagTransform:1,'便利店老板内参',2,'O2O创业内参'}}</td>
		<td><input name="orderNo{{item.id}}" value="{{item.orderNo}}" size="3" class="order form-control width50" required ></td>
		<td><button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button> <button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button></td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
$(loadAll);
function loadAll(){
	$.post("${BASE_PATH}/wx/article/category/list/",{"mp":"${mp}"},function(result){
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",{"list":result.object}));
	});
}
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/wx/article/category/toAdd?mp=${mp}").open();
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/wx/article/category/toEdit/"+id).open();
}
function onEditSuccess(id,newObject){
	$("#item"+id).replaceWith(template("list-tpl",{"list":[newObject]}));
}
function toDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/wx/article/category/doDelete/"+id,function(result){
			if(result.flag==1){
				Kit.alert("请先删除分类下的文章！");
			}else{
				$("#item"+id).remove();
			}
		});
	});
}
function doSort(){
	$("#sortForm").ajaxSubmit({
		success:function(data){
			switch(data.flag){
			case -1:
				Kit.alert("系统异常，请重试");return;
			case 0:
				window.location.reload();
			}
		}
	});
}
</script>
</body>
</html>