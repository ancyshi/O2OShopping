<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 编辑器 -->
<link rel="stylesheet" href="${BASE_PATH}/static/plugins/kindEditor/themes/default/default.css" />
<link rel="stylesheet" href="${BASE_PATH}/static/plugins/kindEditor/plugins/code/prettify.css" />
<script type="text/javascript" src="${BASE_PATH}/static/plugins/kindEditor/kindeditor.js"></script>
<script type="text/javascript" src="${BASE_PATH}/static/plugins/kindEditor/lang/zh_CN.js"></script>
<script type="text/javascript" src="${BASE_PATH}/static/plugins/kindEditor/plugins/code/prettify.js"></script>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		ignore: "",
		messages:{
			name: {required: "请输入分类名"},
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						loadAll();
						addDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="addForm" action="${BASE_PATH}/wx/article/category/doAdd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">分类名</label>
	    <div class="col-lg-9">
        	<input type="text" name="name" value="" class="form-control" placeholder="请输入分类名" required maxlength="50">
	    </div>
	</div>
	<input type="hidden" name="mp" value="${mp}">
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>