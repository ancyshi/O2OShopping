<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 编辑器 -->
<link rel="stylesheet" href="${BASE_PATH}/static/plugins/kindEditor/themes/default/default.css" />
<link rel="stylesheet" href="${BASE_PATH}/static/plugins/kindEditor/plugins/code/prettify.css" />
<script type="text/javascript" src="${BASE_PATH}/static/plugins/kindEditor/kindeditor.js"></script>
<script type="text/javascript" src="${BASE_PATH}/static/plugins/kindEditor/lang/zh_CN.js"></script>
<script type="text/javascript" src="${BASE_PATH}/static/plugins/kindEditor/plugins/code/prettify.js"></script>
<script type="text/javascript">
<!--
$(function(){
	var kindEditor;
	kindEditor = KindEditor.create($("#context"), {
		width:'100%',
		height:'500px',
		resizeType :0,//2或1或0，2时可以拖动改变宽度和高度，1时只能改变高度，0时不能拖动。
		langType : 'zh_CN',
	    allowUpload : true, //允许上传图片
	    uploadJson : '${BASE_PATH}/wx/article/uploadimg',//指定上传文件的服务器端程序。
		items : [
			'source','|','fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
			'insertunorderedlist', '|', 'image', 'link'], 
		afterCreate : function() {this.sync();}, 
	    afterBlur:function() {this.sync();} 
	 });
	//初始化表单验证
	$("#addForm").validate({
		ignore: "",
		messages:{
			title: {required: "请输入标题"},
			describe: {required: "请输入概要"},
			context: {required: "请输入内容"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 6:
						Kit.alert("图片上传失败，请重新上传");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						addDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="addForm" action="${BASE_PATH}/wx/article/doAdd" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group">
	    <label class="col-lg-2 control-label">标题</label>
	    <div class="col-lg-9">
        	<input type="text" name="title" value="" class="form-control" placeholder="请输入标题" required maxlength="100">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">概要</label>
	    <div class="col-lg-9">
        	<input type="text" name="describe" value="" class="form-control" placeholder="请输入概要" required maxlength="100">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">分类</label>
	    <div class="col-lg-9">
	        <select name="categoryid" class="selectpicker form-control" data-url="${BASE_PATH}/wx/article/category/all?mp=${mp}" data-issingle="true" data-key="id:name"></select>
	    </div>
	</div>
	<div class="form-group">
		<label class="col-lg-2 control-label">缩略图</label>
		<div class="col-lg-9">
			<input type="file" id="thumb" name="thumb" accept="image/jpg" title="选择文件">
		</div>
		<div class="col-lg-offset-2 p-t-5 col-lg-9"><span class="text-success">*建议640px*360px，大小30K以内，建议PNG格式，支持JPEG、PNG</span></div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">内容</label>
	    <div class="col-lg-9">
        	<textarea id="context" name="context" placeholder="请输入内容" required></textarea>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">置顶</label>
	    <div class="col-lg-9">
        	<ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="isTop" value="1"> 是</li>
	    		<li><input type="radio" class="iCheck" name="isTop" value="0" checked> 否</li>
	    	</ul>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>