<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title></title>
    <!-- amazeui -->
    <link rel="stylesheet" type="text/css" href="../static/ui/css/amazeui.min.css"/>
    <style type="text/css">
    	.app-tabs{
		    margin:0;
    	}
    	.am-tabs-bd .am-tab-panel{
    		padding:0;
    	}
    	.app-tabs > li.app-tab-active {
		    background-color: #0e90d2;
		    color: #fff;
		}
		.am-tabs-default .am-tabs-nav > .am-active a{
			background-color:#eee;
			color:#21b100;
		}
	    .app-list{
	    	margin: 0;
	    	list-style: none;
	    	padding:0;
	    }
	    .app-list li{
	    	padding: 7px 0;
	    	border-bottom: 1px solid #eee;
	    }
    </style>
</head>
<body>
	<#if articleTopList?exists>
	<div data-am-widget="slider" class="am-slider am-slider-c2">
		<ul class="am-slides">
			<#list articleTopList as article>
			<li>
				<a href="detail/${article.categoryid}-${article.id}.html"><img src="${FTP_PATH}${article.thumb}" height="180"></a>
				<div class="am-slider-desc am-text-truncate">${article.title}</div>
			</li>
			</#list>
		</ul>
	</div>
	</#if>
	
	<#if categoryList?exists>
	<div data-am-widget="tabs" class="am-tabs am-tabs-default app-tabs">
		<ul class="am-tabs-nav am-cf">
			<#list categoryList as item>
			<li <#if item_index==0>class="am-active"</#if>><a href="[item-${item.id}]">${item.name}</a></li>
			</#list>
		</ul>
		<div class="am-tabs-bd" style="overflow:hidden;border:none;">
			<#list categoryList as item>
			<div item-${item.id} class="am-tab-panel <#if item_index==0>am-active</#if>">
				<ul class="app-list" id="category${item.id}">
					<#if item.children?exists>
					<#list item.children as article>
					<li class="am-g am-list-item-desced am-list-item-thumbed am-list-item-thumb-left">
						<#if article.thumb?exists>
						<div class="am-u-sm-3 am-u-md-2 am-u-lg-1 am-list-thumb">
							<a href="detail/${article.categoryid}-${article.id}.html"><img src="${FTP_PATH}${article.thumb}" width="100%"/></a>
						</div>
						</#if>
						<div class="am-u-sm-9 am-u-md-10 am-u-lg-11 am-list-main">
							<h3 class="am-list-item-hd am-text-truncate" style="margin-bottom:0;"><a href="detail/${article.categoryid}-${article.id}.html">${article.title}</a></h3>
							<div class="am-list-item-text">${article.describe}</div>
						</div>
					</li>
					</#list>
					</#if>
				</ul>
			</div>
			</#list>
		</div>
	</div>
	</#if>
	<!-- lib -->
	<script type="text/javascript" src="../static/lib/jquery/jquery-1.11.2.min.js" ></script>
	<!-- amazeui -->
	<script type="text/javascript" src="../static/ui/js/amazeui.min.js"></script>
</body>
</html>