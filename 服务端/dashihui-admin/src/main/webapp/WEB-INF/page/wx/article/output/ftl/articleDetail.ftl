<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title></title>
    <!-- amazeui -->
    <link rel="stylesheet" type="text/css" href="../../static/ui/css/amazeui.min.css"/>
    <style type="text/css">
    	body{
    		padding: 10px 0;
    	}
    	.am-container{
    		max-width: 740px;
    	}
    	.app-title{
		    border-bottom: 1px solid #e3e3e3;
		    padding: 5px 0;
    	}
    	.app-thumb{
    		padding:5px 0;
    		margin:10px 0;
    	}
    	.app-content{
    		margin-top:10px;
    		color: #3e3e3e;
    	}
		.app-content * {
		    box-sizing: border-box !important;
		    max-width: 100% !important;
		    word-wrap: break-word !important;
		}
    	.app-content p {
		    clear: both;
		    min-height: 1em;
		    white-space: pre-wrap;
		    margin: 0;
		}
    </style>
</head>
<body>
	<#if detail?exists>
	<div class="am-container">
		<div class="app-title"><h2>${detail.title}</h2></div>
		<!-- <div class="am-text-right">${detail.createDate?string("yyyy-MM-dd")}</div> -->
		<#if detail.thumb?exists>
		<div class="app-thumb"><img src="${FTP_PATH}${detail.thumb}" width="100%"></div>
		</#if>
		<div class="app-content">${detail.context}</div>
	</div>
	</#if>
</body>
</html>