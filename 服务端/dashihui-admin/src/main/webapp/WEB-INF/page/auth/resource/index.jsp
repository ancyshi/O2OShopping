<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
	<!-- jquery tree table -->
	<link href='${BASE_PATH}/static/plugins/jquery-treetable/css/jquery.treetable.css' media='all' rel='stylesheet' type='text/css' />
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">权限</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6 col-lg-offset-6">
						<div class="pull-right">
							<button type="button" class="btn btn-link" onclick="toAdd(0,0)"><span class="fa fa-plus"></span> 添加</button>
							<button type="button" class="btn btn-link" onclick="doSort()"><span class="fa fa-save"></span> 保存排序</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<form id="sortForm" action="${BASE_PATH}/auth/resource/doSort" method="post">
					<input type="hidden" name="sortKey" value="orderNo">
					<table id="dataTable" class="table table-hover">
						<thead>
							<tr>
								<th width="30%">权限名称</th>
								<th width="25%">权限代码</th>
								<th width="25%">菜单URL</th>
								<th width="5%">排序 <a href="#" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="排序操作仅作用于同级之间"><span class="fa fa-warning"></span></a></th>
			                    <th width="15%">操作</th>
							</tr>
						</thead>
						<tbody>
						<!-- level1 -->
						<c:if test="${resourceList!=null&&fn:length(resourceList)!=0}">
							<c:forEach items="${resourceList}" var="level1">
							<tr id="item${level1.id}" data-id="${level1.id}" data-node-id="${level1.id}" data-node-parent-id="0">
								<td>${level1.name}</td>
								<td>${level1.code}</td>
								<td></td>
								<td><input name="orderNo${level1.id}" value="${level1.orderNo}" size="3" class="order form-control width50" required></td>
								<td><button type="button" class="btn btn-sm btn-success btn-simple" onclick="toAdd('${level1.id}',1)" title="添加"><span class="fa fa-plus fa-lg"></span></button> <button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('${level1.id}',1)" title="编辑"><span class="fa fa-edit fa-lg"></span></button> <button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('${level1.id}',1)" title="删除"><span class="fa fa-remove fa-lg"></span></button></td>
							</tr>
							<!-- level2 -->
			                <c:if test="${level1.children!=null&&fn:length(level1.children)!=0}">
			                	<c:forEach items="${level1.children}" var="level2">
			                	<tr id="item${level2.id}" data-id="${level2.id}" data-node-id="${level2.id}" data-node-parent-id="${level2.parentid}">
									<td>${level2.name}</td>
									<td>${level2.code}</td>
									<td>${level2.url}</td>
									<td><input name="orderNo${level2.id}" value="${level2.orderNo}" size="3" class="order form-control width50" required></td>
									<td><button type="button" class="btn btn-sm btn-success btn-simple" onclick="toAdd('${level2.id}',2)" title="添加"><span class="fa fa-plus fa-lg"></span></button> <button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('${level2.id}',2)" title="编辑"><span class="fa fa-edit fa-lg"></span></button> <button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('${level2.id}',2)" title="删除"><span class="fa fa-remove fa-lg"></span></button></td>
								</tr>
								<!-- level3 -->
								<c:if test="${level2.children!=null&&fn:length(level2.children)!=0}">
				                	<c:forEach items="${level2.children}" var="level3">
				                	<tr id="item${level3.id}" data-id="${level3.id}" data-node-id="${level3.id}" data-node-parent-id="${level3.parentid}">
										<td>${level3.name}</td>
										<td>${level3.code}</td>
										<td>${level3.url}</td>
										<td><input name="orderNo${level3.id}" value="${level3.orderNo}" size="3" class="order form-control width50" required></td>
										<td><button type="button" class="btn btn-sm btn-success btn-simple" onclick="toEdit('${level3.id}',3)" title="编辑"><span class="fa fa-edit fa-lg"></span></button> <button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('${level3.id}',3)" title="删除"><span class="fa fa-remove fa-lg"></span></button></td>
									</tr>
				                	</c:forEach>
				                </c:if>
			                	</c:forEach>
			                </c:if>
				        	</c:forEach>
			        	</c:if>
						</tbody>
					</table>
					</form>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	<tr id="item{{id}}" data-id="{{id}}" data-node-id="{{id}}" {{if parentid}}data-node-parent-id="{{parentid}}"{{/if}}>
		<td>{{name}}</td>
		<td>{{code}}</td>
		<td>{{if type==2}}{{url}}{{/if}}</td>
		<td><input name="orderNo{{id}}" value="{{orderNo}}" size="3" class="order form-control width50" required></td>
		<td>{{if type!=3}}<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toAdd('{{id}}',{{type}})" title="添加"><span class="fa fa-plus fa-lg"></span></button> {{/if}}<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{id}}',{{type}})" title="编辑"><span class="fa fa-edit fa-lg"></span></button> <button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{id}}',{{type}})" title="删除"><span class="fa fa-remove fa-lg"></span></button></td>
	</tr>
</script>
<!-- jquery tree table -->
<script src='${BASE_PATH}/static/plugins/jquery-treetable/js/jquery.treetable.js' type='text/javascript'></script>
<script type="text/javascript">
$(function(){
	$("#dataTable").treetable({expandable:true,clickableNodeNames:true,nodeIdAttr:"nodeId",parentIdAttr:"nodeParentId"});
});
function onResourceAdded(parentid,resource){
	$("#dataTable").treetable("loadBranch",(parentid!=0?$("#dataTable").treetable("node",parentid):null),template("dataTpl",resource));
}
function onResourceEdited(resource){
	$("#dataTable").treetable("updateNode",$("#dataTable").treetable("node",resource.id),template("dataTpl",resource));
}
var addDialog;
function toAdd(parentid,type){
	addDialog = Kit.dialog("添加","${BASE_PATH}/auth/resource/toAdd/"+parentid+"?type="+(type+1)).open();
}
var editDialog;
function toEdit(id,type){
	editDialog = Kit.dialog("修改","${BASE_PATH}/auth/resource/toEdit/"+id+"?type="+(type+1)).open();
}
function toDelete(id,type){
	Kit.confirm("提示",(type==3?"您确定要删除这条数据吗？":"将同时删除其所有子权限，您确定要删除这条数据吗？"),function(){
		$.post("${BASE_PATH}/auth/resource/doDelete/"+id,function(result){
			$("#dataTable").treetable("removeNode",id);
		});
	});
}
function doSort(){
	$("#sortForm").ajaxSubmit({
		success:function(data){
			switch(data.flag){
			case -1:
				Kit.alert("系统异常，请重试");return;
			case 0:
				window.location.reload();
			}
		}
	});
}
</script>
</body>
</html>