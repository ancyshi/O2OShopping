<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		messages:{
			name: {required: "请输入角色"},
			alias: {required: "请输入角色别名"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						addDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="addForm" action="${BASE_PATH}/auth/role/doAdd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">角色名称</label>
	    <div class="col-lg-9">
        	<input type="text" name="name" value="" class="form-control" placeholder="请输入角色名称" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">角色别名</label>
	    <div class="col-lg-9">
        	<input type="text" name="alias" value="" class="form-control" placeholder="请输入角色别名" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">备注</label>
	    <div class="col-lg-9">
	    	<textarea class="form-control" name="remark" rows="3" placeholder="请输入备注" maxlength="300"></textarea>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>