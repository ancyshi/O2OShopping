<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">欢迎使用管理平台</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
			</div>
		</div>
	</div>
	<jsp:include page="include/footer.jsp"></jsp:include>
</div>
<jsp:include page="include/javascripts.jsp"></jsp:include>
</body>
</html>