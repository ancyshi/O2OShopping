<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">店铺</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-2 col-lg-9">
				<select id="type" name="type" class="selectpicker pull-left p-l-5 " data-width="10%">
					<option value="">全部</option>
					<option value="1">总部</option>
					<option value="2">总店</option>
					<option value="3">分店</option>
					<option value="4">配送</option>
				</select>
				<select id="attribute" name="attribute" class="selectpicker pull-left p-l-5 " data-width="10%">
					<option value="">全部</option>
					<option value="1">加盟店</option>
					<option value="2">直营店</option>
				</select>
				<select id="sProvince" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/province"  data-isfirst="true" data-next="#sCity" data-key="id:name"></select>
				<select id="sCity" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/city/{value}"  data-next="#sArea" data-key="id:name"></select>
				<select id="sArea" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/area/{value}"  data-key="id:name"></select>
				<input type="text" id="sTitle" value="" class="form-control pull-left m-l-5 width200" placeholder="店铺名称" maxlength="50" />
				<button type="button" class="btn btn-success pull-left m-l-5" onclick="doSearch()">查询</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div><p></p></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
						<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
			                	<th width="50"></th>
								<th width="20%">店铺名称</th>
								<th width="8%">门店类型</th>
								<th width="8%">门店属性</th>
								<th width="20%">地址</th>
								<th width="8%">店长</th>
								<th width="8%">电话</th>
								<th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="{{item.id}}" data-id="{{item.id}}">
		<td><a href="javascript:void(0)" class="thumbnail thumbnail-none-margin width50"><img src="${FTP_PATH}{{item.thumb}}"></a></td>
		<td>{{item.title}}</td>
		<td>{{item.type | flagTransform:1,'总部',2,'总店',3,'分店',4,'配送'}}</td>
		<td>{{item.attribute | flagTransform:1,'加盟店',2,'直营店'}}</td>
		<td>{{item.address}}</td>
		<td>{{item.username}}</td>
		<td>{{item.tel}}</td>
		<td>
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toEditPwd('{{item.id}}')" title="修改密码"><span class="fa fa-lock fa-lg"></span></button> 
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="doDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button> 
		</td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(doSearch);
function doSearch(){
	var p = $("#sProvince").val(), c = $("#sCity").val(), a = $("#sArea").val();
	var t = $("#title").val();
	var type = $("#type").val(), attribute = $("#attribute").val();
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/dict/store/page",{p:p,c:c,a:a,t:t,type:type,attribute:attribute,"pageSize":20},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var addDialog;
function toAdd(p,c,a){
	addDialog = Kit.dialog("添加","${BASE_PATH}/dict/store/toAdd").open();
}
var editDialog;
function toEdit(id,a){
	editDialog = Kit.dialog("修改","${BASE_PATH}/dict/store/toEdit/"+id).open();
}
var editPwdDialog;
function toEditPwd(id){
	editPwdDialog = Kit.dialog("修改密码","${BASE_PATH}/dict/store/toEditPwd/"+id).open();
}
function doDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/dict/store/doDelete/"+id,function(result){
			dataPaginator.loadPage(1);
		});
	});
}
</script>
</body>
</html>