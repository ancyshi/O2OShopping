<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
	<!-- jquery tree table -->
	<link href='${BASE_PATH}/static/plugins/jquery-treetable/css/jquery.treetable.css' media='all' rel='stylesheet' type='text/css' />
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">社区</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-3 col-lg-9">
				<select id="sProvince" class="selectpicker pull-left" data-width="10%" data-url="${BASE_PATH}/api/province" data-isfirst="true" data-next="#sCity" data-key="id:name" ></select>
				<select id="sCity" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/city/{value}" data-next="#sArea" data-key="id:name" ></select>
				<select id="sArea" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/area/{value}"  data-key="id:name" ></select>
				<input type="text" id="sTitle" value="" class="form-control pull-left m-l-5 width200" placeholder="社区名称" maxlength="50">
				<button type="button" class="btn btn-success pull-left m-l-5" onclick="doSearch()">查询</button>
			</div>
		</div>
		<div><p></p></div>
		<div><p></p></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
						<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
			                	<th width="30%">社区名称</th>
								<th width="30%">社区地址</th>
								<th width="20%">关联便利店</th>
								<th width="20%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.title}}</td>
		<td>{{item.address}}</td>
		<td>{{item.storeTitle}}</td>
		<td>
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button> 
 			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button> 
            <button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toSetStore('{{item.id}}','{{item.province}}','{{item.city}}','{{item.area}}')" title="关联便利店"><span class="fa fa-info fa-lg"></span></button>
		</td>
	</tr>
{{/each}}
</script>
<!-- jquery tree table -->
<script type="text/javascript">
var dataPaginator;
$(doSearch);
function doSearch(){
	var p = $("#sProvince").val(), c = $("#sCity").val(), a = $("#sArea").val(), t = $("#sTitle").val();
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/dict/community/page",{p:p,c:c,a:a,t:t,"pageSize":20},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/dict/community/toAdd").open();
}
var editDialog;
function toEdit(id,a){
	editDialog = Kit.dialog("修改","${BASE_PATH}/dict/community/toEdit/"+id).open();
}
function toDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/dict/community/doDelete/"+id,function(result){
			dataPaginator.loadPage(1);
		});
	});
}
var setStoreDialog;
function toSetStore(id,p,c,a){
	setStoreDialog = Kit.dialog("关联便利店","${BASE_PATH}/dict/community/toSetStore/"+id+"?p="+p+"&c="+c+"&a="+a).open();
}
</script>
</body>
</html>