package cn.com.dashihui.kit;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DirKit {
	public static final int GOODS_BASE = 0;
	public static final int GOODS_LOGO = 1;
	public static final int GOODS_IMAGES = 2;
	public static final int GOODS_DETAIL = 3;
	public static final int STORE_LOGO = 4;
	public static final int AD = 5;
	public static final int STORE_AD = 6;
	public static final int NOTICE = 7;
	public static final int WX_ARTICLE_IMAGES = 8;
	
	public static String getDir(int type){
		String ymd = new SimpleDateFormat("yyyy/MM/dd/").format(new Date());
		switch (type) {
		case GOODS_BASE:
			return "/goods/";
		case GOODS_LOGO:
			return "/goods/"+ymd;
		case GOODS_IMAGES:
			return "/goods/"+ymd;
		case GOODS_DETAIL:
			return "/goods/"+ymd;
		case STORE_LOGO:
			return "/store/"+ymd;
		case AD:
			return "/ad/"+ymd;
		case STORE_AD:
			return "/store/"+ymd;
		case NOTICE:
			return "/notice/"+ymd;
		case WX_ARTICLE_IMAGES:
			return "/wx/"+ymd;
		}
		return ymd;
	}
}
