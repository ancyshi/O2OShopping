package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public class LogKeywordService {
	
	public Page<Record> findByPage(int storied, int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM (SELECT keyword,COUNT(*) amount FROM t_log_keyword A WHERE A.storeid=? GROUP BY keyword) B");
		sqlExcept.append(" ORDER BY B.amount DESC");
		return Db.paginate(pageNum, pageSize, "SELECT B.*", sqlExcept.toString(), storied);
	}
}
