package cn.com.dashihui.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Resource;
import cn.com.dashihui.web.service.ResourceService;

@RequiresAuthentication
public class ResourceController extends BaseController{
	private static final Logger logger = Logger.getLogger(ResourceController.class);
	private ResourceService resourceService = new ResourceService();
    
	//@RequiresPermissions("resource:index")
    public void index(){
    	setAttr("resourceList", resourceService.findAllResources());
        render("index.jsp");
    }
    
    public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		resourceService.sortResource(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }
    
	public void toAdd(){
		int parentid = getParaToInt(0,0);
		if(parentid!=0){
			setAttr("parent", resourceService.findById(parentid));
		}
		keepPara("type");
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：权限名称为空，2：权限名称过长，3：权限代码为空，4：权限代码过长，5：权限代码已经存在，6：权限菜单地址为空，7：权限菜单地址过长
	 */
	public void doAdd(){
		//权限名称
		String name = getPara("name");
		//权限对应的代码
		String code = getPara("code");
		//权限类型，1-一级菜单，2-二级菜单，3-三级操作
		int type = getParaToInt("type",1);
		//菜单类型权限的URL
		String url = getPara("url");
		//上级权限ID
		String parentid = getPara("parentid","0");
		if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>50){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(3);
			return;
		}else if(code.length()>100){
			renderResult(4);
			return;
		}else if(resourceService.findByCode(code)!=null){
			renderResult(5);
			return;
		}else if(type!=1&&StrKit.isBlank(url)){
			renderResult(6);
			return;
		}else if(!StrKit.isBlank(url)&&url.length()>200){
			renderResult(7);
			return;
		}else{
			//保存
			Resource resource = new Resource()
				.set("type", type)
				.set("name", name)
				.set("url", url)
				.set("code", code)
				.set("parentid", parentid);
			if(resourceService.addResource(resource)){
				renderSuccess(resourceService.findById(resource.getInt("id")));
				return;
			}
		}
		renderFailed();
	}
	
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			Resource resource = resourceService.findById(id);
			int parentid = resource.getInt("parentid");
			if(parentid!=0){
				setAttr("parent", resourceService.findById(parentid));
			}
			setAttr("object", resource);
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：权限名称为空，2：权限名称过长，3：权限代码为空，4：权限代码过长，5：权限代码已经存在，6：URL过长
	 */
	public void doEdit(){
		//权限ID
		String resourceid = getPara("resourceid");
		//权限名称
		String name = getPara("name");
		//权限对应的代码
		String code = getPara("code");
		//菜单类型权限的URL
		String url = getPara("url");
		if(StrKit.isBlank(resourceid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>50){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(3);
			return;
		}else if(code.length()>100){
			renderResult(4);
			return;
		}
		Resource findByCode = resourceService.findByCode(code);
		if(findByCode!=null&&findByCode.getInt("id")!=Integer.valueOf(resourceid)){
			renderResult(5);
			return;
		}
		if(!StrKit.isBlank(url)&&url.length()>200){
			renderResult(6);
			return;
		}else{
			//更新
			if(resourceService.editResource(new Resource()
				.set("id", resourceid)
				.set("name", name)
				.set("code", code)
				.set("url", url))){
				renderSuccess(resourceService.findById(Integer.valueOf(resourceid)));
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&resourceService.delResource(id)){
			logger.info("删除权限【"+id+"】，同时将删除其下所有子权限及子操作，以及角色权限关联数据");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
