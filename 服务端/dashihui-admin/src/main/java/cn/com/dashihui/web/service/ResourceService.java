package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.Resource;

public class ResourceService {
	
	public boolean sortResource(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_auth_resources SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public boolean addResource(Resource newObject){
		return newObject.save();
	}
	
	public boolean delResource(int id){
		Resource resource = findById(id);
		List<String> sqlList = new ArrayList<String>();
		//删除相应记录
		sqlList.add("DELETE FROM t_auth_resources WHERE id="+id);
		//同时删除相应的角色权限关联记录
		sqlList.add("DELETE FROM t_auth_role_resources WHERE resourceid="+id);
		if(resource.getInt("type")==2){
			//当前要删除记录如果是二级权限，则需要查询出其三级子权限及三级子权限相应的角色权限关联记录，一并删除
			List<Resource> children = Resource.me().find("SELECT * FROM t_auth_resources WHERE parentid=?",id);
			if(children!=null){
				for(Resource child : children){
					sqlList.add("DELETE FROM t_auth_resources WHERE id="+child.get("id"));
					sqlList.add("DELETE FROM t_auth_role_resources WHERE resourceid="+child.get("id"));
				}
			}
		}else if(resource.getInt("type")==1){
			//当前要删除记录如果是一级权限，则需要查询出其二、三级子权限及二、三级子权限相应的角色权限关联记录，一并删除
			List<Resource> allResource = findAllResources();
			for(Resource item : allResource){
				if(item.getInt("id").intValue()==resource.getInt("id").intValue()){
					List<Resource> children1 = item.getChildren();
					if(children1!=null){
						for(Resource child1 : children1){
							sqlList.add("DELETE FROM t_auth_resources WHERE id="+child1.get("id"));
							sqlList.add("DELETE FROM t_auth_role_resources WHERE resourceid="+child1.get("id"));
							List<Resource> children2 = child1.getChildren();
							if(children2!=null){
								for(Resource child2 : children2){
									sqlList.add("DELETE FROM t_auth_resources WHERE id="+child2.get("id"));
									sqlList.add("DELETE FROM t_auth_role_resources WHERE resourceid="+child2.get("id"));
								}
							}
						}
					}
				}
			}
		}
		Db.batch(sqlList, sqlList.size());
		return true;
	}
	
	public boolean editResource(Resource object){
		return object.update();
	}
	
	public Resource findById(int id){
		return Resource.me().findFirst("SELECT * FROM t_auth_resources WHERE id=?",id);
	}
	
	public Resource findByCode(String code){
		return Resource.me().findFirst("SELECT * FROM t_auth_resources WHERE code=?",code);
	}
	
	public List<Record> findResourcesByUser(int userid){
		return Db.find("SELECT * FROM v_admin_resources WHERE parentid!=0 and userid=?",userid);
	}
	
	public List<Resource> findAllResources(){
		return trim(Resource.me().find("SELECT * FROM t_auth_resources r ORDER BY r.parentid,r.orderNo"));
	}
	
	public List<Resource> findMenuResourcesByUser(int userid){
		return trim(Resource.me().find("SELECT r.* FROM t_auth_resources r inner join v_admin_resources ur on r.id=ur.id WHERE ur.userid=? ORDER BY r.parentid,r.orderNo",userid));
	}
	
	public List<Resource> findResourcesByRole(int roleid){
		return trim(Resource.me().find("SELECT r.*,(CASE WHEN rr.roleid IS NULL THEN false ELSE true END) checked FROM t_auth_resources r left join v_role_resources rr ON r.id=rr.id and rr.roleid=? ORDER BY r.parentid,r.orderNo",roleid));
	}
	
	/**
	 * 将查询出来的权限列表，整理成嵌套形式
	 */
	private List<Resource> trim(List<Resource> resourceList){
		if(resourceList!=null){
			List<Resource> resourceType1List = new ArrayList<Resource>(), resourceType2List = new ArrayList<Resource>(), resourceType3List = new ArrayList<Resource>();
			for(Resource resource : resourceList){
				if(resource.getInt("type")==1){
					resourceType1List.add(resource);
				}else if(resource.getInt("type")==2){
					resourceType2List.add(resource);
				}else if(resource.getInt("type")==3){
					resourceType3List.add(resource);
				}
			}
			if(resourceType3List.size()!=0){
				for(Resource r3 : resourceType3List){
					for(Resource r2 : resourceType2List){
						if(r3.getInt("parentid").intValue()==r2.getInt("id").intValue()){
							r2.addChild(r3);
							break;
						}
					}
				}
			}
			if(resourceType2List.size()!=0){
				for(Resource r2 : resourceType2List){
					for(Resource r1 : resourceType1List){
						if(r2.getInt("parentid").intValue()==r1.getInt("id").intValue()){
							r1.addChild(r2);
							break;
						}
					}
				}
			}
			return resourceType1List;
		}
		return null;
	}
}
