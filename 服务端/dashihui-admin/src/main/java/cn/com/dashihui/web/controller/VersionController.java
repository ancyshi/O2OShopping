package cn.com.dashihui.web.controller;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Version;
import cn.com.dashihui.web.service.VersionService;

@RequiresAuthentication
public class VersionController extends BaseController{
	private VersionService service = new VersionService();
	/**
	 * 版本控制主页面
	 * 2015-10-28
	 */
	public void index(){
		render("index.jsp");
	}
	/**
	 * 页面异步请求获得分页数据
	 * 2015-10-28
	 */
	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		int kind = getParaToInt("kind",0);
		String type = getPara("type","");
		renderResult(0,service.findByPage(pageNum, pageSize,kind, type));
	}
	/**
	 * 转发到版本控制页面不带任何参数
	 * 2015-10-28
	 */
	public void toAdd(){
		render("add.jsp");
	}
	/**
	 * 版本控制页面数据保存
	 * 2015-10-28
	 */
	public void doAdd(){
		//类别
		String type = getPara("type");
		//版本号
		String versionPart1 = getPara("versionPart1");
		String versionPart2 = getPara("versionPart2");
		String versionPart3 = getPara("versionPart3");
		//下载地址
		String downurl = getPara("downurl");
		//升级选项
		String updateFlag = getPara("updateFlag");
		//内容
		String updatelog = getPara("updatelog");
		//发布时间
		String releaseDate = getPara("releaseDate");
		if(StrKit.isBlank(type)){
			renderResult(1);
			return;
		}else if(type.length()>3){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(versionPart1)||StrKit.isBlank(versionPart2)||StrKit.isBlank(versionPart3)){
			renderResult(3);
			return;
		}else if(versionPart1.length()>3||versionPart2.length()>3||versionPart3.length()>5){
			renderResult(4);
			return;
		}else if(StrKit.isBlank(downurl)){
			renderResult(5);
			return;
		}else if(downurl.length()>200){
			renderResult(6);
			return;
		}else if(StrKit.notBlank(updatelog)&&updatelog.length()>200){
			renderResult(7);
			return;
		}else{
			Version versionMode = new Version()
					.set("type", type)
					.set("versionPart1", versionPart1)
					.set("versionPart2", versionPart2)
					.set("versionPart3", versionPart3)
					.set("downurl", downurl)
					.set("updatelog", updatelog);
			if(StrKit.notBlank(updateFlag)){
				versionMode.set("updateFlag", 1);
			}
			if(StrKit.notBlank(releaseDate)){
				versionMode.set("releaseDate", releaseDate);
			}
			//保存到数据库中
			if(service.add(versionMode)){
				renderSuccess();
				return;
			}
		}
		
		renderFailed();
	}
	/**
	 * 转发到版本控制修改页面带有此条数据id
	 * 2015-10-28
	 */
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	/**
	 * 版本控制修改页面数据的修改入库
	 * 2015-10-28
	 */
	public void doEdit(){
		//id
		String versionid = getPara("versionid");
		//类别
		String type = getPara("type");
		//版本号
		String versionPart1 = getPara("versionPart1");
		String versionPart2 = getPara("versionPart2");
		String versionPart3 = getPara("versionPart3");
		//下载地址
		String downurl = getPara("downurl");
		//升级选项
		String updateFlag = getPara("updateFlag");
		//内容
		String updatelog = getPara("updatelog");
		//发布时间
		String releaseDate = getPara("releaseDate");
		if(StrKit.isBlank(versionid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(type)){
			renderResult(1);
			return;
		}else if(type.length()>3){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(versionPart1)||StrKit.isBlank(versionPart2)||StrKit.isBlank(versionPart3)){
			renderResult(3);
			return;
		}else if(versionPart1.length()>3||versionPart2.length()>3||versionPart3.length()>5){
			renderResult(4);
			return;
		}else if(StrKit.isBlank(downurl)){
			renderResult(5);
			return;
		}else if(downurl.length()>200){
			renderResult(6);
			return;
		}else if(StrKit.notBlank(updatelog)&&updatelog.length()>200){
			renderResult(7);
			return;
		}else{
			Version versionMode = new Version()
					.set("id", versionid)
					.set("type", type)
					.set("versionPart1", versionPart1)
					.set("versionPart2", versionPart2)
					.set("versionPart3", versionPart3)
					.set("downurl", downurl)
					.set("updatelog", updatelog);
			if(StrKit.notBlank(updateFlag)){
				versionMode.set("updateFlag", 1);
			}
			if(StrKit.notBlank(releaseDate)){
				versionMode.set("releaseDate", releaseDate);
			}
			if(service.update(versionMode)){
				renderSuccess(service.findById(Integer.valueOf(versionid)));
				return;
			}
		}
		renderFailed();
	}
	/**
	 * 删除指定id的版本控制记录
	 * 2015-10-28
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&service.delete(id)){
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
