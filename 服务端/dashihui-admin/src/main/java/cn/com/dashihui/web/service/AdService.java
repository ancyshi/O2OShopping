package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;

import cn.com.dashihui.web.dao.Ad;

public class AdService {
	
	public boolean sortAd(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_bus_ad SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public List<Ad> findAll(){
		return Ad.me().find("SELECT A.* FROM t_bus_ad A ORDER BY A.orderNo,A.createDate DESC");
	}
	
	public boolean addAd(Ad newObject){
		return newObject.save();
	}
	
	public boolean delAd(int id){
		return Ad.me().deleteById(id);
	}
	
	public boolean editAd(Ad object){
		return object.update();
	}
	
	public Ad findById(int id){
		return Ad.me().findFirst("SELECT * FROM t_bus_ad WHERE id=?",id);
	}
}
