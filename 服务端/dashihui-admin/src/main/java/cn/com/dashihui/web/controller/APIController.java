package cn.com.dashihui.web.controller;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.APIService;

public class APIController extends BaseController{
	private APIService service = new APIService();
	
	public void brand(){
		renderText(JsonKit.toJson(service.getAllBrand()));
	}
	
	public void province(){
		renderText(JsonKit.toJson(service.getAllProvince()));
	}
	
	public void city(){
		int parentid = getParaToInt(0);
		renderText(JsonKit.toJson(service.getAllCityByProvince(parentid)));
	}
	
	public void area(){
		int parentid = getParaToInt(0);
		renderText(JsonKit.toJson(service.getAllAreaByCity(parentid)));
	}
	
	public void category(){
		String parentidStr = getPara(0);
		int parentid = StrKit.isBlank(parentidStr)?0:Integer.valueOf(parentidStr);
		renderText(JsonKit.toJson(service.getCategoryByParent(parentid)));
	}
	
	public void storeAll(){
		renderText(JsonKit.toJson(service.getStoreAll()));
	}
	
	public void logistics(){
		renderText(JsonKit.toJson(service.getLogistics()));
	}
}
