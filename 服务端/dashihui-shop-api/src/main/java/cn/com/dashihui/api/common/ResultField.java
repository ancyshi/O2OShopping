package cn.com.dashihui.api.common;

public class ResultField {
	/*基础属性*/
	public static final String FIELD_STATE = "STATE";
	public static final String FIELD_MSG = "MSG";
	public static final String FIELD_OBJECT = "OBJECT";
	
	/*手机终端签名*/
	public static final String FIELD_CLIENT_TOKEN = "SIGNATURE";
	/*用户登录签名*/
	public static final String FIELD_USER_LOGIN_TOKEN = "TOKEN";
}
