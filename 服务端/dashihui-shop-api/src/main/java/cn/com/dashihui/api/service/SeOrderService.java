package cn.com.dashihui.api.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.api.common.SerOrderCode;
import cn.com.dashihui.api.dao.SerOrder;

public class SeOrderService {
	
	/**
	 * 记录操作日志
	 */
	public void log(String orderNum, String user, String action, String content){
		Db.update("INSERT INTO t_bus_ser_order_log(orderNum,user,action,content) VALUES(?,?,?,?)",orderNum,user,action,content);
	}
	
	/**
	 * 根据订单编号查询出订单
	 */
	public SerOrder getOrderByOrderNum(String orderNum,int proSerid){
		return SerOrder.me().findFirst("SELECT bso.*,bss.tel FROM t_bus_ser_order bso LEFT JOIN t_bus_ser_shop bss ON bso.proSerid=bss.id WHERE orderNum=? AND proSerid=? AND orderState !=?",orderNum,proSerid,SerOrderCode.OrderState.DELETE);
	}
	
	/**
	 * 分页查找出所需订单
	 * @param shopid 商家ID
	 * @param startDate 查询的起始时间
	 * @param flag 0：全部，1：待接单，2：待服务，3：已完成
	 * @param direction 1：上拉加载旧数据，2：下拉加载新数据 
	 */
	public Page<Record> findByPage(int shopid,String startDate,int flag,int direction,int pageNum,int pageSize){
		if(flag==1){
			//1:待接单
			StringBuffer sqlBuffer = new StringBuffer();
			sqlBuffer.append("FROM t_bus_ser_order WHERE proSerid=? AND orderState=? AND deliverState=?");
			if(direction==1){
				sqlBuffer.append(" AND storeDeliverDate<? ORDER BY storeDeliverDate DESC");
			}else{
				sqlBuffer.append(" AND storeDeliverDate>? ORDER BY storeDeliverDate DESC");
			}
			return Db.paginate(pageNum, pageSize, "SELECT *  ",sqlBuffer.toString(),shopid,SerOrderCode.OrderState.NORMAL,SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH,startDate);
		}else if(flag==2){
			//2：待服务
			StringBuffer sqlBuffer = new StringBuffer();
			sqlBuffer.append("FROM t_bus_ser_order WHERE proSerid=? AND orderState=? AND deliverState=?");
			if(direction==1){
				sqlBuffer.append(" AND proSerDeliverDate<? ORDER BY proSerDeliverDate DESC");
			}else{
				sqlBuffer.append(" AND proSerDeliverDate>? ORDER BY proSerDeliverDate DESC");
			}
			return Db.paginate(pageNum, pageSize, "SELECT * ",sqlBuffer.toString(),shopid,SerOrderCode.OrderState.NORMAL,SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT,startDate);
		}else if(flag==3){
			//3：已完成
			StringBuffer sqlBuffer = new StringBuffer();
			sqlBuffer.append("FROM t_bus_ser_order WHERE proSerid=? AND orderState=? AND deliverState=?");
			if(direction==1){
				sqlBuffer.append(" AND signDate<? ORDER BY signDate DESC");
			}else{
				sqlBuffer.append(" AND signDate>? ORDER BY signDate DESC");
			}
			return Db.paginate(pageNum, pageSize, "SELECT * ",sqlBuffer.toString(),shopid,SerOrderCode.OrderState.FINISH,SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT,startDate);
		}else{
			//0：全部订单
			StringBuffer sqlBuffer = new StringBuffer();
			sqlBuffer.append("FROM t_bus_ser_order WHERE proSerid=? AND (orderState=? OR orderState=?) AND deliverState=?");
			if(direction==1){
				sqlBuffer.append(" AND proSerDeliverDate<? ORDER BY proSerDeliverDate DESC");
			}else{
				sqlBuffer.append(" AND proSerDeliverDate>? ORDER BY proSerDeliverDate DESC");
			}
			return Db.paginate(pageNum, pageSize, "SELECT * ",sqlBuffer.toString(),shopid,SerOrderCode.OrderState.NORMAL,SerOrderCode.OrderState.FINISH,SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT,startDate);
		}
	}
	
	/**
	 * 今日订单总数
	 */
	public long getOrderCount(int id,String today){
		String sql = "SELECT count(*) FROM t_bus_ser_order WHERE proSerid=? AND proSerDeliverDate>=? AND deliverState=?";
		return Db.queryLong(sql, id,today,SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT);
	}
	
	/**
	 * 今日营业额
	 */
	public double getOrderAmount(int id,String today){
		String sql = "SELECT IFNULL(sum(amount),0) AS amount  FROM t_bus_ser_order WHERE proSerid=? AND proSerDeliverDate>=? AND deliverState=? AND orderState=?";
		return Db.queryDouble(sql, id,today,SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT,SerOrderCode.OrderState.FINISH);
		
	}
	
}
