package cn.com.dashihui.api.service;

import com.jfinal.plugin.activerecord.Db;

import cn.com.dashihui.api.dao.SerFeedback;
import cn.com.dashihui.api.dao.SerShop;

public class SerShopService{
	
	/**
	 * 根据用户查询出店铺
	 */
	public SerShop getShopByPhone(String phone){
		return SerShop.me().findFirst("SELECT * FROM t_bus_ser_shop WHERE username=? ",phone);
	}
	
	/**
	 * 根据ID查询出店铺
	 */
	public SerShop getShopById(int id){
		return SerShop.me().findFirst("SELECT * FROM t_bus_ser_shop  WHERE id=? ",id);
	}
	
	/**
	 * 改变商架的营业状态
	 */
	public boolean changeWork(int id,int flag){
		return Db.update("UPDATE t_bus_ser_shop SET isWork =? WHERE id=? ",flag,id)>0;
	}
    
	/**
	 * 修改商家登录密码
	 */
	public boolean updateShopPwd(String phone,String pwd){
		return Db.update("UPDATE t_bus_ser_shop SET password =? WHERE username=? ",pwd,phone)>0;
	}
	
	/**
	 * 保存商家反馈信息
	 */
	public boolean saveFeedback(SerFeedback feedback){
		return feedback.save();
	}
}
