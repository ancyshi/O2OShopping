package cn.com.dashihui.wx.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.wx.common.OrderCode;
import cn.com.dashihui.wx.dao.Feedback;
import cn.com.dashihui.wx.dao.Store;
import cn.com.dashihui.wx.dao.User;
import cn.com.dashihui.wx.dao.UserAddress;
import cn.com.dashihui.wx.dao.UserCollection;
import cn.com.dashihui.wx.kit.InviteCodeKit;

public class UserService {
	
	/**
	 * 查询指定用户名的用户信息，用于登录密码验证或验证指定用户名用户是否存在，所以只查询用户名和密码字段
	 */
	public User findByUsername(String username){
		return User.me().findFirst("SELECT * FROM t_bus_user WHERE username=?",username);
	}
	
	/**
	 * 查询指定用户ID的用户信息
	 */
	public User findByUserid(int userid){
		return User.me().findFirst("SELECT * FROM t_bus_user WHERE id=?",userid);
	}
	
	/**
	 * 查询指定邀请码的用户信息
	 */
	public User findByUserInviteCode(String inviteCode){
		//将用户邀请码反向转换为用户ID
		int userid = Long.valueOf(InviteCodeKit.codeToId(inviteCode)).intValue();
		return User.me().findFirst("SELECT * FROM t_bus_user WHERE id=? AND inviteCode=?",userid,inviteCode);
	}
	
	/**
	 * 用户注册
	 */
	public boolean save(User user){
		return user.save();
	}
	
	/**
	 * 用户信息更新
	 */
	public boolean update(User user){
		return user.update();
	}
	
	/**
	 * 解除微信绑定
	 */
	public boolean unbind(int userid, int wxUserid){
		return Db.update("UPDATE t_bus_user SET wxUserid=null WHERE id=? AND wxUserid=?",userid,wxUserid)==1;
	}
	
	/**
	 * 统计用户各状态数量
	 * 1.统计用户收藏的商品数量
	 * 2.统计查询“待付款”的订单数量
	 * 3.统计查询“待发货”的订单数量
	 * 4.统计查询“待签收”的订单数量
	 */
	public Record getUserState(int storeid,int userid){
		StringBuffer sqlBuffer = new StringBuffer("SELECT ");
		List<Object> sqlParams = new ArrayList<Object>();
		//1.统计用户收藏的商品数量
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_user_collection buc WHERE buc.userid=? AND (buc.storeid=? OR buc.isSelf=1) AND buc.isCancel=0) state1,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		
		//2.统计“待付款”的商品订单数量（在线支付、未付款、正常）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND (storeid=? OR isSelf=1) AND payType=? AND payState=? AND orderState=?) state2,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);
		sqlParams.add(OrderCode.OrderPayState.NO_PAY);
		sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//3.统计查询“待收货”的订单数量（1：“送货上门”、“已发货”、“正常”，2：“在线支付+门店自取”、“已支付”、“正常”，3：“货到付款+门店自取”、“正常”）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND (storeid=? OR isSelf=1)");
		sqlBuffer.append(" AND ");
		sqlBuffer.append("((payType=? AND payState=?) OR payType=?)");
		sqlBuffer.append(" AND orderState=?) state3,");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(OrderCode.OrderPayType.ON_LINE);
		sqlParams.add(OrderCode.OrderPayState.HAD_PAY);
		sqlParams.add(OrderCode.OrderPayType.ON_DELIVERY);
		sqlParams.add(OrderCode.OrderState.NORMAL);
		
		//4.统计查询“待评价”的订单数量（“已完成”、“待评价”）
		sqlBuffer.append("(SELECT COUNT(*) FROM t_bus_order WHERE userid=? AND (storeid=? OR isSelf=1) AND orderState=? AND evalState=0) state4");
		sqlParams.add(userid);
		sqlParams.add(storeid);
		sqlParams.add(OrderCode.OrderState.FINISH);
		return Db.findFirst(sqlBuffer.toString(),sqlParams.toArray());
	}

	/**
	 * 更新用户登录密码
	 */
	public boolean updatePwdByUsername(String password,String username){
		String sql = "UPDATE t_bus_user SET password=? WHERE username=?";
		return Db.update(sql, password, username)==1;
	}

	/**
	 * 用户收藏指定商品<br/>
	 * 因为收藏表中记录为了统计，所以用户取消收藏是不会将记录删除的，只是修改了标识isCancel为1<br/>
	 * 所以在此业务中需要判断同一用户同一商品是否已经存在，如果存在且是取消收藏状态，则只需要修改标识为0即可<br/>
	 * 如果不存在，则需要新插入记录<br/>
	 * 否则认为同一用户已经收藏过同一商品，直接返回true
	 */
	@Before(Tx.class)
	public UserCollection doCollect(int storeid,int isSelf,int userid,String goodsid){
		UserCollection c = UserCollection.me().findFirst("SELECT * FROM t_bus_user_collection WHERE userid=? AND (storeid=? OR isSelf=1) AND goodsid=?",userid,storeid,goodsid);
		if(c!=null){
			if(c.getInt("isCancel")==0){
				c.set("isCancel", 1).update();
			}else{
				c.set("isCancel", 0).update();
			}
		}else{
			c = new UserCollection();
			c.set("userid", userid).set("storeid", storeid).set("goodsid", goodsid).set("isSelf", isSelf).save();
		}
		return c;
	}
	
	/**
	 * 用户取消收藏指定商品<br/>
	 */
	public void cancelCollect(int userid,String[] goodsids){
		String sql = "UPDATE t_bus_user_collection SET isCancel=1 WHERE userid=? AND goodsid=?";
		Object[][] params = new Object[goodsids.length][2];
		for(int index = 0;index<goodsids.length;index++){
			params[index][0] = userid;
			params[index][1] = goodsids[index];
		}
		Db.batch(sql, params, goodsids.length);
	}
	
	/**
	 * 查询用户收藏商品列表
	 * @param storeid 店铺ID
	 * @param categoryCode 主分类代码
	 * @param type 优惠类型
	 * @param pageNum 页码
	 * @param pageSize 数量
	 */
	public Page<Record> findCollectionByPage(int storeid,int userid,int pageNum,int pageSize){
		String sqlSelect = "SELECT coled.* ";
		String sqlExceptSelect = "FROM (SELECT bg.id,bg.name,bg.spec,bg.marketPrice,bg.sellPrice,bg.thumb,bg.type,bg.urv,bg.isSelf,uc.createDate collectDate"
					+ " FROM t_bus_user_collection uc"
					+ " INNER JOIN t_bus_goods bg ON uc.goodsid=bg.id"
					+ " WHERE uc.userid=? AND (uc.storeid=? OR uc.isSelf=1) AND uc.isCancel=0) coled ORDER BY coled.collectDate DESC";
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlExceptSelect, userid, storeid);
	}
	
	/**
	 * 查询用户收货地址列表
	 * @param userid 用户ID
	 */
	public List<UserAddress> findAddress(int userid){
		return UserAddress.me().find("SELECT ua.id,ua.linkName,ua.sex,ua.tel,"
				+ "ua.storeid,"
				+ "store.title storeTitle,"
				+ "ua.address,"
				+ "ua.isDefault"
				+ " FROM t_bus_user_address ua"
				+ " LEFT JOIN t_dict_store store ON ua.storeid=store.id"
				+ " WHERE ua.userid=?",userid);
	}
	
	/**
	 * 添加收货地址<br/>
	 * 1.如果本次添加的收货地址，要求设置为默认，则更新该用户的其他收货地址为非默认（在此不再查询用户是否还有其他收货地址，直接UPDATE）
	 * 2.如果本次添加的收货地址，没有要求设置为默认，则查询该用户是否还有其他收货地址，如果没有，则同样设置本次为默认（因为只有一条收货地址时，应当设置为默认）
	 */
	@Before(Tx.class)
	public boolean addAddress(int userid,UserAddress address,int isDefault){
		address.set("isDefault", 0);
		if(isDefault==1){
			Db.update("UPDATE t_bus_user_address SET isDefault=0 WHERE userid=?",userid);
			address.set("isDefault", 1);
		}else{
			Record addrCount = Db.findFirst("SELECT count(*) total FROM t_bus_user_address WHERE userid=?",userid);
			if(addrCount.getLong("total").intValue()==0){
				address.set("isDefault", 1);
			}
		}
		return address.save();
	}
	
	/**
	 * 删除收货地址
	 */
	public boolean delAddress(int userid,String addressid){
		return Db.update("DELETE FROM t_bus_user_address WHERE userid=? AND id=?", userid, addressid)==1;
	}
	
	/**
	 * 修改收货地址<br/>
	 * 如果本次添加的收货地址，要求设置为默认，则更新该用户的其他收货地址为非默认（在此不再查询用户是否还有其他收货地址，直接UPDATE）
	 */
	public boolean updateAddress(int userid,UserAddress address,int isDefault){
		address.set("isDefault", 0);
		if(isDefault==1){
			Db.update("UPDATE t_bus_user_address SET isDefault=0 WHERE userid=?",userid);
			address.set("isDefault", 1);
		}
		return address.update();
	}
	
	/**
	 * 修改收货地址为默认
	 */
	public boolean setAddressDefault(int userid,String addressid,int isDefault){
		if(isDefault==1
				&&Db.update("UPDATE t_bus_user_address SET isDefault=0 WHERE userid=?",userid)>=0
				&&Db.update("UPDATE t_bus_user_address SET isDefault=1 WHERE id=?",addressid)==1){
			return true;
		}
		return false;
	}
	
	/**
	 * 查找指定ID的收货地址信息
	 */
	public UserAddress findAddressById(int addressid){
		return UserAddress.me().findFirst("SELECT ua.id,ua.linkName,ua.sex,ua.tel,"
				+ "ua.storeid,"
				+ "store.title storeTitle,"
				+ "ua.address,"
				+ "ua.isDefault"
				+ " FROM t_bus_user_address ua"
				+ " LEFT JOIN t_dict_store store ON ua.storeid=store.id"
				+ " WHERE ua.id=?",addressid);
	}
	
	/**
	 * 查找所有的收货地址信息
	 */
	public List<UserAddress> findAddressList(int userid){
		return UserAddress.me().find("SELECT ua.id,ua.linkName,ua.sex,ua.tel,"
				+ "ua.storeid,"
				+ "store.title storeTitle,"
				+ "ua.address,"
				+ "ua.isDefault"
				+ " FROM t_bus_user_address ua"
				+ " LEFT JOIN t_dict_store store ON ua.storeid=store.id"
				+ " WHERE ua.userid=?",userid);
	}
	
	/**
	 * 查找指定用户的默认收货地址信息
	 */
	public UserAddress findDefaultAddressByUser(int userid){
		return UserAddress.me().findFirst("SELECT ua.id,ua.linkName,ua.sex,ua.tel,"
				+ "ua.storeid,"
				+ "store.title storeTitle,"
				+ "ua.address,"
				+ "ua.isDefault"
				+ " FROM t_bus_user_address ua"
				+ " LEFT JOIN t_dict_store store ON ua.storeid=store.id"
				+ " WHERE ua.isDefault=1 AND ua.userid=?",userid);
	}
	
	/**
	 * 查询指定ID的店铺信息
	 */
	public Store findStoreById(int storeid){
		String sql = "SELECT * FROM t_dict_store ds"
				+ " LEFT JOIN"
				+ " (SELECT"
				+ " OE.storeid,"
				+ " CEIL(SUM(OE.eval1)/COUNT(OE.eval1)) eval1,"
				+ " CEIL(SUM(OE.eval2)/COUNT(OE.eval2)) eval2,"
				+ " CEIL(SUM(OE.eval3)/COUNT(OE.eval3)) eval3"
				+ " FROM (SELECT boe.eval1,boe.eval2,boe.eval3,bo.storeid FROM t_bus_order_eval boe INNER JOIN t_bus_order bo ON boe.orderNum=bo.orderNum WHERE bo.storeid=?) OE"
				+ " GROUP BY OE.storeid) OER ON OER.storeid=ds.id"
				+ " WHERE ds.id=?";
		return Store.me().findFirst(sql,storeid,storeid);
	}
	
	/**
	 * 记录意见反馈内容
	 */
	public boolean feedback(String context,String contact, int userid){
		return new Feedback().set("userid", userid).set("context", context).set("contact", contact).save();
	}
	
	/**
	 * 查询用户实惠币变动记录
	 * @param pageNum
	 * @param pageSize
	 * @param userid
	 * @param searchType 1:我的购物收益明细; 2:好友购物收益明细; 3:我的消费记录; 4:好友购物收益合计
	 * @return
	 */
	public Page<Record> findUserExpenseRecord(int pageNum, int pageSize, int userid,int searchType){
		String sqlSelect = "SELECT buvl.* ";
		StringBuffer sqlExceptSelect = new StringBuffer("FROM t_bus_user_value_log buvl ");
		if(searchType == 1){
			sqlExceptSelect.append("WHERE buvl.userid=? AND buvl.flag=1 ");
			sqlExceptSelect.append("ORDER BY createDate DESC");
		} else if(searchType == 2){
			sqlSelect = "SELECT buvl.*,bu.nickName";
			sqlExceptSelect.append("LEFT JOIN t_bus_user bu ON buvl.fromUserid = bu.id ");
			sqlExceptSelect.append("WHERE buvl.userid=? AND buvl.flag=2 ");
			sqlExceptSelect.append("ORDER BY createDate DESC");
		} else if(searchType == 3){
			sqlExceptSelect.append("WHERE buvl.userid=? AND buvl.flag=3 ");
			sqlExceptSelect.append("ORDER BY createDate DESC");
		} else if(searchType == 4){
			sqlSelect = "SELECT g.nickName,SUM(g.amount) amount,g.fromUserid,g.createDate ";
			sqlExceptSelect.delete(0,sqlExceptSelect.length());
			sqlExceptSelect.append("FROM ( SELECT buvl.id,buvl.userid,buvl.flag,buvl.amount,buvl.fromUserid,buvl.createDate,bu.nickName ");
			sqlExceptSelect.append("FROM t_bus_user_value_log buvl INNER JOIN t_bus_user bu ");
			sqlExceptSelect.append("ON buvl.fromUserid = bu.id ");
			sqlExceptSelect.append("WHERE bu.inviteUserid = ? AND buvl.flag = 2 ) g ");
			sqlExceptSelect.append("GROUP BY g.nickName,g.fromUserid ");
			sqlExceptSelect.append("ORDER BY g.createDate DESC");
		}
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlExceptSelect.toString(), userid);
	}
	
	/**
	 * 查询用户推荐的好友的数量
	 * @param userid
	 * @return
	 */
	public Record findInviteUserCount(int userid){
		String sql = "SELECT count(1) AS inviteCount FROM t_bus_user WHERE inviteUserid = ?";
		return Db.findFirst(sql,userid);
	}
}
