package cn.com.dashihui.wx;

import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.collect.Maps;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.ApiConfigKit;

import cn.com.dashihui.wx.controller.CartController;
import cn.com.dashihui.wx.controller.GoodsController;
import cn.com.dashihui.wx.controller.OrderController;
import cn.com.dashihui.wx.controller.OrderPayNotifyController;
import cn.com.dashihui.wx.controller.SearchController;
import cn.com.dashihui.wx.controller.SystemController;
import cn.com.dashihui.wx.controller.UserController;
import cn.com.dashihui.wx.dao.Category;
import cn.com.dashihui.wx.dao.Community;
import cn.com.dashihui.wx.dao.CommunityDetail;
import cn.com.dashihui.wx.dao.Feedback;
import cn.com.dashihui.wx.dao.Goods;
import cn.com.dashihui.wx.dao.Keyword;
import cn.com.dashihui.wx.dao.Order;
import cn.com.dashihui.wx.dao.OrderList;
import cn.com.dashihui.wx.dao.OrderPayAPIRecord;
import cn.com.dashihui.wx.dao.OrderRefund;
import cn.com.dashihui.wx.dao.Store;
import cn.com.dashihui.wx.dao.StoreAd;
import cn.com.dashihui.wx.dao.User;
import cn.com.dashihui.wx.dao.UserAddress;
import cn.com.dashihui.wx.dao.UserCollection;
import cn.com.dashihui.wx.dao.WxUser;
import cn.com.dashihui.wx.handler.ContextParamsHandler;
import cn.com.dashihui.wx.interceptor.AuthLocationInterceptor;
import cn.com.dashihui.wx.interceptor.OAuthInterceptor;
import cn.com.dashihui.wx.interceptor.TestInterceptor;

public class Config extends JFinalConfig {
	private Logger logger = Logger.getLogger(getClass());
	
	public void configConstant(Constants me) {
		String config = "config.properties";
		// 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
		loadPropertyFile(config);
		//装载进PropKit中
		PropKit.use(config);
		//设置视图根目录
		me.setBaseViewPath("/WEB-INF/page");
	    //设置字符集
	    me.setEncoding("UTF-8");
	    me.setViewType(ViewType.JSP);
	    //调试模式，会打印详细日志
		me.setDevMode(getPropertyToBoolean("constants.devMode", false));
		//出错跳转页面
		me.setError401View("/WEB-INF/page/401.jsp");
		me.setError403View("/WEB-INF/page/403.jsp");
		me.setError404View("/WEB-INF/page/404.jsp");
		me.setError500View("/WEB-INF/page/500.jsp");
	}
	
	public void configRoute(Routes me) {
		me.add("/", SystemController.class);
		me.add("/goods", GoodsController.class);
		me.add("/search", SearchController.class);
		me.add("/my", UserController.class);
		me.add("/cart", CartController.class);
		me.add("/order", OrderController.class);
		me.add("/notify", OrderPayNotifyController.class);
	}
	
	public void configPlugin(Plugins me) {
		//缓存
		EhCachePlugin ecp = new EhCachePlugin();
		me.add(ecp);
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("db.jdbcUrl"), getProperty("db.username"), getProperty("db.password"), getProperty("db.jdbcDriver"));
		c3p0Plugin.setMaxPoolSize(getPropertyToInt("db.maxPoolSize"));
		c3p0Plugin.setMinPoolSize(getPropertyToInt("db.minPoolSize"));
		c3p0Plugin.setInitialPoolSize(getPropertyToInt("db.initialPoolSize"));
		c3p0Plugin.setMaxIdleTime(getPropertyToInt("db.maxIdleTime"));
		c3p0Plugin.setAcquireIncrement(getPropertyToInt("db.acquireIncrement"));
		me.add(c3p0Plugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setDialect(new MysqlDialect());
		//忽略大小写
		//arp.setContainerFactory(new CaseInsensitiveContainerFactory());
		arp.setShowSql(true);
		me.add(arp);
		//添加model映射
		//社区及社区详情系列
		arp.addMapping("t_dict_community", Community.class);
		arp.addMapping("t_dict_community_detail", CommunityDetail.class);
		//店铺表
		arp.addMapping("t_dict_store", Store.class);
		//搜索关键字
		arp.addMapping("t_dict_keyword", Keyword.class);
		//用户表
		arp.addMapping("t_bus_user", User.class);
		arp.addMapping("t_bus_user_wx", WxUser.class);
		//用户收藏表
		arp.addMapping("t_bus_user_collection", UserCollection.class);
		//用户收货地址
		arp.addMapping("t_bus_user_address", UserAddress.class);
		//商品分类
		arp.addMapping("t_dict_category", Category.class);
		//店铺商品表
		arp.addMapping("t_bus_goods", Goods.class);
		//店铺轮播图
		arp.addMapping("t_bus_store_ad", StoreAd.class);
		//商品订单系列
		arp.addMapping("t_bus_order", Order.class);
		arp.addMapping("t_bus_order_apirecord", OrderPayAPIRecord.class);
		arp.addMapping("t_bus_order_refund", OrderRefund.class);
		arp.addMapping("t_bus_order_list", OrderList.class);
		//建议或反馈
		arp.addMapping("t_sys_feedback", Feedback.class);
	}
	
	public void configInterceptor(Interceptors me) {
		me.add(new SessionInViewInterceptor());
		if(getPropertyToBoolean("constants.devMode", false)){
			//测试用，默认初始化登录信息
			me.add(new TestInterceptor());
		}else{
			//微信授权拦截器
			//注：拦截器放在这里，是防止session在30分钟后失效时，用户可能停留在任意页面
			me.add(new OAuthInterceptor());
			//拦截判断用户是否定位或选择过店铺，如果没有，则跳转至定位页面
			me.add(new AuthLocationInterceptor());
		}
	}
	
	public void configHandler(Handlers me) {
		//可在此设置context_path，解决http://ip:port/context_path的问题
		//因为测试时是在jetty下，所以默认没有context_path，如果部署在tomcat下，会自动加上项目名，所以会用到该配置
		//可自定义context_path，默认下是CONTEXT_PATH，使用如：${CONTEXT_PATH}
		me.add(new ContextPathHandler("BASE_PATH"));
		//需要在页面传递的常量
		Map<String,Object> params = Maps.newHashMap();
		params.put("FTP_PATH", PropKit.get("constants.ftppath"));
		me.add(new ContextParamsHandler(params));
	}
	
	@Override
	public void afterJFinalStart() {
		super.afterJFinalStart();
		logger.error("初始化微信api参数");
		//初始化时缓存公众号参数
		ApiConfig ac = new ApiConfig();
		// 配置微信 API 相关常量
		ac.setToken(PropKit.get("wx.token"));
		ac.setAppId(PropKit.get("wx.appId"));
		ac.setAppSecret(PropKit.get("wx.appSecret"));
		/**
		 *  是否对消息进行加密，对应于微信平台的消息加解密方式：
		 *  1：true进行加密且必须配置 encodingAesKey
		 *  2：false采用明文模式，同时也支持混合模式
		 */
		//ac.setEncryptMessage(PropKit.getBoolean("encryptMessage", false));
		//ac.setEncodingAesKey(PropKit.get("encodingAesKey", "setting it in config file"));
		// 将 ApiConfig 对象与当前线程绑定，以便在后续操作中方便获取该对象： ApiConfigKit.getApiConfig();
		ApiConfigKit.setDevMode(PropKit.getBoolean("constants.devMode", false));
		ApiConfigKit.setApiConfig(ac);
	}
}
