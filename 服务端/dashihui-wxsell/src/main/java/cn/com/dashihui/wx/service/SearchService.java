package cn.com.dashihui.wx.service;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;

import cn.com.dashihui.wx.dao.Keyword;

public class SearchService {
	
	/**
	 * 查询后台设置的指定数量的搜索关键字
	 */
	public List<Keyword> findAllKeyword(int storeid, int size){
		return Keyword.me().find("SELECT id,keyword FROM t_dict_keyword WHERE storeid=? ORDER BY orderNo LIMIT ?",storeid,size);
	}
	
	/**
	 * 根据关键字ID，更新该关键字搜索量加1
	 * @param keywordid
	 */
	public void updateKeywordAmount(int keywordid){
		Db.update("UPDATE t_dict_keyword SET amount=amount+1 WHERE id=?",keywordid);
	}
	
	/**
	 * 记录用户自定义的关键字
	 * @param keyword
	 */
	public void logKeyword(int storeid, int userid, String keyword){
		Db.update("INSERT INTO t_log_keyword(storeid,userid,keyword) VALUES(?,?,?)",storeid,userid,keyword);
	}
}
