package cn.com.dashihui.wx.controller;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.wx.dao.Goods;
import cn.com.dashihui.wx.kit.MapKit;
import cn.com.dashihui.wx.service.GoodsService;

public class GoodsController extends BaseController{
	private GoodsService service = new GoodsService();
	
	/**
	 * 商品列表
	 */
	public void index(){
		//一级分类代码
		String codeStr = getPara("code");
		int isSelf = getParaToInt("isSelf",0);
		if(!StrKit.isBlank(codeStr)){
			setAttr("code", codeStr);
		}
		//查询商品分类列表
		setAttr("categoryList", service.findAllCategory(getCurrentStoreid(),isSelf));
		render("index.jsp");
	}
	
	/**
	 * 异步查询商品分页列表
	 * @param conCode 一级类别代码
	 * @param ctwCode 二级类别代码
	 * @param type 优惠类型，1：普通，2：推荐，3：限量，4：一元购
	 * @param orderBy 排序，1：默认排序，2：销量从高到低，3：价格从低到高，4：价格从高到低
	 * @param pageNum 页码
	 * @param pageSize 数量
	 */
	public void page(){
		String conCode = getPara("conCode");
		String ctwCode = getPara("ctwCode");
		int orderBy = getParaToInt("orderBy",1);
		int isSelf = getParaToInt("isSelf",0);
		int pageNum = getParaToInt("pageNum",1);
		int pageSize = getParaToInt("pageSize",PropKit.getInt("constants.pageSize"));
		renderSuccess(service.findByPage(getCurrentStoreid(),conCode,ctwCode,isSelf,pageNum,pageSize,orderBy));
	}
	
	/**
	 * 商品按标签查询页面
	 */
	public void tag(){
		//标签代码
		String codeStr = getPara(0);
		if(!StrKit.isBlank(codeStr)){
			Record tag = service.getTagByCode(codeStr,getCurrentStoreid());
			if(tag!=null){
				setAttr("tag", tag);
				render("tag.jsp");
				return;
			}
		}
		redirect("/index");
	}
	
	/**
	 * 按标签查询商品列表
	 * @param code 标签代码
	 * @param pageNum 页码
	 * @param pageSize 数量
	 */
	public void pageByTag(){
		String tagCode = getPara("code");
		int pageNum = getParaToInt("pageNum",1);
		int pageSize = getParaToInt("pageSize",PropKit.getInt("constants.pageSize"));
		if(StrKit.isBlank(tagCode)){
			renderFailed("参数TAGCODE不能为空");
    		return;
		}
		renderSuccess(service.findPageByTag(getCurrentStoreid(), tagCode, pageNum, pageSize));
	}
	
	/**
	 * 查询精品推荐商品列表
	 * @param pageNum 页码
	 * @param pageSize 数量
	 */
	public void pageByRecom(){
		int pageNum = getParaToInt("pageNum",1);
		int pageSize = getParaToInt("pageSize",PropKit.getInt("constants.pageSize"));
		renderSuccess(service.findPageByRecom(getCurrentStoreid(), pageNum, pageSize));
	}
	
	/**
	 * 商品详情
	 */
	public void detail(){
		String goodsidStr = getPara(0,null);
		if(StrKit.isBlank(goodsidStr)){
			redirect("/index");
    		return;
		}else{
			//查询商品详情信息
			Goods goods;
			if(getCurrentUser()!=null){
				//如果当前有用户登录，则查询商品详情的同时，查询该用户与该商品的收藏关系
				goods = service.findDetail(getCurrentUserid(),getCurrentStoreid(), Integer.valueOf(goodsidStr));
			}else{
				//如果当前没有用户登录，则只查询商品详情
				goods = service.findDetail(getCurrentStoreid(), Integer.valueOf(goodsidStr));
			}
			//设置给商品详情信息
			if(goods!=null){
				goods.put("images", service.findImages(Integer.valueOf(goodsidStr)));
				setAttr("goods", goods);
				render("detail.jsp");
			}else{
				redirect("/index");
	    		return;
			}
		}
	}
	
	/**
	 * 商品详情描述
	 */
	public void describe(){
		String goodsidStr = getPara(0,null);
		if(StrKit.isBlank(goodsidStr)){
			renderFailed();
			return;
		}
		Goods goods = service.findDescribe(getCurrentStoreid(), Integer.valueOf(goodsidStr));
		if(goods!=null&&!StrKit.isBlank(goods.getStr("describe"))){
			renderSuccess(MapKit.one().put("describe", goods.getStr("describe")).getAttrs());
			return;
		}
		renderFailed();
		return;
	}
}
