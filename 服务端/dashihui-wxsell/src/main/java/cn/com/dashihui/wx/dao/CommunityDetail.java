package cn.com.dashihui.wx.dao;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class CommunityDetail extends Model<CommunityDetail>{
	private static final long serialVersionUID = 1L;
	private static CommunityDetail me = new CommunityDetail();
	public static CommunityDetail me(){
		return me;
	}
	
	//下级（单元或楼层或房间）
	private boolean hasChildren = false;
	public List<CommunityDetail> getChildren() {
		return get("children");
	}
	public void setChildren(List<CommunityDetail> children) {
		put("children", children);
		hasChildren = true;
	}
	public boolean hasChildren(){
		return hasChildren;
	}
	public void addChild(CommunityDetail child){
		List<CommunityDetail> children = getChildren();
		if(children==null){
			children = new ArrayList<CommunityDetail>();
		}
		children.add(child);
		setChildren(children);
		hasChildren = true;
	}
}
