package cn.com.dashihui.wx.common;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 系统常量获取<br/>
 * 从数据库t_sys_config表获取
 */
public class SysConfig {
	private static final String LEVEL2_LINE_CODE = "S0001";
	
	/**
	 * 获取普通用户升级为黄金会员时要满足的消费金额线
	 */
	public static double getLevel2Line() throws Exception{
		Record config = Db.findFirst("SELECT value FROM t_sys_config WHERE code=?",LEVEL2_LINE_CODE);
		if(config!=null){
			return Double.valueOf(config.getStr("value"));
		}
		throw new Exception("系统配置常量（代码："+LEVEL2_LINE_CODE+"）对应的值找不到!");
	}
}
