<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>我要分享</title>
		<style>
			*{margin:0;padding:0}
			body,html{font-size:100%;margin:0;padding:0;font-family:"Hiragino Sans GB","Microsoft YaHei","WenQuanYi Micro Hei",sans-serif;}
			img{width: 100%;margin:0;padding:0;height: auto;}
			a:link{color: #fff; text-decoration: none;font-size: 120% ;}			
			.main{width: 100%;}
			.invite{font-size: 180%; font-weight: bold; text-align: center;margin: 20px 0 20px 0;}
			.invite b{color: #e73f18;}
			.c_1{margin: 1% 2% 2% 2%;width:96%;height:auto; float: left;line-height: 26px;}
			.c_1_left,.c_2_left{width: 50%; float: left;}
			.c_1_right,.c_2_right{width: 50%; float: right;}
			.c_1_right b,.c_2_left b{font-size: 100%;}
			.c_1_right p,.c_2_left p{font-size: 80%;}
			.c_2{background: #f5f5f5;width: 93%;height:auto; float: left;line-height: 26px;padding: 0 3%;}
			.font_bottom{background: #e73f18;text-align:center; height:50px;line-height: 50px;}
			.toRegistBtn:LINK,.toRegistBtn:VISITED{color:#fff;}
		</style>
	</head>
	<body>
		<div class="main">
			<img src="${BASE_PATH}/static/app/img/share/top.png" width="100%"/>
			<p class="invite">邀请码：<b>${inviteCode}</b></p>
			<section class="c_1">
				<img class="c_1_left" src="${BASE_PATH}/static/app/img/share/img1.png" />
				<font class="c_1_right">
					<b>购物即存钱，购物即理财</b>
					<p>在本商城购物，你和朋友都可享受购物返现活动，边购物边存钱，轻松变身持家小能手！</p>
				</font>
			</section>
			<img style="float: left;" src="${BASE_PATH}/static/app/img/share/gray_bg_top.png" />
			<section class="c_2">
				<font class="c_2_left" style="padding-top: 20px;">
					<b>注册方法</b>
					<p>点击下方的注册按钮，邀请码会自动出现在注册页面，注册成功朋友和你即可享受活动。</p>
				</font>
				<img class="c_2_right" src="${BASE_PATH}/static/app/img/share/img2.png" />
			</section>
			<img src="${BASE_PATH}/static/app/img/share/gray_bg_bottom.png" />
			<section style="text-align: center;margin:8% 0% 10% 0;height：200px;">
				<p style="font-size: 130%;color:#e73f18;">注册时别忘了</p>
				<p style="font-size: 130%;margin-left:25%;">填写邀请码哦~</p>
			</section>
			<a class="toRegistBtn" href="${BASE_PATH}/regist/${inviteCode}"><p class="font_bottom">点击去注册</p></a>
		</div>
	</body>
</html>