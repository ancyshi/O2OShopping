<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>${STORE.title}</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_address.css" />
	</head>
	<body>
		<div id="emptyPart" <c:if test="${addressList!=null&&fn:length(addressList)!=0}">style="display:none;"</c:if>>
			<div>
				<div class="am-text-center app-m-t-10">暂无收货地址</div>
			</div>
		</div>
		<div id="notEmptyPart" <c:if test="${addressList==null||fn:length(addressList)==0}">style="display:none;"</c:if>>
			<div class="app-addr-wrapper">
				<c:forEach items="${addressList}" var="address">
				<div id="address${address.id}" class="app-addr-item am-g" data-default="${address.isDefault}">
					<div class="am-u-sm-11 am-u-md-11 am-u-lg-11 app-addr-left" onclick="editRow(${address.id})">
						<div class="am-text-truncate">${address.address}</div>
						<div>
							<span class="am-badge am-badge-danger am-radius app-addr-def" <c:if test="${address.isDefault==0}">style="display:none;"</c:if>>默认</span>
							<span>${address.linkName}</span><span class="app-m-l-20">${address.sex==1?'先生':'女士'}</span><span class="app-m-l-20">${address.tel}</span>
						</div>
					</div>
					<div class="am-u-sm-1 am-u-md-1 am-u-lg-1 app-addr-right">
						<a href="${BASE_PATH}/my/addrEdit/${address.id}"><i class="am-icon-edit"></i></a>
					</div>
					<div class="am-cf"></div>
				</div>
				<hr/>
				</c:forEach>
			</div>
		</div>
		
		<div class="app-addr-btn">
			<a href="${BASE_PATH}/my/addrAdd" class="am-btn am-btn-danger app-m-t-10" style="width:100%;">新增收货地址 </a>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript">
			var addressid;
			function editRow(id){
				var isDefault = $("#address"+id).data("default");
				console.log(isDefault);
				var popup = '<div class="edit-dialog">'
						+ '<a href="javascript:setDefault();" class="am-btn am-btn-danger am-btn-block"'+(isDefault==1?'disabled="disabled"':'')+'>设为默认</a>'
						+ '<a href="javascript:doDel();" class="am-btn am-btn-default am-btn-block">删除地址</a>'
						+ '</div>';
				Kit.ui.popup(popup);
				addressid = id;
			}
			function setDefault(){
				Kit.ajax.post("${BASE_PATH}/my/setAddressDefault",{addressid:addressid},function(result){
					if(result.flag==0){
						$(".app-addr-def").hide();
						$(".app-addr-def","#address"+addressid).show();
						$(".app-addr-item[data-default]").data("default",0);
						$("#address"+addressid).data("default",1);
					}
					Kit.ui.popdown();
				});
			}
			function doDel(){
				Kit.ui.confirm("确定要删除收货地址？",function(){
					Kit.ajax.post("${BASE_PATH}/my/delAddress",{addressid:addressid},function(result){
						if(result.flag==0){
							if($(".app-addr-item").length==1){
								Kit.ui.toggle("#emptyPart","#notEmptyPart");
							}
							//移除收货地址及跟随的hr
							var nexthr = $("#address"+addressid).next();$("#address"+addressid).remove();
							if(nexthr.length==1)nexthr.remove();
						}
						Kit.ui.popdown();
					});
				},function(){
					Kit.ui.popdown();
				});
			}
		</script>
	</body>
</html>