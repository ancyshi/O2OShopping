<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>订单跟踪</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_order_detail.css" />
	</head>
	<body class="app-navbar-body app-p-b-10">
		<header class="am-header app-header">
			<div class="am-header-left am-header-nav">
          		<a href="javascript:history.go(-1);" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title app-title">订单跟踪</h1>
		</header>
		
		<div id="panel"></div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
		<div class="app-list">
			<div class="app-list-item">
				{{if payType==<%=OrderCode.OrderPayType.ON_LINE%>}}
				<label>支付方式：</label><span>在线支付</span>
				{{else}}
				<label>支付方式：</label><span>货到付款</span>
				{{/if}}
			</div>
			<div class="app-list-item">
				{{if takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
				<label>配送方式：</label><span>门店配送</span>
				{{else}}
				<label>配送方式：</label><span>上门自取</span>
				{{/if}}
			</div>
			<div class="app-list-item">
				<label>订单编号：</label><span>{{orderNum}}</span>
			</div>
		</div>
		<div class="app-list app-m-t-10 am-g">
			<div style="margin-left:10px;padding-left:20px;border-left:1px solid #eee;">
				{{each logList as log index}}
				<div style="position:relative;">
					{{if index!=0}}
					<img src="${BASE_PATH}/static/app/img/clock_n.png" width="20" style="position:absolute;top:10px;left:-30px;">
					{{else}}
					<img src="${BASE_PATH}/static/app/img/clock_c.png" width="20" style="position:absolute;top:0;left:-30px;">
					{{/if}}
					{{if index!=0}}<hr/>{{/if}}
					<div>
						<div>{{log.content}}</div>
						<span class="app-text-gray">{{log.createDate}}</span>
					</div>
				</div>
				{{/each}}
			</div>
		</div>
		</script>
		<script type="text/javascript">
			$(function(){
				//初始化显示订单信息
				Kit.ajax.post("${BASE_PATH}/order/getTrack",{orderNum:'${orderNum}'},function(result){
					if(result.flag==0){
						$("#panel").append(template("dataTpl",result.object));
					}else{
						Kit.ui.toast(result.message);
						setTimeout(function(){
							window.history.go(-1);
						},2000);
					}
				});
			});
		</script>
	</body>
</html>