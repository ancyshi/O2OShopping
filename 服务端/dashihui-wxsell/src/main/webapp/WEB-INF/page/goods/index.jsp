<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>${STORE.title}</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_goods_index.css" />
	</head>
	<body style="position: relative;">
		<span id="top"></span>
		<div data-am-sticky>
			<div style="position:relative;">
				<div class="app-tab">
					<div class="app-tab-item am-fl" style="width:20%;">
						<a id="app-all-title" href="javascript:toggleCategory(1);" class="rdriver">
							<span id="categoryTitle">全部</span>
							<i id="app-all-icon" class="am-icon-angle-up"></i>
						</a>
					</div>
					<div class="app-tab-item am-fl" style="width:20%;">
						<a href="javascript:void(0);" class="rdriver">
							<span id="app-sort-sale-title" onclick="javascript:sortBySale();">销量</span>
						</a>
					</div>
					<div class="app-tab-item am-fl" style="width:40%;">
						<a href="javascript:void(0);" class="rdriver" onclick="javascript:filterBySelf();">
							<span id="app-filter-self-title">大实惠直营</span>
							<img id="app-filter-self-false-icon" src="${BASE_PATH}/static/app/img/check_false_square.png" height="15" style="margin-top:-4px;">
							<img id="app-filter-self-true-icon" src="${BASE_PATH}/static/app/img/check_true_square.png" height="15" style="margin-top:-4px;display:none;">
						</a>
					</div>
					<div class="app-tab-item am-fl" style="width:20%;">
						<a href="javascript:void(0);" onclick="javascript:sortByPrice();">
							<span id="app-sort-price-title">价格</span>
							<i id="app-sort-price-icon"></i>
						</a>
					</div>
					<div class="am-cf"></div>
				</div>
				<div id="categoryPop" class="app-category-wrapper" style="display:none;">
					<div class="am-g app-category">
						<div class="am-u-sm-6 am-u-md-6 am-u-lg-6">
							<ul class="app-category-one">
								<c:forEach items="${categoryList}" var="one">
								<li <c:if test="${code==one.code}">class="active"</c:if>>
									<a href="javascript:void(0)" data-code="${one.code}">
										<img src="${BASE_PATH}/static/app/img/category_${one.code}.png" width="20">
										<span>${one.name}</span>
										<i class="am-icon-angle-right am-fr"></i>
									</a>
								</li>
								</c:forEach>
							</ul>
						</div>
						<div class="am-u-sm-6 am-u-md-6 am-u-lg-6">
							<c:forEach items="${categoryList}" var="one">
							<ul class="app-category-two" data-parent-code="${one.code}" <c:if test="${code!=one.code}">style="display:none;"</c:if>>
								<li <c:if test="${code==one.code}">class="active"</c:if>>
									<a href="javascript:void(0)" data-code="${one.code}" data-title="${one.name}">
										<span>全部</span>
										<span class="app-category-count am-fr">${one.goodscount}</span>
									</a>
								</li>
								<c:forEach items="${one.children}" var="two">
								<li>
									<a href="javascript:void(0)" data-code="${two.code}" data-title="${two.name}">
										<span>${two.name}</span>
										<span class="app-category-count am-fr">${two.goodscount}</span>
									</a>
								</li>
								</c:forEach>
							</ul>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
	    
	    <!-- 商品列表 -->
	    <div class="app-part app-part-flow">
			<div id="goodsList" class="am-g app-goods-list"></div>
			<div id="goodsShadow" class="app-goods-shadow" style="display:none;"></div>
		</div>
		
		<!-- 购物车+1的动画效果，需要app-navbar设置z-index小于本控件 -->
		<span id="cartNumAnimate" style="color:red;font-weight:bold;font-size:18px;position:fixed;left:7%;bottom:50px;z-index:2;display:none;">+1</span>
		
		<div class="app-cart" >
			<span id="cartNum" class="am-badge am-round" style="display:none;"></span>
			<a href="${BASE_PATH }/cart"><img src="${BASE_PATH}/static/app/img/breakfast_bottom_left.png" style="width:50px;height:50px;"/></a>
		</div>
		
		<div data-am-widget="gotop" class="am-gotop am-gotop-fixed" >
			<a href="#top"><span class="am-gotop-title">回到顶部</span><i class="am-gotop-icon am-icon-chevron-up"></i></a>
		</div>
	    
	    <div class="emptyPart" style="display: none;">
			<div>
				<div>
					<img src="${BASE_PATH}/static/app/img/search_icon_ineffective.png" style="margin:0 auto;display:block;" width="140">
					<div class="am-text-center app-m-t-10 app-text-weight">没有搜索结果</div>
					<div class="am-text-center">换个关键词试试吧</div>
				</div>
			</div>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-marquee/jquery.marquee.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			{{each list as goods}}
			<div class="am-u-sm-6 am-u-md-4 am-u-lg-3 app-goods-item">
            	<dl>
            		<dt>
						<a href="${BASE_PATH}/goods/detail/{{goods.id}}">
							<img data-original="${FTP_PATH}{{goods.thumb}}" class="am-center lazyload" width="100%"/>
						</a>
					</dt>
					{{if goods.isSelf==1}}
            		<dd class="app-item-title"><img src="${BASE_PATH}/static/app/img/goods_self.png" height="18"/>{{goods.name}}</dd>
					{{else}}
					<dd class="app-item-title">{{goods.name}}</dd>
					{{/if}}
            		<dd>
            			<span class="app-item-price-normal"><i class="am-icon-rmb"></i> {{goods.sellPrice}}</span>
            			<span class="app-item-price-del"><i class="am-icon-rmb"></i> {{goods.marketPrice}}</span>
						<span>
							<a onclick="javascript:addCart('{{goods.id}}','{{goods.isSelf}}','{{goods.name}}','{{goods.spec}}','{{goods.thumb}}','{{goods.sellPrice}}','{{goods.marketPrice}}');">
								<img src="${BASE_PATH }/static/app/img/add_car.png" style="width:30px;heigt:30px;padding-top:6px;"/>
							</a>
						</span>
            		</dd>
            	</dl>
            </div>
			{{/each}}
		</script>
		<script type="text/javascript">
			var categoryPopShown = false,conCode = "${code}", ctwCode = "",isSelf = 0,pageNum = 0, totalPage = 1, orderBy = 0;
			//加载标识，表示当前是否有请求未完成，防止同时多个请求
			var loading = false;
			$(function(){
				//弹出分类事件
				$("a",".app-category-one").click(function(){
					var code = $(this).data("code");
					$(".app-category-two").hide();
					$(".app-category-two[data-parent-code="+code+"]").show();
					$(this).parent().addClass("active").siblings().removeClass("active");
					conCode = code;
				});
				$("a",".app-category-two").click(function(){
					var code = $(this).data("code");
					$(this).parent().addClass("active").siblings().removeClass("active");
					ctwCode = conCode==code?"":code;
					$("#categoryTitle").text($(this).data("title"));
					//关闭分类选择弹框
					initPage();
					toggleCategory(2);
					//重新加载第一页
				});
				//初始化滚动加载分页
				initPage();
				//查询购物车状态
				Kit.ajax.post("${BASE_PATH}/cart/state",{},function(result){
					if(result.object.CART_COUNTER!=0)
						$("#cartNum").text(result.object.CART_COUNTER).show();
				});
			});
			
			//销量默认按从大到小排序
			function sortBySale() {
				orderBy = 1;
				$("#app-sort-price-title").removeAttr("style");
				$("#app-sort-price-icon").removeAttr("style");
				$("#app-sort-sale-title").attr("style", "color:red;font-weight:bold;");
				initPage();
			}
			
			//按自营搜索
			function filterBySelf() {
				var title = $("#app-filter-self-title");
				var checkboxTrue = $("#app-filter-self-true-icon");
				var checkboxFalse = $("#app-filter-self-false-icon");
				if(checkboxTrue.is(":hidden")){
					isSelf = 1;
					title.attr("style", "color:red;color:red;font-weight:bold;");
					checkboxTrue.show();
					checkboxFalse.hide();
				}else{
					isSelf = 0;
					title.removeAttr("style");
					checkboxFalse.show();
					checkboxTrue.hide();
				}
				//初始化滚动加载分页
				initPage();
			}
			
			//按价格排序
			function sortByPrice() {
				$("#app-sort-price-title").attr("style", "color:red;color:red;font-weight:bold;");
				$("#app-sort-price-icon").attr("style", "color:red;color:red;font-weight:bold;");
				$("#app-sort-sale-title").removeAttr("style");
				if(orderBy == 0 || orderBy ==1 || orderBy == 3) {
					orderBy = 2;//价格从低到高排序
					$("#app-sort-price-icon").remove("class");
					$("#app-sort-price-icon").attr("class","am-icon-angle-up");
				} else if(orderBy == 2) {
					orderBy = 3;//价格从高到低排序
					$("#app-sort-price-icon").remove("class");
					$("#app-sort-price-icon").attr("class","am-icon-angle-down");
				}
				initPage();
			}
			function toggleCategory(f){
				if(f==1){
					$("#app-all-title").attr("href", "javascript:toggleCategory(2);");
					$("#app-all-icon").attr("class", "am-icon-angle-down");
					$("#categoryPop").slideDown();
					$("#goodsShadow").show();
					$("#goodsShadow").one("click",function(){
						$("#categoryPop").slideUp();
						$("#goodsShadow").hide();
					});
				}else if(f==2){
					$("#app-all-title").attr("href", "javascript:toggleCategory(1);");
					$("#app-all-icon").attr("class", "am-icon-angle-up");
					$("#categoryPop").slideUp();
					$("#goodsShadow").hide();
				}
			}
			function initPage(){
				console.log({pageNum:pageNum+1,pageSize:10,conCode:conCode,ctwCode:ctwCode,isSelf:isSelf,orderBy:orderBy});
				pageNum = 0;
				totalPage = 1;
				$("#goodsList").empty();
				bindSpy();
			}
			function bindSpy(){
				//绑定“加载中”进入加载事件
				Kit.util.onPageEnd(function(){
	               	if(pageNum<totalPage&&!loading){
	               		loading = true;
	               		$("#goodsList").append("<div class=\"app-loading\">正在加载</div>");
	                	Kit.ajax.post("${BASE_PATH}/goods/page",{pageNum:pageNum+1,pageSize:10,conCode:conCode,ctwCode:ctwCode,isSelf:isSelf,orderBy:orderBy},function(result){
							$("#goodsList").append(template("dataTpl",result.object));
							//amazeui要求在最后一个元素上添加am-u-end样式，否则默认为右浮动
							$(".app-goods-item","#goodsList").removeClass("am-u-end").last().addClass("am-u-end");
							//图片延迟加载
							$("img.lazyload","#goodsList").not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
							$(".app-loading","#goodsList").remove();
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
							if(totalPage == 0){
								Kit.ui.toggle($(".emptyPart"), $(".app-part"));
							} else {
								Kit.ui.toggle($(".app-part"), $(".emptyPart"));
								$("img.lazyload","#goodsList").lazyload({
									failurelimit : 100,
									effect : "show"
								}).addClass("lazyload-binded");
							}
							//重置加载标识
							loading = false;
						});
	               	}
				});
			}
			function addCart(id,isSelf,name,spec,thumb,sellPrice,marketPrice){
				var goods = {
					"id":id,
					"isSelf":isSelf,
					"name":name,
					"spec":spec,
					"thumb":thumb,
					"sellPrice":sellPrice,
					"marketPrice":marketPrice
				};
				Kit.ajax.post("${BASE_PATH}/cart/add",goods,function(result){
					$("#cartNum").show().text(result.object.counter);
					$("#cartNumAnimate").show().animate({bottom:70,opacity:0},500,"linear",function(){
						$("#cartNumAnimate").css({"bottom":50+"px","opacity":150}).hide();
					});
				});
			}
		</script>
	</body>
</html>